package net.wg.gui.lobby.vehiclePreview.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class VehPreviewEliteFactSheetVO extends DAAPIDataClass
    {

        public var title:String = "";

        public var info:String = "";

        public function VehPreviewEliteFactSheetVO(param1:Object)
        {
            super(param1);
        }
    }
}
