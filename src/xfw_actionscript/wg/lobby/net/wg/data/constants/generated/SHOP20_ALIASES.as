package net.wg.data.constants.generated
{
    public class SHOP20_ALIASES extends Object
    {

        public static const VEHICLE_SELL_CONFIRMATION_POPOVER_ALIAS:String = "VehicleSellConfirmationPopover";

        public static const RENTAL_TERM_SELECTION_POPOVER_ALIAS:String = "RentalTermSelectionPopover";

        public function SHOP20_ALIASES()
        {
            super();
        }
    }
}
