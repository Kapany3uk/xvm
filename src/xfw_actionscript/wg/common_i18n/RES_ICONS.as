package
{
    public class RES_ICONS extends Object
    {

        public static const MAPS_ICONS_BUTTONS:String = "../maps/icons/buttons";

        public static const MAPS_ICONS_IMG:String = "../maps/icons/img.png";

        public static const MAPS_ICONS_BATTLEHELP_DUALGUNHELP_QUICK_FIRE:String = "../maps/icons/battleHelp/dualGunHelp/quick_fire.png";

        public static const MAPS_ICONS_BATTLEHELP_DUALGUNHELP_VOLLEY_FIRE:String = "../maps/icons/battleHelp/dualGunHelp/volley_fire.png";

        public static const MAPS_ICONS_BATTLEHELP_WHEELEDHELP_WHEEL_BURNOUT:String = "../maps/icons/battleHelp/wheeledHelp/wheel_burnout.png";

        public static const MAPS_ICONS_BATTLEHELP_WHEELEDHELP_WHEEL_CHASSIS:String = "../maps/icons/battleHelp/wheeledHelp/wheel_chassis.png";

        public static const MAPS_ICONS_BATTLEHELP_WHEELEDHELP_WHEEL_DETAILS:String = "../maps/icons/battleHelp/wheeledHelp/wheel_details.png";

        public static const MAPS_ICONS_BATTLEHELP_WHEELEDHELP_WHEEL_TWO_MODE:String = "../maps/icons/battleHelp/wheeledHelp/wheel_two_mode.png";

        public static const MAPS_ICONS_BATTLELOADING_SELFTABLEBACKGROUND:String = "../maps/icons/battleLoading/selfTableBackground.png";

        public static const MAPS_ICONS_BATTLELOADING_SELFTIPSBACKGROUND:String = "../maps/icons/battleLoading/selfTipsBackground.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_ALL:String = "../maps/icons/battleLoading/groups/all.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_CREW:String = "../maps/icons/battleLoading/groups/crew.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_ECONOMICS:String = "../maps/icons/battleLoading/groups/economics.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_INTERFACE:String = "../maps/icons/battleLoading/groups/interface.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_RULES:String = "../maps/icons/battleLoading/groups/rules.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_SECURITY:String = "../maps/icons/battleLoading/groups/security.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_TACTICS:String = "../maps/icons/battleLoading/groups/tactics.png";

        public static const MAPS_ICONS_BATTLELOADING_GROUPS_TECHPROBLEMS:String = "../maps/icons/battleLoading/groups/techProblems.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_EPICRANDOM0:String = "../maps/icons/battleLoading/tips/epicRandom0.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_EVENT:String = "../maps/icons/battleLoading/tips/event.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED1:String = "../maps/icons/battleLoading/tips/ranked1.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED2:String = "../maps/icons/battleLoading/tips/ranked2.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED3:String = "../maps/icons/battleLoading/tips/ranked3.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED4:String = "../maps/icons/battleLoading/tips/ranked4.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED5:String = "../maps/icons/battleLoading/tips/ranked5.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED6:String = "../maps/icons/battleLoading/tips/ranked6.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED7:String = "../maps/icons/battleLoading/tips/ranked7.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED8:String = "../maps/icons/battleLoading/tips/ranked8.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_RANKED9:String = "../maps/icons/battleLoading/tips/ranked9.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_SANDBOX00:String = "../maps/icons/battleLoading/tips/sandbox00.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_SANDBOX01:String = "../maps/icons/battleLoading/tips/sandbox01.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_SANDBOX10:String = "../maps/icons/battleLoading/tips/sandbox10.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_SANDBOX11:String = "../maps/icons/battleLoading/tips/sandbox11.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP106:String = "../maps/icons/battleLoading/tips/tip106.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP107:String = "../maps/icons/battleLoading/tips/tip107.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP108:String = "../maps/icons/battleLoading/tips/tip108.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP11:String = "../maps/icons/battleLoading/tips/tip11.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP111:String = "../maps/icons/battleLoading/tips/tip111.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP123:String = "../maps/icons/battleLoading/tips/tip123.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP124:String = "../maps/icons/battleLoading/tips/tip124.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP125:String = "../maps/icons/battleLoading/tips/tip125.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP126:String = "../maps/icons/battleLoading/tips/tip126.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP127:String = "../maps/icons/battleLoading/tips/tip127.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP128:String = "../maps/icons/battleLoading/tips/tip128.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP130:String = "../maps/icons/battleLoading/tips/tip130.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP131:String = "../maps/icons/battleLoading/tips/tip131.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP132:String = "../maps/icons/battleLoading/tips/tip132.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP145:String = "../maps/icons/battleLoading/tips/tip145.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP165:String = "../maps/icons/battleLoading/tips/tip165.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP166:String = "../maps/icons/battleLoading/tips/tip166.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP167:String = "../maps/icons/battleLoading/tips/tip167.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP168:String = "../maps/icons/battleLoading/tips/tip168.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP17:String = "../maps/icons/battleLoading/tips/tip17.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP170:String = "../maps/icons/battleLoading/tips/tip170.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP173:String = "../maps/icons/battleLoading/tips/tip173.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP174:String = "../maps/icons/battleLoading/tips/tip174.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP186:String = "../maps/icons/battleLoading/tips/tip186.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP20:String = "../maps/icons/battleLoading/tips/tip20.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP23:String = "../maps/icons/battleLoading/tips/tip23.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP25:String = "../maps/icons/battleLoading/tips/tip25.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP28:String = "../maps/icons/battleLoading/tips/tip28.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP29:String = "../maps/icons/battleLoading/tips/tip29.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP31:String = "../maps/icons/battleLoading/tips/tip31.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP32:String = "../maps/icons/battleLoading/tips/tip32.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP37:String = "../maps/icons/battleLoading/tips/tip37.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP39:String = "../maps/icons/battleLoading/tips/tip39.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP42:String = "../maps/icons/battleLoading/tips/tip42.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP44:String = "../maps/icons/battleLoading/tips/tip44.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP47:String = "../maps/icons/battleLoading/tips/tip47.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP56:String = "../maps/icons/battleLoading/tips/tip56.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP57:String = "../maps/icons/battleLoading/tips/tip57.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP58:String = "../maps/icons/battleLoading/tips/tip58.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP59:String = "../maps/icons/battleLoading/tips/tip59.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP60:String = "../maps/icons/battleLoading/tips/tip60.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP65:String = "../maps/icons/battleLoading/tips/tip65.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP71:String = "../maps/icons/battleLoading/tips/tip71.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP75:String = "../maps/icons/battleLoading/tips/tip75.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP78:String = "../maps/icons/battleLoading/tips/tip78.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP79:String = "../maps/icons/battleLoading/tips/tip79.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP87:String = "../maps/icons/battleLoading/tips/tip87.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP89:String = "../maps/icons/battleLoading/tips/tip89.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP95:String = "../maps/icons/battleLoading/tips/tip95.png";

        public static const MAPS_ICONS_BATTLELOADING_TIPS_TIP99:String = "../maps/icons/battleLoading/tips/tip99.png";

        public static const MAPS_ICONS_BATTLETYPES_1:String = "../maps/icons/battleTypes/1.png";

        public static const MAPS_ICONS_BATTLETYPES_10:String = "../maps/icons/battleTypes/10.png";

        public static const MAPS_ICONS_BATTLETYPES_11:String = "../maps/icons/battleTypes/11.png";

        public static const MAPS_ICONS_BATTLETYPES_12:String = "../maps/icons/battleTypes/12.png";

        public static const MAPS_ICONS_BATTLETYPES_13:String = "../maps/icons/battleTypes/13.png";

        public static const MAPS_ICONS_BATTLETYPES_14:String = "../maps/icons/battleTypes/14.png";

        public static const MAPS_ICONS_BATTLETYPES_15:String = "../maps/icons/battleTypes/15.png";

        public static const MAPS_ICONS_BATTLETYPES_16:String = "../maps/icons/battleTypes/16.png";

        public static const MAPS_ICONS_BATTLETYPES_17:String = "../maps/icons/battleTypes/17.png";

        public static const MAPS_ICONS_BATTLETYPES_18:String = "../maps/icons/battleTypes/18.png";

        public static const MAPS_ICONS_BATTLETYPES_19:String = "../maps/icons/battleTypes/19.png";

        public static const MAPS_ICONS_BATTLETYPES_2:String = "../maps/icons/battleTypes/2.png";

        public static const MAPS_ICONS_BATTLETYPES_20:String = "../maps/icons/battleTypes/20.png";

        public static const MAPS_ICONS_BATTLETYPES_21:String = "../maps/icons/battleTypes/21.png";

        public static const MAPS_ICONS_BATTLETYPES_23:String = "../maps/icons/battleTypes/23.png";

        public static const MAPS_ICONS_BATTLETYPES_3:String = "../maps/icons/battleTypes/3.png";

        public static const MAPS_ICONS_BATTLETYPES_4:String = "../maps/icons/battleTypes/4.png";

        public static const MAPS_ICONS_BATTLETYPES_5:String = "../maps/icons/battleTypes/5.png";

        public static const MAPS_ICONS_BATTLETYPES_6:String = "../maps/icons/battleTypes/6.png";

        public static const MAPS_ICONS_BATTLETYPES_7:String = "../maps/icons/battleTypes/7.png";

        public static const MAPS_ICONS_BATTLETYPES_8:String = "../maps/icons/battleTypes/8.png";

        public static const MAPS_ICONS_BATTLETYPES_9:String = "../maps/icons/battleTypes/9.png";

        public static const MAPS_ICONS_BATTLETYPES_EPICQUEUE:String = "../maps/icons/battleTypes/epicQueue.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_ASSAULT:String = "../maps/icons/battleTypes/136x136/assault.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_ASSAULT1:String = "../maps/icons/battleTypes/136x136/assault1.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_ASSAULT2:String = "../maps/icons/battleTypes/136x136/assault2.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_BOB:String = "../maps/icons/battleTypes/136x136/bob.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_BOOTCAMP:String = "../maps/icons/battleTypes/136x136/bootcamp.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_CLAN:String = "../maps/icons/battleTypes/136x136/clan.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_CTF:String = "../maps/icons/battleTypes/136x136/ctf.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_CTF30X30:String = "../maps/icons/battleTypes/136x136/ctf30x30.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_DOMINATION:String = "../maps/icons/battleTypes/136x136/domination.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_DOMINATION30X30:String = "../maps/icons/battleTypes/136x136/domination30x30.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_EPICBATTLE:String = "../maps/icons/battleTypes/136x136/epicbattle.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_ESCORT:String = "../maps/icons/battleTypes/136x136/escort.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_FALLOUT:String = "../maps/icons/battleTypes/136x136/fallout.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_FALLOUT_CLASSIC:String = "../maps/icons/battleTypes/136x136/fallout_classic.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_FALLOUT_MULTITEAM:String = "../maps/icons/battleTypes/136x136/fallout_multiteam.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_FORTIFICATIONS:String = "../maps/icons/battleTypes/136x136/fortifications.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_HISTORICAL:String = "../maps/icons/battleTypes/136x136/historical.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_NATIONS:String = "../maps/icons/battleTypes/136x136/nations.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_NEUTRAL:String = "../maps/icons/battleTypes/136x136/neutral.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_PLATOON:String = "../maps/icons/battleTypes/136x136/platoon.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_RANDOM:String = "../maps/icons/battleTypes/136x136/random.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_RANKED:String = "../maps/icons/battleTypes/136x136/ranked.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_RATEDSANDBOX:String = "../maps/icons/battleTypes/136x136/ratedsandbox.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_RESOURCEPOINTS:String = "../maps/icons/battleTypes/136x136/resourcePoints.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_SANDBOX:String = "../maps/icons/battleTypes/136x136/sandbox.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_SQUAD:String = "../maps/icons/battleTypes/136x136/squad.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_TEAM:String = "../maps/icons/battleTypes/136x136/team.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_TEAM7X7:String = "../maps/icons/battleTypes/136x136/team7x7.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_TRAINING:String = "../maps/icons/battleTypes/136x136/training.png";

        public static const MAPS_ICONS_BATTLETYPES_136X136_TUTORIAL:String = "../maps/icons/battleTypes/136x136/tutorial.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_BATTLETEACHING:String = "../maps/icons/battleTypes/40x40/battleTeaching.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_BOB:String = "../maps/icons/battleTypes/40x40/bob.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_BOBSQUAD:String = "../maps/icons/battleTypes/40x40/bobSquad.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_EPICBATTLE:String = "../maps/icons/battleTypes/40x40/epicbattle.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_EPICQUEUE:String = "../maps/icons/battleTypes/40x40/epicQueue.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_EPICTRAININGSLIST:String = "../maps/icons/battleTypes/40x40/epicTrainingsList.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_ESPORT:String = "../maps/icons/battleTypes/40x40/eSport.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_EVENT:String = "../maps/icons/battleTypes/40x40/event.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_EVENTSQUAD:String = "../maps/icons/battleTypes/40x40/eventSquad.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_FALLOUT:String = "../maps/icons/battleTypes/40x40/fallout.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_FALLOUT_CLASSIC:String = "../maps/icons/battleTypes/40x40/fallout_classic.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_FALLOUT_MULTITEAM:String = "../maps/icons/battleTypes/40x40/fallout_multiteam.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_FORT:String = "../maps/icons/battleTypes/40x40/fort.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_HISTORICAL:String = "../maps/icons/battleTypes/40x40/historical.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_RANDOM:String = "../maps/icons/battleTypes/40x40/random.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_RANKED:String = "../maps/icons/battleTypes/40x40/ranked.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_SPECBATTLESLIST:String = "../maps/icons/battleTypes/40x40/specBattlesList.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_SQUAD:String = "../maps/icons/battleTypes/40x40/squad.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_STRONGHOLDSBATTLESLIST:String = "../maps/icons/battleTypes/40x40/strongholdsBattlesList.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_TRAININGSLIST:String = "../maps/icons/battleTypes/40x40/trainingsList.png";

        public static const MAPS_ICONS_BATTLETYPES_40X40_TUTORIAL:String = "../maps/icons/battleTypes/40x40/tutorial.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_ASSAULT:String = "../maps/icons/battleTypes/64x64/assault.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_ASSAULT1:String = "../maps/icons/battleTypes/64x64/assault1.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_ASSAULT2:String = "../maps/icons/battleTypes/64x64/assault2.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_BATTLETEACHING:String = "../maps/icons/battleTypes/64x64/battleTeaching.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_BOB:String = "../maps/icons/battleTypes/64x64/bob.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_BOBSQUAD:String = "../maps/icons/battleTypes/64x64/bobSquad.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_CTF:String = "../maps/icons/battleTypes/64x64/ctf.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_CTF30X30:String = "../maps/icons/battleTypes/64x64/ctf30x30.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_DOMINATION:String = "../maps/icons/battleTypes/64x64/domination.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_EPICBATTLE:String = "../maps/icons/battleTypes/64x64/epicbattle.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_EPICQUEUE:String = "../maps/icons/battleTypes/64x64/epicQueue.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_EPICTRAININGSLIST:String = "../maps/icons/battleTypes/64x64/epicTrainingsList.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_ESCORT:String = "../maps/icons/battleTypes/64x64/escort.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_ESPORT:String = "../maps/icons/battleTypes/64x64/eSport.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_EVENT:String = "../maps/icons/battleTypes/64x64/event.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_EVENTSQUAD:String = "../maps/icons/battleTypes/64x64/eventSquad.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_FALLOUT:String = "../maps/icons/battleTypes/64x64/fallout.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_FALLOUT_CLASSIC:String = "../maps/icons/battleTypes/64x64/fallout_classic.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_FALLOUT_MULTITEAM:String = "../maps/icons/battleTypes/64x64/fallout_multiteam.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_FORT:String = "../maps/icons/battleTypes/64x64/fort.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_FORTIFICATIONS:String = "../maps/icons/battleTypes/64x64/fortifications.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_HISTORICAL:String = "../maps/icons/battleTypes/64x64/historical.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_NATIONS:String = "../maps/icons/battleTypes/64x64/nations.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_NEUTRAL:String = "../maps/icons/battleTypes/64x64/neutral.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_RANDOM:String = "../maps/icons/battleTypes/64x64/random.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_RANKED:String = "../maps/icons/battleTypes/64x64/ranked.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_RANKED_EPICRANDOM:String = "../maps/icons/battleTypes/64x64/ranked_epicRandom.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_RESOURCEPOINTS:String = "../maps/icons/battleTypes/64x64/resourcePoints.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_SPECBATTLESLIST:String = "../maps/icons/battleTypes/64x64/specBattlesList.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_SQUAD:String = "../maps/icons/battleTypes/64x64/squad.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_STRONGHOLDSBATTLESLIST:String = "../maps/icons/battleTypes/64x64/strongholdsBattlesList.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_TEAM7X7:String = "../maps/icons/battleTypes/64x64/team7x7.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_TRAININGSLIST:String = "../maps/icons/battleTypes/64x64/trainingsList.png";

        public static const MAPS_ICONS_BATTLETYPES_64X64_TUTORIAL:String = "../maps/icons/battleTypes/64x64/tutorial.png";

        public static const MAPS_ICONS_BATTLETYPES_BACKGROUNDS_RANDOM:String = "../maps/icons/battleTypes/backgrounds/random.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUECHECK:String = "../maps/icons/blueprints/blueCheck.png";

        public static const MAPS_ICONS_BLUEPRINTS_BORDERSLOT:String = "../maps/icons/blueprints/borderSlot.png";

        public static const MAPS_ICONS_BLUEPRINTS_GRID_SLOT:String = "../maps/icons/blueprints/grid_slot.png";

        public static const MAPS_ICONS_BLUEPRINTS_PLUS:String = "../maps/icons/blueprints/plus.png";

        public static const MAPS_ICONS_BLUEPRINTS_PROGRESSBARGRDNT:String = "../maps/icons/blueprints/progressBarGrdnt.png";

        public static const MAPS_ICONS_BLUEPRINTS_SLOT_GLOW:String = "../maps/icons/blueprints/slot_glow.png";

        public static const MAPS_ICONS_BLUEPRINTS_VEHICLESKETCH:String = "../maps/icons/blueprints/vehicleSketch.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_ADDICON:String = "../maps/icons/blueprints/blueprintScreen/addIcon.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_BACKGROUND:String = "../maps/icons/blueprints/blueprintScreen/background.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_BALANCEBACKGROUND:String = "../maps/icons/blueprints/blueprintScreen/balanceBackground.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_COMPLETEANIMATIONSMOKE:String = "../maps/icons/blueprints/blueprintScreen/completeAnimationSmoke.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_DISCOUNTSHINE:String = "../maps/icons/blueprints/blueprintScreen/discountShine.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_FRAGMENTCORNER:String = "../maps/icons/blueprints/blueprintScreen/fragmentCorner.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_ICEQUAL:String = "../maps/icons/blueprints/blueprintScreen/icEqual.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_ICPLUS:String = "../maps/icons/blueprints/blueprintScreen/icPlus.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_ITEM_HOVER_BLACK:String = "../maps/icons/blueprints/blueprintScreen/item_hover_black.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_ITEM_HOVER_BLUE:String = "../maps/icons/blueprints/blueprintScreen/item_hover_blue.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_LOCKSTATUS:String = "../maps/icons/blueprints/blueprintScreen/lockStatus.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NOTAVAILABLEICON:String = "../maps/icons/blueprints/blueprintScreen/notAvailableIcon.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_PMLIGHTORANGE:String = "../maps/icons/blueprints/blueprintScreen/pmLightOrange.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_RULES:String = "../maps/icons/blueprints/blueprintScreen/rules.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_SHEMEITEMICON:String = "../maps/icons/blueprints/blueprintScreen/shemeItemIcon.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_STAMPBORDER:String = "../maps/icons/blueprints/blueprintScreen/stampBorder.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_TOS_BG:String = "../maps/icons/blueprints/blueprintScreen/tos_bg.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_GRIDS_GRIDS_10:String = "../maps/icons/blueprints/blueprintScreen/grids/grids_10.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_GRIDS_GRIDS_12:String = "../maps/icons/blueprints/blueprintScreen/grids/grids_12.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_GRIDS_GRIDS_4:String = "../maps/icons/blueprints/blueprintScreen/grids/grids_4.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_GRIDS_GRIDS_6:String = "../maps/icons/blueprints/blueprintScreen/grids/grids_6.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_GRIDS_GRIDS_8:String = "../maps/icons/blueprints/blueprintScreen/grids/grids_8.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_1:String = "../maps/icons/blueprints/blueprintScreen/numbering/1.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_10:String = "../maps/icons/blueprints/blueprintScreen/numbering/10.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_11:String = "../maps/icons/blueprints/blueprintScreen/numbering/11.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_12:String = "../maps/icons/blueprints/blueprintScreen/numbering/12.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_2:String = "../maps/icons/blueprints/blueprintScreen/numbering/2.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_3:String = "../maps/icons/blueprints/blueprintScreen/numbering/3.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_4:String = "../maps/icons/blueprints/blueprintScreen/numbering/4.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_5:String = "../maps/icons/blueprints/blueprintScreen/numbering/5.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_6:String = "../maps/icons/blueprints/blueprintScreen/numbering/6.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_7:String = "../maps/icons/blueprints/blueprintScreen/numbering/7.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_8:String = "../maps/icons/blueprints/blueprintScreen/numbering/8.png";

        public static const MAPS_ICONS_BLUEPRINTS_BLUEPRINTSCREEN_NUMBERING_9:String = "../maps/icons/blueprints/blueprintScreen/numbering/9.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_CHINA:String = "../maps/icons/blueprints/fragment/big/china.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_CZECH:String = "../maps/icons/blueprints/fragment/big/czech.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_FRANCE:String = "../maps/icons/blueprints/fragment/big/france.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_GERMANY:String = "../maps/icons/blueprints/fragment/big/germany.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_INTELLIGENCE:String = "../maps/icons/blueprints/fragment/big/intelligence.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_ITALY:String = "../maps/icons/blueprints/fragment/big/italy.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_JAPAN:String = "../maps/icons/blueprints/fragment/big/japan.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_POLAND:String = "../maps/icons/blueprints/fragment/big/poland.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_SWEDEN:String = "../maps/icons/blueprints/fragment/big/sweden.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_UK:String = "../maps/icons/blueprints/fragment/big/uk.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_USA:String = "../maps/icons/blueprints/fragment/big/usa.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_USSR:String = "../maps/icons/blueprints/fragment/big/ussr.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_VEHICLE:String = "../maps/icons/blueprints/fragment/big/vehicle.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_VEHICLE_COMPLETE:String = "../maps/icons/blueprints/fragment/big/vehicle_complete.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_BIG_VEHICLE_FINAL:String = "../maps/icons/blueprints/fragment/big/vehicle_final.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_LARGE_VEHICLE_COMPLETE:String = "../maps/icons/blueprints/fragment/large/vehicle_complete.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_LARGE_VEHICLE_FINAL:String = "../maps/icons/blueprints/fragment/large/vehicle_final.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_CHINA:String = "../maps/icons/blueprints/fragment/medium/china.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_CZECH:String = "../maps/icons/blueprints/fragment/medium/czech.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_FRANCE:String = "../maps/icons/blueprints/fragment/medium/france.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_GERMANY:String = "../maps/icons/blueprints/fragment/medium/germany.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_INTELLIGENCE:String = "../maps/icons/blueprints/fragment/medium/intelligence.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_ITALY:String = "../maps/icons/blueprints/fragment/medium/italy.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_JAPAN:String = "../maps/icons/blueprints/fragment/medium/japan.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_POLAND:String = "../maps/icons/blueprints/fragment/medium/poland.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_SWEDEN:String = "../maps/icons/blueprints/fragment/medium/sweden.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_UK:String = "../maps/icons/blueprints/fragment/medium/uk.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_USA:String = "../maps/icons/blueprints/fragment/medium/usa.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_USSR:String = "../maps/icons/blueprints/fragment/medium/ussr.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_MEDIUM_VEHICLE:String = "../maps/icons/blueprints/fragment/medium/vehicle.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_CHINA:String = "../maps/icons/blueprints/fragment/small/china.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_CZECH:String = "../maps/icons/blueprints/fragment/small/czech.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_FRANCE:String = "../maps/icons/blueprints/fragment/small/france.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_GERMANY:String = "../maps/icons/blueprints/fragment/small/germany.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_INTELLIGENCE:String = "../maps/icons/blueprints/fragment/small/intelligence.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_ITALY:String = "../maps/icons/blueprints/fragment/small/italy.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_JAPAN:String = "../maps/icons/blueprints/fragment/small/japan.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_POLAND:String = "../maps/icons/blueprints/fragment/small/poland.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_SWEDEN:String = "../maps/icons/blueprints/fragment/small/sweden.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_UK:String = "../maps/icons/blueprints/fragment/small/uk.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_USA:String = "../maps/icons/blueprints/fragment/small/usa.png";

        public static const MAPS_ICONS_BLUEPRINTS_FRAGMENT_SMALL_USSR:String = "../maps/icons/blueprints/fragment/small/ussr.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_ARROW_LEFT:String = "../maps/icons/blueprints/tooltip/arrow_left.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_ARROW_RIGHT:String = "../maps/icons/blueprints/tooltip/arrow_right.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_DISCOUNT:String = "../maps/icons/blueprints/tooltip/discount.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_FRAGMENTRIBBON:String = "../maps/icons/blueprints/tooltip/fragmentRibbon.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_PLUS:String = "../maps/icons/blueprints/tooltip/plus.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_POINTER:String = "../maps/icons/blueprints/tooltip/pointer.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_LAYOUT_1:String = "../maps/icons/blueprints/tooltip/layout/1.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_LAYOUT_10:String = "../maps/icons/blueprints/tooltip/layout/10.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_LAYOUT_12:String = "../maps/icons/blueprints/tooltip/layout/12.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_LAYOUT_4:String = "../maps/icons/blueprints/tooltip/layout/4.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_LAYOUT_6:String = "../maps/icons/blueprints/tooltip/layout/6.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_LAYOUT_8:String = "../maps/icons/blueprints/tooltip/layout/8.png";

        public static const MAPS_ICONS_BLUEPRINTS_TOOLTIP_LAYOUT_9:String = "../maps/icons/blueprints/tooltip/layout/9.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_1:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_1.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_10:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_10.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_11:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_11.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_12:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_12.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_2:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_2.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_3:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_3.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_4:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_4.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_5:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_5.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_6:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_6.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_7:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_7.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_8:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_8.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_9:String = "../maps/icons/bob/bloggers/fullPortrait/blogger_9.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_1:String = "../maps/icons/bob/bloggers/portrait/blogger_1.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_10:String = "../maps/icons/bob/bloggers/portrait/blogger_10.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_11:String = "../maps/icons/bob/bloggers/portrait/blogger_11.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_12:String = "../maps/icons/bob/bloggers/portrait/blogger_12.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_2:String = "../maps/icons/bob/bloggers/portrait/blogger_2.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_3:String = "../maps/icons/bob/bloggers/portrait/blogger_3.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_4:String = "../maps/icons/bob/bloggers/portrait/blogger_4.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_5:String = "../maps/icons/bob/bloggers/portrait/blogger_5.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_6:String = "../maps/icons/bob/bloggers/portrait/blogger_6.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_7:String = "../maps/icons/bob/bloggers/portrait/blogger_7.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_8:String = "../maps/icons/bob/bloggers/portrait/blogger_8.png";

        public static const MAPS_ICONS_BOB_BLOGGERS_PORTRAIT_BLOGGER_9:String = "../maps/icons/bob/bloggers/portrait/blogger_9.png";

        public static const MAPS_ICONS_BOB_HANGAR_PRIMETIMEVIEW_BG:String = "../maps/icons/bob/hangar/primeTimeView/bg.png";

        public static const MAPS_ICONS_BOB_HANGAR_REWARDSSCREEN_CIS_BG:String = "../maps/icons/bob/hangar/rewardsScreen/cis_bg.png";

        public static const MAPS_ICONS_BOB_HANGAR_REWARDSSCREEN_DELIMITER:String = "../maps/icons/bob/hangar/rewardsScreen/delimiter.png";

        public static const MAPS_ICONS_BOB_HANGAR_REWARDSSCREEN_OTHER_BG:String = "../maps/icons/bob/hangar/rewardsScreen/other_bg.png";

        public static const MAPS_ICONS_BOB_HANGAR_REWARDSSCREEN_PERSONALREWARDHIGHLIGHT:String = "../maps/icons/bob/hangar/rewardsScreen/personalRewardHighlight.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_1:String = "../maps/icons/bob/hangar/widget/blogger_bg_1.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_10:String = "../maps/icons/bob/hangar/widget/blogger_bg_10.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_11:String = "../maps/icons/bob/hangar/widget/blogger_bg_11.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_12:String = "../maps/icons/bob/hangar/widget/blogger_bg_12.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_2:String = "../maps/icons/bob/hangar/widget/blogger_bg_2.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_3:String = "../maps/icons/bob/hangar/widget/blogger_bg_3.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_4:String = "../maps/icons/bob/hangar/widget/blogger_bg_4.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_5:String = "../maps/icons/bob/hangar/widget/blogger_bg_5.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_6:String = "../maps/icons/bob/hangar/widget/blogger_bg_6.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_7:String = "../maps/icons/bob/hangar/widget/blogger_bg_7.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_8:String = "../maps/icons/bob/hangar/widget/blogger_bg_8.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_BLOGGER_BG_9:String = "../maps/icons/bob/hangar/widget/blogger_bg_9.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_POINTS:String = "../maps/icons/bob/hangar/widget/points.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SHADOW:String = "../maps/icons/bob/hangar/widget/shadow.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_BATTLE_RUSH:String = "../maps/icons/bob/hangar/widget/skill/battle_rush.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_DEFAULT:String = "../maps/icons/bob/hangar/widget/skill/default.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_MORALE_BOOST_1:String = "../maps/icons/bob/hangar/widget/skill/morale_boost_1.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_MORALE_BOOST_2:String = "../maps/icons/bob/hangar/widget/skill/morale_boost_2.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_MORALE_BOOST_3:String = "../maps/icons/bob/hangar/widget/skill/morale_boost_3.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_MORALE_BOOST_4:String = "../maps/icons/bob/hangar/widget/skill/morale_boost_4.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_RISKY_ATTACK_1:String = "../maps/icons/bob/hangar/widget/skill/risky_attack_1.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_RISKY_ATTACK_2:String = "../maps/icons/bob/hangar/widget/skill/risky_attack_2.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_RISKY_ATTACK_3:String = "../maps/icons/bob/hangar/widget/skill/risky_attack_3.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_RISKY_ATTACK_4:String = "../maps/icons/bob/hangar/widget/skill/risky_attack_4.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_SAFE_STRATEGY:String = "../maps/icons/bob/hangar/widget/skill/safe_strategy.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_SAVING_BATTLE:String = "../maps/icons/bob/hangar/widget/skill/saving_battle.png";

        public static const MAPS_ICONS_BOB_HANGAR_WIDGET_SKILL_TERRITORY_CONTROL:String = "../maps/icons/bob/hangar/widget/skill/territory_control.png";

        public static const MAPS_ICONS_BOOTCAMP_EMPTYBATTLESELECTORICON:String = "../maps/icons/bootcamp/emptyBattleSelectorIcon.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_CROSSHAIR_BIG:String = "../maps/icons/bootcamp/loading/crosshair_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_CROSSHAIR_BIG_CORE:String = "../maps/icons/bootcamp/loading/crosshair_big_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_CROSSHAIR_SMALL:String = "../maps/icons/bootcamp/loading/crosshair_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_CROSSHAIR_SMALL_CORE:String = "../maps/icons/bootcamp/loading/crosshair_small_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_EQUIPMENT_BIG:String = "../maps/icons/bootcamp/loading/equipment_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_EQUIPMENT_SMALL:String = "../maps/icons/bootcamp/loading/equipment_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_MODULES_BIG:String = "../maps/icons/bootcamp/loading/modules_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_MODULES_BIG_CORE:String = "../maps/icons/bootcamp/loading/modules_big_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_MODULES_SMALL:String = "../maps/icons/bootcamp/loading/modules_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_MODULES_SMALL_CORE:String = "../maps/icons/bootcamp/loading/modules_small_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_MOVEMENT_BIG:String = "../maps/icons/bootcamp/loading/movement_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_MOVEMENT_SMALL:String = "../maps/icons/bootcamp/loading/movement_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_PENETRATION_BIG:String = "../maps/icons/bootcamp/loading/penetration_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_PENETRATION_BIG_CORE:String = "../maps/icons/bootcamp/loading/penetration_big_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_PENETRATION_SMALL:String = "../maps/icons/bootcamp/loading/penetration_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_PENETRATION_SMALL_CORE:String = "../maps/icons/bootcamp/loading/penetration_small_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_SNIPER_BIG:String = "../maps/icons/bootcamp/loading/sniper_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_SNIPER_BIG_CORE:String = "../maps/icons/bootcamp/loading/sniper_big_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_SNIPER_SMALL:String = "../maps/icons/bootcamp/loading/sniper_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_SNIPER_SMALL_CORE:String = "../maps/icons/bootcamp/loading/sniper_small_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VICTORY_BIG:String = "../maps/icons/bootcamp/loading/victory_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VICTORY_BIG_CORE:String = "../maps/icons/bootcamp/loading/victory_big_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VICTORY_SMALL:String = "../maps/icons/bootcamp/loading/victory_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VICTORY_SMALL_CORE:String = "../maps/icons/bootcamp/loading/victory_small_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VISIBILITY_BIG:String = "../maps/icons/bootcamp/loading/visibility_big.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VISIBILITY_BIG_CORE:String = "../maps/icons/bootcamp/loading/visibility_big_core.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VISIBILITY_SMALL:String = "../maps/icons/bootcamp/loading/visibility_small.png";

        public static const MAPS_ICONS_BOOTCAMP_LOADING_VISIBILITY_SMALL_CORE:String = "../maps/icons/bootcamp/loading/visibility_small_core.png";

        public static const MAPS_ICONS_BOOTCAMP_MENU_MENUBOOTCAMPICON:String = "../maps/icons/bootcamp/menu/menuBootcampIcon.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCAWARDOPTIONS:String = "../maps/icons/bootcamp/rewards/bcAwardOptions.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCCONSUMABLES:String = "../maps/icons/bootcamp/rewards/bcConsumables.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCCREDITS:String = "../maps/icons/bootcamp/rewards/bcCredits.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCCREW:String = "../maps/icons/bootcamp/rewards/bcCrew.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCDUELIST:String = "../maps/icons/bootcamp/rewards/bcDuelist.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCENGINEL2:String = "../maps/icons/bootcamp/rewards/bcEngineL2.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCENGINEL3:String = "../maps/icons/bootcamp/rewards/bcEngineL3.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCEXP:String = "../maps/icons/bootcamp/rewards/bcExp.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCGOLD:String = "../maps/icons/bootcamp/rewards/bcGold.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCHANDEXTINGUISHERS:String = "../maps/icons/bootcamp/rewards/bchandExtinguishers.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCINVADER:String = "../maps/icons/bootcamp/rewards/bcInvader.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCLARGEMEDKIT:String = "../maps/icons/bootcamp/rewards/bclargeMedkit.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCLARGEREPAIRKIT:String = "../maps/icons/bootcamp/rewards/bclargeRepairkit.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCMAINGUN:String = "../maps/icons/bootcamp/rewards/bcMaingun.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCMISSION:String = "../maps/icons/bootcamp/rewards/bcMission.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCPREMIUM:String = "../maps/icons/bootcamp/rewards/bcPremium.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCPREMIUM3D:String = "../maps/icons/bootcamp/rewards/bcPremium3d.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCPREMIUMPLUS:String = "../maps/icons/bootcamp/rewards/bcPremiumPlus.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCPREMIUMPLUS3D:String = "../maps/icons/bootcamp/rewards/bcPremiumPlus3d.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCPZ35T:String = "../maps/icons/bootcamp/rewards/bcPz35t.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCSHOOTTOKILL:String = "../maps/icons/bootcamp/rewards/bcShoottokill.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCSIXSENCE:String = "../maps/icons/bootcamp/rewards/bcSixSence.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCSNIPER:String = "../maps/icons/bootcamp/rewards/bcSniper.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCT26:String = "../maps/icons/bootcamp/rewards/bcT26.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCT2_MED:String = "../maps/icons/bootcamp/rewards/bcT2_med.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_BCTOOLBOX:String = "../maps/icons/bootcamp/rewards/bctoolbox.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_NATIONSSELECTGE:String = "../maps/icons/bootcamp/rewards/nationsSelectGE.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_NATIONSSELECTUSA:String = "../maps/icons/bootcamp/rewards/nationsSelectUSA.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_NATIONSSELECTUSSR:String = "../maps/icons/bootcamp/rewards/nationsSelectUSSR.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCAWARDOPTIONS:String = "../maps/icons/bootcamp/rewards/tooltips/bcAwardOptions.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCCONSUMABLES:String = "../maps/icons/bootcamp/rewards/tooltips/bcConsumables.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCCREDITS:String = "../maps/icons/bootcamp/rewards/tooltips/bcCredits.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCCREW:String = "../maps/icons/bootcamp/rewards/tooltips/bcCrew.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCDUELIST:String = "../maps/icons/bootcamp/rewards/tooltips/bcDuelist.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCEXP:String = "../maps/icons/bootcamp/rewards/tooltips/bcExp.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCFIREEX:String = "../maps/icons/bootcamp/rewards/tooltips/bcFireEx.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCGOLD:String = "../maps/icons/bootcamp/rewards/tooltips/bcGold.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCINVADER:String = "../maps/icons/bootcamp/rewards/tooltips/bcInvader.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCMAINGUN:String = "../maps/icons/bootcamp/rewards/tooltips/bcMaingun.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCMEDBIG:String = "../maps/icons/bootcamp/rewards/tooltips/bcMedBig.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCMISSION:String = "../maps/icons/bootcamp/rewards/tooltips/bcMission.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCPREMIUM:String = "../maps/icons/bootcamp/rewards/tooltips/bcPremium.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCPREMIUM3D:String = "../maps/icons/bootcamp/rewards/tooltips/bcPremium3d.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCPREMIUMPLUS:String = "../maps/icons/bootcamp/rewards/tooltips/bcPremiumPlus.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCPREMIUMPLUS3D:String = "../maps/icons/bootcamp/rewards/tooltips/bcPremiumPlus3d.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCREMBIG:String = "../maps/icons/bootcamp/rewards/tooltips/bcRemBig.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCSHOOTTOKILL:String = "../maps/icons/bootcamp/rewards/tooltips/bcShoottokill.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCSNIPER:String = "../maps/icons/bootcamp/rewards/tooltips/bcSniper.png";

        public static const MAPS_ICONS_BOOTCAMP_REWARDS_TOOLTIPS_BCTOOLS:String = "../maps/icons/bootcamp/rewards/tooltips/bcTools.png";

        public static const MAPS_ICONS_BUTTONS_BOOKMARK:String = "../maps/icons/buttons/bookmark.png";

        public static const MAPS_ICONS_BUTTONS_BUTTON_BACK_ARROW:String = "../maps/icons/buttons/button_back_arrow.png";

        public static const MAPS_ICONS_BUTTONS_BUTTON_NEXT_ARROW:String = "../maps/icons/buttons/button_next_arrow.png";

        public static const MAPS_ICONS_BUTTONS_BUTTON_PLAY2:String = "../maps/icons/buttons/button_play2.png";

        public static const MAPS_ICONS_BUTTONS_CALENDAR:String = "../maps/icons/buttons/calendar.png";

        public static const MAPS_ICONS_BUTTONS_CHECKMARK:String = "../maps/icons/buttons/checkmark.png";

        public static const MAPS_ICONS_BUTTONS_CLAN_LIST:String = "../maps/icons/buttons/clan_list.png";

        public static const MAPS_ICONS_BUTTONS_CLAN_STATS:String = "../maps/icons/buttons/clan_stats.png";

        public static const MAPS_ICONS_BUTTONS_DESTROY:String = "../maps/icons/buttons/destroy.png";

        public static const MAPS_ICONS_BUTTONS_ENTERWHITE:String = "../maps/icons/buttons/enterWhite.png";

        public static const MAPS_ICONS_BUTTONS_ENVELOPE:String = "../maps/icons/buttons/envelope.png";

        public static const MAPS_ICONS_BUTTONS_ENVELOPEOPENED:String = "../maps/icons/buttons/envelopeOpened.png";

        public static const MAPS_ICONS_BUTTONS_EQUIPPED_ICON:String = "../maps/icons/buttons/equipped_icon.png";

        public static const MAPS_ICONS_BUTTONS_EXPLORE:String = "../maps/icons/buttons/explore.png";

        public static const MAPS_ICONS_BUTTONS_EXP_GATHERING:String = "../maps/icons/buttons/exp_gathering.png";

        public static const MAPS_ICONS_BUTTONS_FILTER:String = "../maps/icons/buttons/filter.png";

        public static const MAPS_ICONS_BUTTONS_FOOTHOLD:String = "../maps/icons/buttons/foothold.png";

        public static const MAPS_ICONS_BUTTONS_GOLD_EXCHANGING:String = "../maps/icons/buttons/gold_exchanging.png";

        public static const MAPS_ICONS_BUTTONS_HORN:String = "../maps/icons/buttons/Horn.png";

        public static const MAPS_ICONS_BUTTONS_ICONDESTROY:String = "../maps/icons/buttons/iconDestroy.png";

        public static const MAPS_ICONS_BUTTONS_ICONDOWN:String = "../maps/icons/buttons/iconDown.png";

        public static const MAPS_ICONS_BUTTONS_ICONUP:String = "../maps/icons/buttons/iconUp.png";

        public static const MAPS_ICONS_BUTTONS_ICONUPGRADE:String = "../maps/icons/buttons/iconUpgrade.png";

        public static const MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_LEFT:String = "../maps/icons/buttons/icon_arrow_formation_left.png";

        public static const MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_LEFT_DOUBLE:String = "../maps/icons/buttons/icon_arrow_formation_left_double.png";

        public static const MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_RIGHT:String = "../maps/icons/buttons/icon_arrow_formation_right.png";

        public static const MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_RIGHT_DOUBLE:String = "../maps/icons/buttons/icon_arrow_formation_right_double.png";

        public static const MAPS_ICONS_BUTTONS_ICON_BUTTON_ARROW:String = "../maps/icons/buttons/icon_button_arrow.png";

        public static const MAPS_ICONS_BUTTONS_ICON_CHANNELS_HIDE_SYMBOLS:String = "../maps/icons/buttons/icon_channels_hide_symbols.png";

        public static const MAPS_ICONS_BUTTONS_ICON_CHANNELS_SHOW_SYMBOLS_097:String = "../maps/icons/buttons/icon_channels_show_symbols_097.png";

        public static const MAPS_ICONS_BUTTONS_ICON_TABLE_COMPARISON_CHECKMARK:String = "../maps/icons/buttons/icon_table_comparison_checkmark.png";

        public static const MAPS_ICONS_BUTTONS_ICON_TABLE_COMPARISON_INHANGAR:String = "../maps/icons/buttons/icon_table_comparison_inhangar.png";

        public static const MAPS_ICONS_BUTTONS_LEVEL_UP:String = "../maps/icons/buttons/level_up.png";

        public static const MAPS_ICONS_BUTTONS_MAINMENU:String = "../maps/icons/buttons/mainMenu.png";

        public static const MAPS_ICONS_BUTTONS_MAITENANCE:String = "../maps/icons/buttons/maitenance.png";

        public static const MAPS_ICONS_BUTTONS_NATION_CHANGE:String = "../maps/icons/buttons/nation_change.png";

        public static const MAPS_ICONS_BUTTONS_NC_ICON_MEDIUM:String = "../maps/icons/buttons/nc_icon_medium.png";

        public static const MAPS_ICONS_BUTTONS_NEXTPAGE:String = "../maps/icons/buttons/nextPage.png";

        public static const MAPS_ICONS_BUTTONS_NON_HISTORICAL:String = "../maps/icons/buttons/non_historical.png";

        public static const MAPS_ICONS_BUTTONS_PAUSE:String = "../maps/icons/buttons/pause.png";

        public static const MAPS_ICONS_BUTTONS_PLAY:String = "../maps/icons/buttons/play.png";

        public static const MAPS_ICONS_BUTTONS_PLAYERS:String = "../maps/icons/buttons/Players.png";

        public static const MAPS_ICONS_BUTTONS_PREMIUM:String = "../maps/icons/buttons/premium.png";

        public static const MAPS_ICONS_BUTTONS_PREVIOUSPAGE:String = "../maps/icons/buttons/previousPage.png";

        public static const MAPS_ICONS_BUTTONS_REFILL_ACCOUNT:String = "../maps/icons/buttons/refill_account.png";

        public static const MAPS_ICONS_BUTTONS_REFRESH:String = "../maps/icons/buttons/refresh.png";

        public static const MAPS_ICONS_BUTTONS_REMOVE:String = "../maps/icons/buttons/remove.png";

        public static const MAPS_ICONS_BUTTONS_REMOVEGOLD:String = "../maps/icons/buttons/removeGold.png";

        public static const MAPS_ICONS_BUTTONS_REVERT_24X24:String = "../maps/icons/buttons/revert_24x24.png";

        public static const MAPS_ICONS_BUTTONS_SEARCH:String = "../maps/icons/buttons/search.png";

        public static const MAPS_ICONS_BUTTONS_SELECTORRENDERERBGEVENT:String = "../maps/icons/buttons/selectorRendererBGEvent.png";

        public static const MAPS_ICONS_BUTTONS_SETTINGS:String = "../maps/icons/buttons/settings.png";

        public static const MAPS_ICONS_BUTTONS_SMALLATTACKICON:String = "../maps/icons/buttons/smallAttackIcon.png";

        public static const MAPS_ICONS_BUTTONS_SOUND:String = "../maps/icons/buttons/sound.png";

        public static const MAPS_ICONS_BUTTONS_SPEAKER_SMALL:String = "../maps/icons/buttons/speaker_small.png";

        public static const MAPS_ICONS_BUTTONS_SPEAKER_SUB:String = "../maps/icons/buttons/speaker_sub.png";

        public static const MAPS_ICONS_BUTTONS_SWAP:String = "../maps/icons/buttons/swap.png";

        public static const MAPS_ICONS_BUTTONS_SWAP2:String = "../maps/icons/buttons/swap2.png";

        public static const MAPS_ICONS_BUTTONS_SWAP3:String = "../maps/icons/buttons/swap3.png";

        public static const MAPS_ICONS_BUTTONS_SWAP3_ROTATED:String = "../maps/icons/buttons/swap3_rotated.png";

        public static const MAPS_ICONS_BUTTONS_TANK_ICO:String = "../maps/icons/buttons/Tank-ico.png";

        public static const MAPS_ICONS_BUTTONS_TANKMAN_SKILLS_DROP:String = "../maps/icons/buttons/tankman_skills_drop.png";

        public static const MAPS_ICONS_BUTTONS_TIMER:String = "../maps/icons/buttons/Timer.png";

        public static const MAPS_ICONS_BUTTONS_TRANSPORTING:String = "../maps/icons/buttons/transporting.png";

        public static const MAPS_ICONS_BUTTONS_TRANSPORTINGARROW:String = "../maps/icons/buttons/transportingArrow.png";

        public static const MAPS_ICONS_BUTTONS_TUNING:String = "../maps/icons/buttons/tuning.png";

        public static const MAPS_ICONS_BUTTONS_VEHICLE_FILTER:String = "../maps/icons/buttons/Vehicle-Filter.png";

        public static const MAPS_ICONS_BUTTONS_VEHICLECOMPAREBTN:String = "../maps/icons/buttons/vehicleCompareBtn.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_FACEBOOK:String = "../maps/icons/buttons/social/facebook.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_GOOGLE:String = "../maps/icons/buttons/social/google.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_NAVER:String = "../maps/icons/buttons/social/naver.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_VKONTAKTE:String = "../maps/icons/buttons/social/vkontakte.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_WGNI:String = "../maps/icons/buttons/social/wgni.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_YAHOO:String = "../maps/icons/buttons/social/yahoo.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_COLOR_FACEBOOK:String = "../maps/icons/buttons/social/color/facebook.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_COLOR_GOOGLE:String = "../maps/icons/buttons/social/color/google.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_COLOR_NAVER:String = "../maps/icons/buttons/social/color/naver.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_COLOR_VKONTAKTE:String = "../maps/icons/buttons/social/color/vkontakte.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_COLOR_WGNI:String = "../maps/icons/buttons/social/color/wgni.png";

        public static const MAPS_ICONS_BUTTONS_SOCIAL_COLOR_YAHOO:String = "../maps/icons/buttons/social/color/yahoo.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ASCENDINGSORTARROW:String = "../maps/icons/buttons/tab_sort_button/ascendingSortArrow.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ASCPROFILESORTARROW:String = "../maps/icons/buttons/tab_sort_button/ascProfileSortArrow.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DAMAGE:String = "../maps/icons/buttons/tab_sort_button/damage.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DEATHS:String = "../maps/icons/buttons/tab_sort_button/deaths.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DESCENDINGSORTARROW:String = "../maps/icons/buttons/tab_sort_button/descendingSortArrow.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DESCPROFILESORTARROW:String = "../maps/icons/buttons/tab_sort_button/descProfileSortArrow.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FALLOUTRESOURCEPOINTS:String = "../maps/icons/buttons/tab_sort_button/falloutResourcePoints.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FLAG:String = "../maps/icons/buttons/tab_sort_button/flag.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FRAG:String = "../maps/icons/buttons/tab_sort_button/frag.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_HEALTH:String = "../maps/icons/buttons/tab_sort_button/health.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_LEVEL:String = "../maps/icons/buttons/tab_sort_button/level.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_LEVEL_6_8:String = "../maps/icons/buttons/tab_sort_button/level_6_8.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_MEDAL:String = "../maps/icons/buttons/tab_sort_button/medal.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_NOTAVAILABLE:String = "../maps/icons/buttons/tab_sort_button/notAvailable.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_OK:String = "../maps/icons/buttons/tab_sort_button/ok.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYER:String = "../maps/icons/buttons/tab_sort_button/player.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYERNUMBER:String = "../maps/icons/buttons/tab_sort_button/playerNumber.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYERRANK:String = "../maps/icons/buttons/tab_sort_button/playerRank.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RANK:String = "../maps/icons/buttons/tab_sort_button/rank.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RESOURCECOUNT:String = "../maps/icons/buttons/tab_sort_button/resourceCount.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RESPAWN:String = "../maps/icons/buttons/tab_sort_button/respawn.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_SQUAD:String = "../maps/icons/buttons/tab_sort_button/squad.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_STAR_RED:String = "../maps/icons/buttons/tab_sort_button/star_red.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TABLEHEADERBG:String = "../maps/icons/buttons/tab_sort_button/tableHeaderBg.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TABLEHEADERSORTEDBG:String = "../maps/icons/buttons/tab_sort_button/tableHeaderSortedBg.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TANK:String = "../maps/icons/buttons/tab_sort_button/tank.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TEAMSCORE:String = "../maps/icons/buttons/tab_sort_button/teamScore.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_VICTORYSCORE:String = "../maps/icons/buttons/tab_sort_button/victoryScore.png";

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_XP:String = "../maps/icons/buttons/tab_sort_button/xp.png";

        public static const MAPS_ICONS_CLANS_CLAN_CARD_HEADER:String = "../maps/icons/clans/clan_card_header.png";

        public static const MAPS_ICONS_CLANS_DEFAULT_CLAN_ICON128X128:String = "../maps/icons/clans/default_clan_icon128x128.png";

        public static const MAPS_ICONS_CLANS_DEFAULT_CLAN_ICON16X16:String = "../maps/icons/clans/default_clan_icon16x16.png";

        public static const MAPS_ICONS_CLANS_DEFAULT_CLAN_ICON256X256:String = "../maps/icons/clans/default_clan_icon256x256.png";

        public static const MAPS_ICONS_CLANS_DEFAULT_CLAN_ICON32X32:String = "../maps/icons/clans/default_clan_icon32x32.png";

        public static const MAPS_ICONS_CLANS_GLOBAL_MAP_PROMO:String = "../maps/icons/clans/global_map_promo.png";

        public static const MAPS_ICONS_CLANS_NO_CLAN_ICON32X32:String = "../maps/icons/clans/no_clan_icon32x32.png";

        public static const MAPS_ICONS_CLANS_PIC_CLAN_IVITATION_BACK:String = "../maps/icons/clans/pic_clan_ivitation_back.png";

        public static const MAPS_ICONS_CLANS_TEXTFIELDRECT:String = "../maps/icons/clans/textFieldRect.png";

        public static const MAPS_ICONS_CLANS_INVITESWINDOW_CC_HEADER_BACK:String = "../maps/icons/clans/invitesWindow/cc_header_back.png";

        public static const MAPS_ICONS_CLANS_INVITESWINDOW_CROSS:String = "../maps/icons/clans/invitesWindow/cross.png";

        public static const MAPS_ICONS_CLANS_INVITESWINDOW_ICON_INVITE_MESSAGE:String = "../maps/icons/clans/invitesWindow/icon_invite_message.png";

        public static const MAPS_ICONS_CLANS_INVITESWINDOW_ICON_STATISTICS_CLAN_INVITE_098:String = "../maps/icons/clans/invitesWindow/icon_statistics_clan_invite_098.png";

        public static const MAPS_ICONS_CLANS_INVITESWINDOW_PERSONAL_INVITES_HEADER_BACK:String = "../maps/icons/clans/invitesWindow/personal_invites_header_back.png";

        public static const MAPS_ICONS_CLANS_LEAGUES_DEFAULT_10:String = "../maps/icons/clans/leagues/default_10.png";

        public static const MAPS_ICONS_CLANS_LEAGUES_DEFAULT_6:String = "../maps/icons/clans/leagues/default_6.png";

        public static const MAPS_ICONS_CLANS_LEAGUES_DEFAULT_8:String = "../maps/icons/clans/leagues/default_8.png";

        public static const MAPS_ICONS_CLANS_ROLES_COMMANDER:String = "../maps/icons/clans/roles/commander.png";

        public static const MAPS_ICONS_CLANS_ROLES_DIPLOMAT:String = "../maps/icons/clans/roles/diplomat.png";

        public static const MAPS_ICONS_CLANS_ROLES_JUNIOR:String = "../maps/icons/clans/roles/junior.png";

        public static const MAPS_ICONS_CLANS_ROLES_LEADER:String = "../maps/icons/clans/roles/leader.png";

        public static const MAPS_ICONS_CLANS_ROLES_PRIVATE:String = "../maps/icons/clans/roles/private.png";

        public static const MAPS_ICONS_CLANS_ROLES_RECRUIT:String = "../maps/icons/clans/roles/recruit.png";

        public static const MAPS_ICONS_CLANS_ROLES_RECRUITER:String = "../maps/icons/clans/roles/recruiter.png";

        public static const MAPS_ICONS_CLANS_ROLES_RESERVIST:String = "../maps/icons/clans/roles/reservist.png";

        public static const MAPS_ICONS_CLANS_ROLES_STAFF:String = "../maps/icons/clans/roles/staff.png";

        public static const MAPS_ICONS_CLANS_ROLES_TREASURER:String = "../maps/icons/clans/roles/treasurer.png";

        public static const MAPS_ICONS_CLANS_ROLES_VICE_LEADER:String = "../maps/icons/clans/roles/vice_leader.png";

        public static const MAPS_ICONS_COMPONENTS_BUTTON_STANDARD_TEXTURE:String = "../maps/icons/components/button/standard_texture.png";

        public static const MAPS_ICONS_COMPONENTS_BUTTON_TEXTURE_DISABLE:String = "../maps/icons/components/button/texture_disable.png";

        public static const MAPS_ICONS_COMPONENTS_CHECKBOX_CHECK:String = "../maps/icons/components/checkbox/check.png";

        public static const MAPS_ICONS_COMPONENTS_CHECKBOX_CHECKBOX:String = "../maps/icons/components/checkbox/checkbox.png";

        public static const MAPS_ICONS_COMPONENTS_CHECKBOX_CHECKBOX_CHECKED:String = "../maps/icons/components/checkbox/checkbox_checked.png";

        public static const MAPS_ICONS_COMPONENTS_CHECKBOX_CHECKBOX_M:String = "../maps/icons/components/checkbox/checkbox_m.png";

        public static const MAPS_ICONS_COMPONENTS_CHECKBOX_CHECKBOX_M_GREEN:String = "../maps/icons/components/checkbox/checkbox_m_green.png";

        public static const MAPS_ICONS_COMPONENTS_CHECKBOX_CHECK_MAIN_MEDIUM:String = "../maps/icons/components/checkbox/check_main_medium.png";

        public static const MAPS_ICONS_COMPONENTS_COUNTER_PATTERN:String = "../maps/icons/components/counter/pattern.png";

        public static const MAPS_ICONS_COMPONENTS_NUMERICAL_STEPPER_STEPPER_LARGE_BUTTONS:String = "../maps/icons/components/numerical_stepper/stepper_large_buttons.png";

        public static const MAPS_ICONS_COMPONENTS_NUMERICAL_STEPPER_STEPPER_MEDIUM_BUTTONS:String = "../maps/icons/components/numerical_stepper/stepper_medium_buttons.png";

        public static const MAPS_ICONS_COMPONENTS_NUMERICAL_STEPPER_STEPPER_SMALL_BUTTONS:String = "../maps/icons/components/numerical_stepper/stepper_small_buttons.png";

        public static const MAPS_ICONS_COMPONENTS_PROGRESS_BAR_GLOW:String = "../maps/icons/components/progress_bar/glow.png";

        public static const MAPS_ICONS_COMPONENTS_PROGRESS_BAR_GLOW_SMALL:String = "../maps/icons/components/progress_bar/glow_small.png";

        public static const MAPS_ICONS_COMPONENTS_SCROLL_AREA_ARROW_DOWN:String = "../maps/icons/components/scroll_area/arrow_down.png";

        public static const MAPS_ICONS_COMPONENTS_SCROLL_AREA_ARROW_TOP:String = "../maps/icons/components/scroll_area/arrow_top.png";

        public static const MAPS_ICONS_COMPONENTS_SCROLL_AREA_BACKGROUND:String = "../maps/icons/components/scroll_area/background.png";

        public static const MAPS_ICONS_COMPONENTS_SCROLL_AREA_THUMB:String = "../maps/icons/components/scroll_area/thumb.png";

        public static const MAPS_ICONS_COMPONENTS_TABS_TAB_BORDER:String = "../maps/icons/components/tabs/tab/border.png";

        public static const MAPS_ICONS_COMPONENTS_TABS_TAB_HIGHLIGHT:String = "../maps/icons/components/tabs/tab/highlight.png";

        public static const MAPS_ICONS_COMPONENTS_TABS_TAB_SEPARATOR:String = "../maps/icons/components/tabs/tab/separator.png";

        public static const MAPS_ICONS_COMPONENTS_TOOLTIP_CREDITS:String = "../maps/icons/components/tooltip/credits.png";

        public static const MAPS_ICONS_COMPONENTS_TOOLTIP_HIGHLIGHTS_RED:String = "../maps/icons/components/tooltip/highlights_red.png";

        public static const MAPS_ICONS_COMPONENTS_TOOLTIP_ICONCONTACTS:String = "../maps/icons/components/tooltip/iconContacts.png";

        public static const MAPS_ICONS_COMPONENTS_TOOLTIP_TOOLTIP_BG:String = "../maps/icons/components/tooltip/tooltip_bg.png";

        public static const MAPS_ICONS_COMPONENTS_TOOLTIP_TOOL_TIP_SEPARATOR:String = "../maps/icons/components/tooltip/tool_tip_separator.png";

        public static const MAPS_ICONS_COMPONENTS_TOOLTIP_WHITE_BG:String = "../maps/icons/components/tooltip/white_bg.png";

        public static const MAPS_ICONS_COMPONENTS_TOOLTIP_XPICONBIG:String = "../maps/icons/components/tooltip/XpIconBig.png";

        public static const MAPS_ICONS_COMPONENTS_TUTORIAL_ARROW:String = "../maps/icons/components/tutorial/arrow.png";

        public static const MAPS_ICONS_COMPONENTS_TUTORIAL_BACK:String = "../maps/icons/components/tutorial/back.png";

        public static const MAPS_ICONS_COMPONENTS_TUTORIAL_BORDER:String = "../maps/icons/components/tutorial/border.png";

        public static const MAPS_ICONS_CREWBOOKS_BREAK_LINE:String = "../maps/icons/crewBooks/break_line.png";

        public static const MAPS_ICONS_CREWBOOKS_LIGHTSMALL_ADD:String = "../maps/icons/crewBooks/lightSmall_add.png";

        public static const MAPS_ICONS_CREWBOOKS_LIGHTSMALL_ADD_USE_DIALOG:String = "../maps/icons/crewBooks/lightSmall_add_use_dialog.png";

        public static const MAPS_ICONS_CREWBOOKS_SCREEN_BG:String = "../maps/icons/crewBooks/screen_bg.png";

        public static const MAPS_ICONS_CREWBOOKS_USE_DIALOG:String = "../maps/icons/crewBooks/use_dialog.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_BACK:String = "../maps/icons/crewBooks/bookItem/back.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_BACK_SMALL:String = "../maps/icons/crewBooks/bookItem/back_small.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_BUY_BTN_LINE:String = "../maps/icons/crewBooks/bookItem/buy_btn_line.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_DISABLED:String = "../maps/icons/crewBooks/bookItem/disabled.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_DISABLED_SMALL:String = "../maps/icons/crewBooks/bookItem/disabled_small.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_FRAME:String = "../maps/icons/crewBooks/bookItem/frame.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_FRAME_SELECTED:String = "../maps/icons/crewBooks/bookItem/frame_selected.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_FRAME_SELECTED_SMALL:String = "../maps/icons/crewBooks/bookItem/frame_selected_small.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_FRAME_SMALL:String = "../maps/icons/crewBooks/bookItem/frame_small.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_HOVER:String = "../maps/icons/crewBooks/bookItem/hover.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKITEM_HOVER_SMALL:String = "../maps/icons/crewBooks/bookItem/hover_small.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_CHINA:String = "../maps/icons/crewBooks/books/big/brochure_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_CZECH:String = "../maps/icons/crewBooks/books/big/brochure_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_FRANCE:String = "../maps/icons/crewBooks/books/big/brochure_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_GERMANY:String = "../maps/icons/crewBooks/books/big/brochure_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_ITALY:String = "../maps/icons/crewBooks/books/big/brochure_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_JAPAN:String = "../maps/icons/crewBooks/books/big/brochure_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_POLAND:String = "../maps/icons/crewBooks/books/big/brochure_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_SWEDEN:String = "../maps/icons/crewBooks/books/big/brochure_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_UK:String = "../maps/icons/crewBooks/books/big/brochure_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_USA:String = "../maps/icons/crewBooks/books/big/brochure_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_BROCHURE_USSR:String = "../maps/icons/crewBooks/books/big/brochure_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_CHINA:String = "../maps/icons/crewBooks/books/big/crewBook_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_CZECH:String = "../maps/icons/crewBooks/books/big/crewBook_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_FRANCE:String = "../maps/icons/crewBooks/books/big/crewBook_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_GERMANY:String = "../maps/icons/crewBooks/books/big/crewBook_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_ITALY:String = "../maps/icons/crewBooks/books/big/crewBook_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_JAPAN:String = "../maps/icons/crewBooks/books/big/crewBook_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_POLAND:String = "../maps/icons/crewBooks/books/big/crewBook_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_SWEDEN:String = "../maps/icons/crewBooks/books/big/crewBook_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_UK:String = "../maps/icons/crewBooks/books/big/crewBook_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_USA:String = "../maps/icons/crewBooks/books/big/crewBook_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_CREWBOOK_USSR:String = "../maps/icons/crewBooks/books/big/crewBook_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_CHINA:String = "../maps/icons/crewBooks/books/big/guide_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_CZECH:String = "../maps/icons/crewBooks/books/big/guide_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_FRANCE:String = "../maps/icons/crewBooks/books/big/guide_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_GERMANY:String = "../maps/icons/crewBooks/books/big/guide_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_ITALY:String = "../maps/icons/crewBooks/books/big/guide_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_JAPAN:String = "../maps/icons/crewBooks/books/big/guide_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_POLAND:String = "../maps/icons/crewBooks/books/big/guide_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_SWEDEN:String = "../maps/icons/crewBooks/books/big/guide_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_UK:String = "../maps/icons/crewBooks/books/big/guide_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_USA:String = "../maps/icons/crewBooks/books/big/guide_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_GUIDE_USSR:String = "../maps/icons/crewBooks/books/big/guide_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_PERSONALBOOK:String = "../maps/icons/crewBooks/books/big/personalBook.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_BIG_UNIVERSALBOOK:String = "../maps/icons/crewBooks/books/big/universalBook.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_CHINA:String = "../maps/icons/crewBooks/books/large/brochure_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_CZECH:String = "../maps/icons/crewBooks/books/large/brochure_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_FRANCE:String = "../maps/icons/crewBooks/books/large/brochure_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_GERMANY:String = "../maps/icons/crewBooks/books/large/brochure_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_ITALY:String = "../maps/icons/crewBooks/books/large/brochure_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_JAPAN:String = "../maps/icons/crewBooks/books/large/brochure_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_POLAND:String = "../maps/icons/crewBooks/books/large/brochure_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_SWEDEN:String = "../maps/icons/crewBooks/books/large/brochure_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_UK:String = "../maps/icons/crewBooks/books/large/brochure_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_USA:String = "../maps/icons/crewBooks/books/large/brochure_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_BROCHURE_USSR:String = "../maps/icons/crewBooks/books/large/brochure_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_CHINA:String = "../maps/icons/crewBooks/books/large/crewBook_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_CZECH:String = "../maps/icons/crewBooks/books/large/crewBook_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_FRANCE:String = "../maps/icons/crewBooks/books/large/crewBook_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_GERMANY:String = "../maps/icons/crewBooks/books/large/crewBook_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_ITALY:String = "../maps/icons/crewBooks/books/large/crewBook_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_JAPAN:String = "../maps/icons/crewBooks/books/large/crewBook_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_POLAND:String = "../maps/icons/crewBooks/books/large/crewBook_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_SWEDEN:String = "../maps/icons/crewBooks/books/large/crewBook_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_UK:String = "../maps/icons/crewBooks/books/large/crewBook_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_USA:String = "../maps/icons/crewBooks/books/large/crewBook_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_CREWBOOK_USSR:String = "../maps/icons/crewBooks/books/large/crewBook_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_CHINA:String = "../maps/icons/crewBooks/books/large/guide_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_CZECH:String = "../maps/icons/crewBooks/books/large/guide_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_FRANCE:String = "../maps/icons/crewBooks/books/large/guide_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_GERMANY:String = "../maps/icons/crewBooks/books/large/guide_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_ITALY:String = "../maps/icons/crewBooks/books/large/guide_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_JAPAN:String = "../maps/icons/crewBooks/books/large/guide_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_POLAND:String = "../maps/icons/crewBooks/books/large/guide_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_SWEDEN:String = "../maps/icons/crewBooks/books/large/guide_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_UK:String = "../maps/icons/crewBooks/books/large/guide_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_USA:String = "../maps/icons/crewBooks/books/large/guide_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_GUIDE_USSR:String = "../maps/icons/crewBooks/books/large/guide_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_PERSONALBOOK:String = "../maps/icons/crewBooks/books/large/personalBook.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_LARGE_UNIVERSALBOOK:String = "../maps/icons/crewBooks/books/large/universalBook.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_CHINA:String = "../maps/icons/crewBooks/books/small/brochure_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_CZECH:String = "../maps/icons/crewBooks/books/small/brochure_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_FRANCE:String = "../maps/icons/crewBooks/books/small/brochure_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_GERMANY:String = "../maps/icons/crewBooks/books/small/brochure_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_ITALY:String = "../maps/icons/crewBooks/books/small/brochure_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_JAPAN:String = "../maps/icons/crewBooks/books/small/brochure_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_POLAND:String = "../maps/icons/crewBooks/books/small/brochure_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_SWEDEN:String = "../maps/icons/crewBooks/books/small/brochure_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_UK:String = "../maps/icons/crewBooks/books/small/brochure_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_USA:String = "../maps/icons/crewBooks/books/small/brochure_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_BROCHURE_USSR:String = "../maps/icons/crewBooks/books/small/brochure_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_CHINA:String = "../maps/icons/crewBooks/books/small/crewBook_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_CZECH:String = "../maps/icons/crewBooks/books/small/crewBook_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_FRANCE:String = "../maps/icons/crewBooks/books/small/crewBook_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_GERMANY:String = "../maps/icons/crewBooks/books/small/crewBook_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_ITALY:String = "../maps/icons/crewBooks/books/small/crewBook_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_JAPAN:String = "../maps/icons/crewBooks/books/small/crewBook_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_POLAND:String = "../maps/icons/crewBooks/books/small/crewBook_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_SWEDEN:String = "../maps/icons/crewBooks/books/small/crewBook_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_UK:String = "../maps/icons/crewBooks/books/small/crewBook_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_USA:String = "../maps/icons/crewBooks/books/small/crewBook_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_CREWBOOK_USSR:String = "../maps/icons/crewBooks/books/small/crewBook_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_CHINA:String = "../maps/icons/crewBooks/books/small/guide_china.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_CZECH:String = "../maps/icons/crewBooks/books/small/guide_czech.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_FRANCE:String = "../maps/icons/crewBooks/books/small/guide_france.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_GERMANY:String = "../maps/icons/crewBooks/books/small/guide_germany.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_ITALY:String = "../maps/icons/crewBooks/books/small/guide_italy.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_JAPAN:String = "../maps/icons/crewBooks/books/small/guide_japan.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_POLAND:String = "../maps/icons/crewBooks/books/small/guide_poland.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_SWEDEN:String = "../maps/icons/crewBooks/books/small/guide_sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_UK:String = "../maps/icons/crewBooks/books/small/guide_uk.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_USA:String = "../maps/icons/crewBooks/books/small/guide_usa.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_GUIDE_USSR:String = "../maps/icons/crewBooks/books/small/guide_ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_PERSONALBOOK:String = "../maps/icons/crewBooks/books/small/personalBook.png";

        public static const MAPS_ICONS_CREWBOOKS_BOOKS_SMALL_UNIVERSALBOOK:String = "../maps/icons/crewBooks/books/small/universalBook.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_CHINA:String = "../maps/icons/crewBooks/flags/china.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_CZECH:String = "../maps/icons/crewBooks/flags/czech.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_FRANCE:String = "../maps/icons/crewBooks/flags/france.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_GERMANY:String = "../maps/icons/crewBooks/flags/germany.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_ITALY:String = "../maps/icons/crewBooks/flags/italy.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_JAPAN:String = "../maps/icons/crewBooks/flags/japan.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_POLAND:String = "../maps/icons/crewBooks/flags/poland.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_SWEDEN:String = "../maps/icons/crewBooks/flags/sweden.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_UK:String = "../maps/icons/crewBooks/flags/uk.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_USA:String = "../maps/icons/crewBooks/flags/usa.png";

        public static const MAPS_ICONS_CREWBOOKS_FLAGS_USSR:String = "../maps/icons/crewBooks/flags/ussr.png";

        public static const MAPS_ICONS_CREWBOOKS_NOBOOKS_ARROW_GLOW:String = "../maps/icons/crewBooks/noBooks/arrow_glow.png";

        public static const MAPS_ICONS_CREWBOOKS_NOBOOKS_INVALIDCREW_1:String = "../maps/icons/crewBooks/noBooks/invalidCrew/1.png";

        public static const MAPS_ICONS_CREWBOOKS_NOBOOKS_INVALIDCREW_2:String = "../maps/icons/crewBooks/noBooks/invalidCrew/2.png";

        public static const MAPS_ICONS_CREWBOOKS_NOBOOKS_INVALIDCREW_3:String = "../maps/icons/crewBooks/noBooks/invalidCrew/3.png";

        public static const MAPS_ICONS_CREWBOOKS_NOBOOKS_NOITEMS_1:String = "../maps/icons/crewBooks/noBooks/noItems/1.png";

        public static const MAPS_ICONS_CREWBOOKS_NOBOOKS_NOITEMS_2:String = "../maps/icons/crewBooks/noBooks/noItems/2.png";

        public static const MAPS_ICONS_CREWBOOKS_NOBOOKS_NOITEMS_3:String = "../maps/icons/crewBooks/noBooks/noItems/3.png";

        public static const MAPS_ICONS_CREWBOOKS_RANDOMREWARD_CONGRATSICON:String = "../maps/icons/crewBooks/randomReward/congratsIcon.png";

        public static const MAPS_ICONS_CREWBOOKS_TANKMANCOMPONENTS_CREW_EMPTY:String = "../maps/icons/crewBooks/tankmanComponents/crew_empty.png";

        public static const MAPS_ICONS_CREWBOOKS_TANKMANCOMPONENTS_DISABLED:String = "../maps/icons/crewBooks/tankmanComponents/disabled.png";

        public static const MAPS_ICONS_CREWBOOKS_TANKMANCOMPONENTS_HOVER:String = "../maps/icons/crewBooks/tankmanComponents/hover.png";

        public static const MAPS_ICONS_CREWBOOKS_TANKMANCOMPONENTS_RED_LIGHT:String = "../maps/icons/crewBooks/tankmanComponents/red_light.png";

        public static const MAPS_ICONS_CREWBOOKS_TANKMANCOMPONENTS_RED_LIGHT_NATIVE_VEHICLE:String = "../maps/icons/crewBooks/tankmanComponents/red_light_native_vehicle.png";

        public static const MAPS_ICONS_CREWBOOKS_TANKMANCOMPONENTS_XP_ARROW:String = "../maps/icons/crewBooks/tankmanComponents/xp_arrow.png";

        public static const MAPS_ICONS_CREWBOOKS_TANKMANCOMPONENTS_XP_ARROW_SMALL:String = "../maps/icons/crewBooks/tankmanComponents/xp_arrow_small.png";

        public static const MAPS_ICONS_CREWBUNDLES_OFFSPRING:String = "../maps/icons/crewBundles/offspring.png";

        public static const MAPS_ICONS_CREWBUNDLES_BONUSES_BASICROLEBOOST_100:String = "../maps/icons/crewBundles/bonuses/basicRoleBoost_100.png";

        public static const MAPS_ICONS_CUSTOMIZATION_BRUSH_RARE:String = "../maps/icons/customization/brush_rare.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CLOCKICON_1:String = "../maps/icons/customization/ClockIcon-1.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CORNER_RARE:String = "../maps/icons/customization/corner_rare.png";

        public static const MAPS_ICONS_CUSTOMIZATION_EQUIPPED_SLOT:String = "../maps/icons/customization/equipped_slot.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_1:String = "../maps/icons/customization/icon_form_1.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_1_C:String = "../maps/icons/customization/icon_form_1_c.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_2:String = "../maps/icons/customization/icon_form_2.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_2_C:String = "../maps/icons/customization/icon_form_2_c.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_3:String = "../maps/icons/customization/icon_form_3.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_3_C:String = "../maps/icons/customization/icon_form_3_c.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_4:String = "../maps/icons/customization/icon_form_4.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_4_C:String = "../maps/icons/customization/icon_form_4_c.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_FORM_6:String = "../maps/icons/customization/icon_form_6.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_RENT:String = "../maps/icons/customization/icon_rent.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ICON_RENT_DEL:String = "../maps/icons/customization/icon_rent_del.png";

        public static const MAPS_ICONS_CUSTOMIZATION_INSTALLED_ON_TANK_ICON:String = "../maps/icons/customization/installed_on_tank_icon.png";

        public static const MAPS_ICONS_CUSTOMIZATION_NON_HISTORICAL:String = "../maps/icons/customization/non_historical.png";

        public static const MAPS_ICONS_CUSTOMIZATION_NON_HISTORICAL_MINI:String = "../maps/icons/customization/non_historical_mini.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STAR:String = "../maps/icons/customization/star.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STORAGE_ICON:String = "../maps/icons/customization/storage_icon.png";

        public static const MAPS_ICONS_CUSTOMIZATION_TANK:String = "../maps/icons/customization/tank.png";

        public static const MAPS_ICONS_CUSTOMIZATION_TOOLTIP_SEPARATOR:String = "../maps/icons/customization/tooltip_separator.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_CAMOUFLAGE_ACTIVE:String = "../maps/icons/customization/customization_icon/camouflage_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_CAMOUFLAGE_HOVER:String = "../maps/icons/customization/customization_icon/camouflage_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_EMBLEM_ACTIVE:String = "../maps/icons/customization/customization_icon/emblem_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_EMBLEM_HOVER:String = "../maps/icons/customization/customization_icon/emblem_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_INSCRIPTION_ACTIVE:String = "../maps/icons/customization/customization_icon/inscription_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_INSCRIPTION_HOVER:String = "../maps/icons/customization/customization_icon/inscription_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_MODIFICATION_ACTIVE:String = "../maps/icons/customization/customization_icon/modification_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_MODIFICATION_HOVER:String = "../maps/icons/customization/customization_icon/modification_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_NUMBER_ACTIVE:String = "../maps/icons/customization/customization_icon/number_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_NUMBER_HOVER:String = "../maps/icons/customization/customization_icon/number_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_PAINT_ACTIVE:String = "../maps/icons/customization/customization_icon/paint_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_PAINT_HOVER:String = "../maps/icons/customization/customization_icon/paint_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_PROJECTIONDECAL_ACTIVE:String = "../maps/icons/customization/customization_icon/projectionDecal_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ICON_PROJECTIONDECAL_HOVER:String = "../maps/icons/customization/customization_icon/projectionDecal_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_CAMOUFLAGE:String = "../maps/icons/customization/customization_items/180x135/icon_camouflage.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_EFFECT:String = "../maps/icons/customization/customization_items/180x135/icon_effect.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_EMBLEM:String = "../maps/icons/customization/customization_items/180x135/icon_emblem.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_INSCRIPTION:String = "../maps/icons/customization/customization_items/180x135/icon_inscription.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_PAINT:String = "../maps/icons/customization/customization_items/180x135/icon_paint.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_PERSONAL_NUMBERS:String = "../maps/icons/customization/customization_items/180x135/icon_personal_numbers.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_PROJECTION_DECALS:String = "../maps/icons/customization/customization_items/180x135/icon_projection_decals.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_180X135_ICON_STYLE:String = "../maps/icons/customization/customization_items/180x135/icon_style.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_CAMOUFLAGE:String = "../maps/icons/customization/customization_items/360x270/icon_camouflage.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_EFFECT:String = "../maps/icons/customization/customization_items/360x270/icon_effect.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_EMBLEM:String = "../maps/icons/customization/customization_items/360x270/icon_emblem.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_INSCRIPTION:String = "../maps/icons/customization/customization_items/360x270/icon_inscription.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_PAINT:String = "../maps/icons/customization/customization_items/360x270/icon_paint.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_PERSONAL_NUMBERS:String = "../maps/icons/customization/customization_items/360x270/icon_personal_numbers.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_PROJECTION_DECALS:String = "../maps/icons/customization/customization_items/360x270/icon_projection_decals.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_360X270_ICON_STYLE:String = "../maps/icons/customization/customization_items/360x270/icon_style.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_CAMOUFLAGE:String = "../maps/icons/customization/customization_items/480x280/icon_camouflage.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_EFFECT:String = "../maps/icons/customization/customization_items/480x280/icon_effect.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_EMBLEM:String = "../maps/icons/customization/customization_items/480x280/icon_emblem.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_INSCRIPTION:String = "../maps/icons/customization/customization_items/480x280/icon_inscription.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_PAINT:String = "../maps/icons/customization/customization_items/480x280/icon_paint.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_PERSONAL_NUMBERS:String = "../maps/icons/customization/customization_items/480x280/icon_personal_numbers.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_PROJECTION_DECALS:String = "../maps/icons/customization/customization_items/480x280/icon_projection_decals.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_480X280_ICON_STYLE:String = "../maps/icons/customization/customization_items/480x280/icon_style.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_CAMOUFLAGE:String = "../maps/icons/customization/customization_items/48x48/icon_camouflage.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_EFFECT:String = "../maps/icons/customization/customization_items/48x48/icon_effect.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_EMBLEM:String = "../maps/icons/customization/customization_items/48x48/icon_emblem.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_INSCRIPTION:String = "../maps/icons/customization/customization_items/48x48/icon_inscription.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_PAINT:String = "../maps/icons/customization/customization_items/48x48/icon_paint.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_PERSONAL_NUMBERS:String = "../maps/icons/customization/customization_items/48x48/icon_personal_numbers.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_PROJECTION_DECALS:String = "../maps/icons/customization/customization_items/48x48/icon_projection_decals.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_48X48_ICON_STYLE:String = "../maps/icons/customization/customization_items/48x48/icon_style.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_CAMOUFLAGE:String = "../maps/icons/customization/customization_items/600x450/icon_camouflage.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_EFFECT:String = "../maps/icons/customization/customization_items/600x450/icon_effect.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_EMBLEM:String = "../maps/icons/customization/customization_items/600x450/icon_emblem.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_INSCRIPTION:String = "../maps/icons/customization/customization_items/600x450/icon_inscription.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_PAINT:String = "../maps/icons/customization/customization_items/600x450/icon_paint.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_PERSONAL_NUMBERS:String = "../maps/icons/customization/customization_items/600x450/icon_personal_numbers.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_PROJECTION_DECALS:String = "../maps/icons/customization/customization_items/600x450/icon_projection_decals.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_600X450_ICON_STYLE:String = "../maps/icons/customization/customization_items/600x450/icon_style.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_CAMOUFLAGE:String = "../maps/icons/customization/customization_items/80x80/icon_camouflage.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_EFFECT:String = "../maps/icons/customization/customization_items/80x80/icon_effect.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_EMBLEM:String = "../maps/icons/customization/customization_items/80x80/icon_emblem.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_INSCRIPTION:String = "../maps/icons/customization/customization_items/80x80/icon_inscription.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_PAINT:String = "../maps/icons/customization/customization_items/80x80/icon_paint.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_PERSONAL_NUMBERS:String = "../maps/icons/customization/customization_items/80x80/icon_personal_numbers.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_PROJECTION_DECALS:String = "../maps/icons/customization/customization_items/80x80/icon_projection_decals.png";

        public static const MAPS_ICONS_CUSTOMIZATION_CUSTOMIZATION_ITEMS_80X80_ICON_STYLE:String = "../maps/icons/customization/customization_items/80x80/icon_style.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_LIP:String = "../maps/icons/customization/gf_customization_cart/lip.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOTS:String = "../maps/icons/customization/gf_customization_cart/slots.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_STYLE:String = "../maps/icons/customization/gf_customization_cart/style.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_BORDER:String = "../maps/icons/customization/gf_customization_cart/slot/border.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_CHECKED_ACTIVE:String = "../maps/icons/customization/gf_customization_cart/slot/checked_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_CHECKED_HOVER:String = "../maps/icons/customization/gf_customization_cart/slot/checked_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_HOVER:String = "../maps/icons/customization/gf_customization_cart/slot/hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_SHADOW:String = "../maps/icons/customization/gf_customization_cart/slot/shadow.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_SHINE:String = "../maps/icons/customization/gf_customization_cart/slot/shine.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_UNCHECKED_ACTIVE:String = "../maps/icons/customization/gf_customization_cart/slot/unchecked_active.png";

        public static const MAPS_ICONS_CUSTOMIZATION_GF_CUSTOMIZATION_CART_SLOT_UNCHECKED_HOVER:String = "../maps/icons/customization/gf_customization_cart/slot/unchecked_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_INSCRIPTION_CONTROLLER_BSPACE_BTN:String = "../maps/icons/customization/inscription_controller/bspace_btn.png";

        public static const MAPS_ICONS_CUSTOMIZATION_INSCRIPTION_CONTROLLER_ENTER_BTN:String = "../maps/icons/customization/inscription_controller/enter_btn.png";

        public static const MAPS_ICONS_CUSTOMIZATION_INSCRIPTION_CONTROLLER_ICON_ERROR:String = "../maps/icons/customization/inscription_controller/icon_error.png";

        public static const MAPS_ICONS_CUSTOMIZATION_INSCRIPTION_CONTROLLER_TEXT_INPUT_BG:String = "../maps/icons/customization/inscription_controller/text_input_bg.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_DEFAULT_LIST30X16:String = "../maps/icons/customization/items_popover/default_list30x16.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_DESERT16X16:String = "../maps/icons/customization/items_popover/desert16x16.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_DESERT_BACK_LIST:String = "../maps/icons/customization/items_popover/desert_back_list.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_DESERT_LIST30X16:String = "../maps/icons/customization/items_popover/desert_list30x16.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_SUMMER16X16:String = "../maps/icons/customization/items_popover/summer16x16.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_SUMMER_BACK_LIST:String = "../maps/icons/customization/items_popover/summer_back_list.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_SUMMER_LIST30X16:String = "../maps/icons/customization/items_popover/summer_list30x16.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_WINTER16X16:String = "../maps/icons/customization/items_popover/winter16x16.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_WINTER_BACK_LIST:String = "../maps/icons/customization/items_popover/winter_back_list.png";

        public static const MAPS_ICONS_CUSTOMIZATION_ITEMS_POPOVER_WINTER_LIST30X16:String = "../maps/icons/customization/items_popover/winter_list30x16.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_EDIT_DISABLE:String = "../maps/icons/customization/property_sheet/disable/icon_edit_disable.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_FULL_TANK_DISABLE:String = "../maps/icons/customization/property_sheet/disable/icon_full_tank_disable.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_INFO_DISABLE:String = "../maps/icons/customization/property_sheet/disable/icon_info_disable.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_MIRROR_01_DISABLED:String = "../maps/icons/customization/property_sheet/disable/icon_mirror_01_disabled.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_OFFSET_02_DISABLED:String = "../maps/icons/customization/property_sheet/disable/icon_offset_02_disabled.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_PALETTE_DISABLE:String = "../maps/icons/customization/property_sheet/disable/icon_palette_disable.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_RENTAL_DISABLE:String = "../maps/icons/customization/property_sheet/disable/icon_rental_disable.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_SCALE_DISABLE:String = "../maps/icons/customization/property_sheet/disable/icon_scale_disable.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_DISABLE_ICON_SEASON_DISABLE:String = "../maps/icons/customization/property_sheet/disable/icon_season_disable.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_CLOSE:String = "../maps/icons/customization/property_sheet/idle/icon_close.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_CLOSE_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_close_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_DEL:String = "../maps/icons/customization/property_sheet/idle/icon_del.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_DEL_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_del_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_EDIT:String = "../maps/icons/customization/property_sheet/idle/icon_edit.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_EDIT_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_edit_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_FULL_TANK:String = "../maps/icons/customization/property_sheet/idle/icon_full_tank.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_FULL_TANK_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_full_tank_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_INFO:String = "../maps/icons/customization/property_sheet/idle/icon_info.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_INFO_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_info_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_MIRROR_01_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_mirror_01_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_MIRROR_01_NORMAL:String = "../maps/icons/customization/property_sheet/idle/icon_mirror_01_normal.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_MIRROR_02_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_mirror_02_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_MIRROR_02_NORMAL:String = "../maps/icons/customization/property_sheet/idle/icon_mirror_02_normal.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_OFFSET_01_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_offset_01_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_OFFSET_01_NORMAL:String = "../maps/icons/customization/property_sheet/idle/icon_offset_01_normal.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_OFFSET_02_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_offset_02_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_OFFSET_02_NORMAL:String = "../maps/icons/customization/property_sheet/idle/icon_offset_02_normal.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_OFFSET_03_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_offset_03_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_OFFSET_3_NORMAL:String = "../maps/icons/customization/property_sheet/idle/icon_offset_3_normal.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_PALETTE:String = "../maps/icons/customization/property_sheet/idle/icon_palette.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_PALETTE_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_palette_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_RENTAL:String = "../maps/icons/customization/property_sheet/idle/icon_rental.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_RENTAL_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_rental_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_SCALE:String = "../maps/icons/customization/property_sheet/idle/icon_scale.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_SCALE_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_scale_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_SEASON:String = "../maps/icons/customization/property_sheet/idle/icon_season.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_ICON_SEASON_HOVER:String = "../maps/icons/customization/property_sheet/idle/icon_season_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_MOVE_HOVER:String = "../maps/icons/customization/property_sheet/idle/move_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_IDLE_MOVE_NORMAL:String = "../maps/icons/customization/property_sheet/idle/move_normal.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_CAMO_X:String = "../maps/icons/customization/property_sheet/remove/camo_x.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_CAMO_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/camo_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_COLORS_X:String = "../maps/icons/customization/property_sheet/remove/colors_x.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_COLORS_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/colors_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_DEL:String = "../maps/icons/customization/property_sheet/remove/del.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_DEL_HOVER:String = "../maps/icons/customization/property_sheet/remove/del_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_EFFECTS_X:String = "../maps/icons/customization/property_sheet/remove/effects_x.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_EFFECTS_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/effects_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_EMBLEM_X:String = "../maps/icons/customization/property_sheet/remove/emblem_x.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_EMBLEM_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/emblem_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_DECAL_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/icon_decal_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_DECAL_X_NORMAL:String = "../maps/icons/customization/property_sheet/remove/icon_decal_x_normal.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_DEL_ALL_SEASON:String = "../maps/icons/customization/property_sheet/remove/icon_del_all_season.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_DEL_RENT:String = "../maps/icons/customization/property_sheet/remove/icon_del_rent.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_DEL_RENT_HOVER:String = "../maps/icons/customization/property_sheet/remove/icon_del_rent_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_DEL_TANK:String = "../maps/icons/customization/property_sheet/remove/icon_del_tank.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_DEL_TANK_HOVER:String = "../maps/icons/customization/property_sheet/remove/icon_del_tank_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_ICON_SEASON_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/icon_season_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_STYLE_X:String = "../maps/icons/customization/property_sheet/remove/style_x.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_STYLE_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/style_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_TYPE_X:String = "../maps/icons/customization/property_sheet/remove/type_x.png";

        public static const MAPS_ICONS_CUSTOMIZATION_PROPERTY_SHEET_REMOVE_TYPE_X_HOVER:String = "../maps/icons/customization/property_sheet/remove/type_x_hover.png";

        public static const MAPS_ICONS_CUSTOMIZATION_SEASON_SELECTOR_DESERT_BG:String = "../maps/icons/customization/season_selector/desert_bg.png";

        public static const MAPS_ICONS_CUSTOMIZATION_SEASON_SELECTOR_DESERT_SHINE:String = "../maps/icons/customization/season_selector/desert_shine.png";

        public static const MAPS_ICONS_CUSTOMIZATION_SEASON_SELECTOR_SUMMER_BG:String = "../maps/icons/customization/season_selector/summer_bg.png";

        public static const MAPS_ICONS_CUSTOMIZATION_SEASON_SELECTOR_SUMMER_SHINE:String = "../maps/icons/customization/season_selector/summer_shine.png";

        public static const MAPS_ICONS_CUSTOMIZATION_SEASON_SELECTOR_WINTER_BG:String = "../maps/icons/customization/season_selector/winter_bg.png";

        public static const MAPS_ICONS_CUSTOMIZATION_SEASON_SELECTOR_WINTER_SHINE:String = "../maps/icons/customization/season_selector/winter_shine.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_BONUS:String = "../maps/icons/customization/style_info/bonus.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_FOOTBALL:String = "../maps/icons/customization/style_info/football.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_HALLOWEEN:String = "../maps/icons/customization/style_info/halloween.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_HISTORICAL:String = "../maps/icons/customization/style_info/historical.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_MARATHON:String = "../maps/icons/customization/style_info/marathon.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_NEWYEAR:String = "../maps/icons/customization/style_info/newYear.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_NONHISTORICAL:String = "../maps/icons/customization/style_info/nonhistorical.png";

        public static const MAPS_ICONS_CUSTOMIZATION_STYLE_INFO_RENTABLE:String = "../maps/icons/customization/style_info/rentable.png";

        public static const MAPS_ICONS_DEMOUNTKIT_ALERTICON:String = "../maps/icons/demountKit/alertIcon.png";

        public static const MAPS_ICONS_DEMOUNTKIT_BG:String = "../maps/icons/demountKit/bg.png";

        public static const MAPS_ICONS_DEMOUNTKIT_BGDISABLED:String = "../maps/icons/demountKit/bgDisabled.png";

        public static const MAPS_ICONS_DEMOUNTKIT_BGLIGHT:String = "../maps/icons/demountKit/bgLight.png";

        public static const MAPS_ICONS_DEMOUNTKIT_BGSELECT:String = "../maps/icons/demountKit/bgSelect.png";

        public static const MAPS_ICONS_DEMOUNTKIT_COMMON_150X150:String = "../maps/icons/demountKit/common_150x150.png";

        public static const MAPS_ICONS_DEMOUNTKIT_COMMON_16X16:String = "../maps/icons/demountKit/common_16x16.png";

        public static const MAPS_ICONS_DEMOUNTKIT_COMMON_180X135:String = "../maps/icons/demountKit/common_180x135.png";

        public static const MAPS_ICONS_DEMOUNTKIT_COMMON_48X48:String = "../maps/icons/demountKit/common_48x48.png";

        public static const MAPS_ICONS_DEMOUNTKIT_COMMON_80X80:String = "../maps/icons/demountKit/common_80x80.png";

        public static const MAPS_ICONS_DEMOUNTKIT_HIGHLIGHTALERT:String = "../maps/icons/demountKit/highlightAlert.png";

        public static const MAPS_ICONS_DEMOUNTKIT_HIGHLIGHTDELUXE:String = "../maps/icons/demountKit/highlightDeluxe.png";

        public static const MAPS_ICONS_DEMOUNTKIT_HIGHLIGHTREGULAR:String = "../maps/icons/demountKit/highlightRegular.png";

        public static const MAPS_ICONS_DEMOUNTKIT_HIGHLIGHTTROPHYEQUIPMENT:String = "../maps/icons/demountKit/highlightTrophyEquipment.png";

        public static const MAPS_ICONS_DEMOUNTKIT_ICONGOLD:String = "../maps/icons/demountKit/iconGold.png";

        public static const MAPS_ICONS_DEMOUNTKIT_OVERLAYDELUXE:String = "../maps/icons/demountKit/overlayDeluxe.png";

        public static const MAPS_ICONS_DEMOUNTKIT_STOREGE:String = "../maps/icons/demountKit/storege.png";

        public static const MAPS_ICONS_EPICBATTLES_FAME_POINT_TINY:String = "../maps/icons/epicBattles/fame_point_tiny.png";

        public static const MAPS_ICONS_EPICBATTLES_ITALY_IT14_P44_PANTERA:String = "../maps/icons/epicBattles/italy_It14_P44_Pantera.png";

        public static const MAPS_ICONS_EPICBATTLES_MODEICON:String = "../maps/icons/epicBattles/modeIcon.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_48X48_EPICBATTLE1:String = "../maps/icons/epicBattles/achievements/48x48/epicBattle1.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_48X48_EPICBATTLE2:String = "../maps/icons/epicBattles/achievements/48x48/epicBattle2.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_48X48_EPICBATTLE3:String = "../maps/icons/epicBattles/achievements/48x48/epicBattle3.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_48X48_EPICBATTLE4:String = "../maps/icons/epicBattles/achievements/48x48/epicBattle4.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_80X80_EPICBATTLE1:String = "../maps/icons/epicBattles/achievements/80x80/epicBattle1.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_80X80_EPICBATTLE2:String = "../maps/icons/epicBattles/achievements/80x80/epicBattle2.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_80X80_EPICBATTLE3:String = "../maps/icons/epicBattles/achievements/80x80/epicBattle3.png";

        public static const MAPS_ICONS_EPICBATTLES_ACHIEVEMENTS_80X80_EPICBATTLE4:String = "../maps/icons/epicBattles/achievements/80x80/epicBattle4.png";

        public static const MAPS_ICONS_EPICBATTLES_PRESTIGEPOINTS_16X16:String = "../maps/icons/epicBattles/prestigePoints/16x16.png";

        public static const MAPS_ICONS_EPICBATTLES_PRESTIGEPOINTS_24X24:String = "../maps/icons/epicBattles/prestigePoints/24x24.png";

        public static const MAPS_ICONS_EPICBATTLES_PRESTIGEPOINTS_32X32:String = "../maps/icons/epicBattles/prestigePoints/32x32.png";

        public static const MAPS_ICONS_EPICBATTLES_PRESTIGEPOINTS_48X48:String = "../maps/icons/epicBattles/prestigePoints/48x48.png";

        public static const MAPS_ICONS_EPICBATTLES_PRESTIGEPOINTS_80X80:String = "../maps/icons/epicBattles/prestigePoints/80x80.png";

        public static const MAPS_ICONS_EPICBATTLES_PRIMETIME_PRIME_TIME_BACK_DEFAULT:String = "../maps/icons/epicBattles/primeTime/prime_time_back_default.png";

        public static const MAPS_ICONS_FILTERS_DOUBLE_CAROUSEL:String = "../maps/icons/filters/double_carousel.png";

        public static const MAPS_ICONS_FILTERS_EMPTY:String = "../maps/icons/filters/empty.png";

        public static const MAPS_ICONS_FILTERS_PRESENCE:String = "../maps/icons/filters/presence.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_1:String = "../maps/icons/filters/levels/level_1.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_10:String = "../maps/icons/filters/levels/level_10.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_2:String = "../maps/icons/filters/levels/level_2.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_3:String = "../maps/icons/filters/levels/level_3.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_4:String = "../maps/icons/filters/levels/level_4.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_5:String = "../maps/icons/filters/levels/level_5.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_6:String = "../maps/icons/filters/levels/level_6.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_7:String = "../maps/icons/filters/levels/level_7.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_8:String = "../maps/icons/filters/levels/level_8.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_9:String = "../maps/icons/filters/levels/level_9.png";

        public static const MAPS_ICONS_FILTERS_LEVELS_LEVEL_ALL:String = "../maps/icons/filters/levels/level_all.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_ALL:String = "../maps/icons/filters/nations/all.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_CHINA:String = "../maps/icons/filters/nations/china.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_CZECH:String = "../maps/icons/filters/nations/czech.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_FRANCE:String = "../maps/icons/filters/nations/france.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_GERMANY:String = "../maps/icons/filters/nations/germany.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_IGR:String = "../maps/icons/filters/nations/igr.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_ITALY:String = "../maps/icons/filters/nations/italy.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_JAPAN:String = "../maps/icons/filters/nations/japan.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_POLAND:String = "../maps/icons/filters/nations/poland.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_SWEDEN:String = "../maps/icons/filters/nations/sweden.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_UK:String = "../maps/icons/filters/nations/uk.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_USA:String = "../maps/icons/filters/nations/usa.png";

        public static const MAPS_ICONS_FILTERS_NATIONS_USSR:String = "../maps/icons/filters/nations/ussr.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_CHINA:String = "../maps/icons/filters/nationsMedium/china.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_CZECH:String = "../maps/icons/filters/nationsMedium/czech.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_FRANCE:String = "../maps/icons/filters/nationsMedium/france.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_GERMANY:String = "../maps/icons/filters/nationsMedium/germany.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_ITALY:String = "../maps/icons/filters/nationsMedium/italy.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_JAPAN:String = "../maps/icons/filters/nationsMedium/japan.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_POLAND:String = "../maps/icons/filters/nationsMedium/poland.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_SWEDEN:String = "../maps/icons/filters/nationsMedium/sweden.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_UK:String = "../maps/icons/filters/nationsMedium/uk.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_USA:String = "../maps/icons/filters/nationsMedium/usa.png";

        public static const MAPS_ICONS_FILTERS_NATIONSMEDIUM_USSR:String = "../maps/icons/filters/nationsMedium/ussr.png";

        public static const MAPS_ICONS_FILTERS_TANKS_ALL:String = "../maps/icons/filters/tanks/all.png";

        public static const MAPS_ICONS_FILTERS_TANKS_AT_SPG:String = "../maps/icons/filters/tanks/AT-SPG.png";

        public static const MAPS_ICONS_FILTERS_TANKS_HEAVYTANK:String = "../maps/icons/filters/tanks/heavyTank.png";

        public static const MAPS_ICONS_FILTERS_TANKS_LIGHTTANK:String = "../maps/icons/filters/tanks/lightTank.png";

        public static const MAPS_ICONS_FILTERS_TANKS_MEDIUMTANK:String = "../maps/icons/filters/tanks/mediumTank.png";

        public static const MAPS_ICONS_FILTERS_TANKS_NONE:String = "../maps/icons/filters/tanks/none.png";

        public static const MAPS_ICONS_FILTERS_TANKS_SPG:String = "../maps/icons/filters/tanks/SPG.png";

        public static const MAPS_ICONS_FLAGS_362X362_CHINA:String = "../maps/icons/flags/362x362/china.png";

        public static const MAPS_ICONS_FLAGS_362X362_CZECH:String = "../maps/icons/flags/362x362/czech.png";

        public static const MAPS_ICONS_FLAGS_362X362_FRANCE:String = "../maps/icons/flags/362x362/france.png";

        public static const MAPS_ICONS_FLAGS_362X362_GERMANY:String = "../maps/icons/flags/362x362/germany.png";

        public static const MAPS_ICONS_FLAGS_362X362_ITALY:String = "../maps/icons/flags/362x362/italy.png";

        public static const MAPS_ICONS_FLAGS_362X362_JAPAN:String = "../maps/icons/flags/362x362/japan.png";

        public static const MAPS_ICONS_FLAGS_362X362_POLAND:String = "../maps/icons/flags/362x362/poland.png";

        public static const MAPS_ICONS_FLAGS_362X362_SWEDEN:String = "../maps/icons/flags/362x362/sweden.png";

        public static const MAPS_ICONS_FLAGS_362X362_UK:String = "../maps/icons/flags/362x362/uk.png";

        public static const MAPS_ICONS_FLAGS_362X362_USA:String = "../maps/icons/flags/362x362/usa.png";

        public static const MAPS_ICONS_FLAGS_362X362_USSR:String = "../maps/icons/flags/362x362/ussr.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_AMMOSLIDER:String = "../maps/icons/hangarTutorial/ammoSlider.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_BUYBUTTON:String = "../maps/icons/hangarTutorial/buyButton.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_COMMONTANK:String = "../maps/icons/hangarTutorial/commonTank.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_DESERT:String = "../maps/icons/hangarTutorial/desert.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_GOALSQUEST:String = "../maps/icons/hangarTutorial/goalsQuest.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_PREMTANK:String = "../maps/icons/hangarTutorial/premTank.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q1:String = "../maps/icons/hangarTutorial/q1.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q11:String = "../maps/icons/hangarTutorial/q11.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q12:String = "../maps/icons/hangarTutorial/q12.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q13:String = "../maps/icons/hangarTutorial/q13.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q2:String = "../maps/icons/hangarTutorial/q2.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q3:String = "../maps/icons/hangarTutorial/q3.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q4:String = "../maps/icons/hangarTutorial/q4.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q5:String = "../maps/icons/hangarTutorial/q5.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q6:String = "../maps/icons/hangarTutorial/q6.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q7:String = "../maps/icons/hangarTutorial/q7.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_Q9:String = "../maps/icons/hangarTutorial/Q9.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_RESEARCHBUTTON:String = "../maps/icons/hangarTutorial/researchButton.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_SUMMER:String = "../maps/icons/hangarTutorial/summer.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_WINTER:String = "../maps/icons/hangarTutorial/winter.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_MODULES_CHASSIS:String = "../maps/icons/hangarTutorial/modules/chassis.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_MODULES_ENGINE:String = "../maps/icons/hangarTutorial/modules/engine.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_MODULES_GUN:String = "../maps/icons/hangarTutorial/modules/gun.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_MODULES_RADIO:String = "../maps/icons/hangarTutorial/modules/radio.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_MODULES_TOWER:String = "../maps/icons/hangarTutorial/modules/tower.png";

        public static const MAPS_ICONS_HANGARTUTORIAL_MODULES_WHEEL:String = "../maps/icons/hangarTutorial/modules/wheel.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_1:String = "../maps/icons/levels/tank_level_1.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_10:String = "../maps/icons/levels/tank_level_10.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_2:String = "../maps/icons/levels/tank_level_2.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_3:String = "../maps/icons/levels/tank_level_3.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_4:String = "../maps/icons/levels/tank_level_4.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_5:String = "../maps/icons/levels/tank_level_5.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_6:String = "../maps/icons/levels/tank_level_6.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_7:String = "../maps/icons/levels/tank_level_7.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_8:String = "../maps/icons/levels/tank_level_8.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_9:String = "../maps/icons/levels/tank_level_9.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_1:String = "../maps/icons/levels/tank_level_big_1.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_10:String = "../maps/icons/levels/tank_level_big_10.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_2:String = "../maps/icons/levels/tank_level_big_2.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_3:String = "../maps/icons/levels/tank_level_big_3.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_4:String = "../maps/icons/levels/tank_level_big_4.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_5:String = "../maps/icons/levels/tank_level_big_5.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_6:String = "../maps/icons/levels/tank_level_big_6.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_7:String = "../maps/icons/levels/tank_level_big_7.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_8:String = "../maps/icons/levels/tank_level_big_8.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_BIG_9:String = "../maps/icons/levels/tank_level_big_9.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_1:String = "../maps/icons/levels/tank_level_small_1.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_10:String = "../maps/icons/levels/tank_level_small_10.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_2:String = "../maps/icons/levels/tank_level_small_2.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_3:String = "../maps/icons/levels/tank_level_small_3.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_4:String = "../maps/icons/levels/tank_level_small_4.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_5:String = "../maps/icons/levels/tank_level_small_5.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_6:String = "../maps/icons/levels/tank_level_small_6.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_7:String = "../maps/icons/levels/tank_level_small_7.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_8:String = "../maps/icons/levels/tank_level_small_8.png";

        public static const MAPS_ICONS_LEVELS_TANK_LEVEL_SMALL_9:String = "../maps/icons/levels/tank_level_small_9.png";

        public static const MAPS_ICONS_LIBRARY_ACCOUNTTYPECHANGEDICON_1:String = "../maps/icons/library/AccountTypeChangedIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_ADD_TO_COMPARE:String = "../maps/icons/library/add_to_compare.png";

        public static const MAPS_ICONS_LIBRARY_ADVANCEDSHINE:String = "../maps/icons/library/advancedShine.png";

        public static const MAPS_ICONS_LIBRARY_ADVANCEDSHINEBLUE:String = "../maps/icons/library/advancedShineBlue.png";

        public static const MAPS_ICONS_LIBRARY_ALERTBIGICON:String = "../maps/icons/library/alertBigIcon.png";

        public static const MAPS_ICONS_LIBRARY_ALERTICON:String = "../maps/icons/library/alertIcon.png";

        public static const MAPS_ICONS_LIBRARY_ALERTICON1:String = "../maps/icons/library/alertIcon1.png";

        public static const MAPS_ICONS_LIBRARY_ALERTICON2:String = "../maps/icons/library/alertIcon2.png";

        public static const MAPS_ICONS_LIBRARY_ARROWORANGERIGHTICON8X8:String = "../maps/icons/library/arrowOrangeRightIcon8x8.png";

        public static const MAPS_ICONS_LIBRARY_ARROW_DOWN:String = "../maps/icons/library/arrow_down.png";

        public static const MAPS_ICONS_LIBRARY_ARROW_NAV_LEFT:String = "../maps/icons/library/arrow_nav_left.png";

        public static const MAPS_ICONS_LIBRARY_ARROW_NAV_RIGHT:String = "../maps/icons/library/arrow_nav_right.png";

        public static const MAPS_ICONS_LIBRARY_ARROW_UP:String = "../maps/icons/library/arrow_up.png";

        public static const MAPS_ICONS_LIBRARY_ASSET_1:String = "../maps/icons/library/Asset_1.png";

        public static const MAPS_ICONS_LIBRARY_ATTENTIONICON:String = "../maps/icons/library/attentionIcon.png";

        public static const MAPS_ICONS_LIBRARY_ATTENTIONICON1:String = "../maps/icons/library/attentionIcon1.png";

        public static const MAPS_ICONS_LIBRARY_ATTENTIONICONFILLED:String = "../maps/icons/library/attentionIconFilled.png";

        public static const MAPS_ICONS_LIBRARY_ATTENTIONICONFILLEDBIG:String = "../maps/icons/library/attentionIconFilledBig.png";

        public static const MAPS_ICONS_LIBRARY_AWARDOBTAINED:String = "../maps/icons/library/awardObtained.png";

        public static const MAPS_ICONS_LIBRARY_BASIC_BIG:String = "../maps/icons/library/basic_big.png";

        public static const MAPS_ICONS_LIBRARY_BASIC_SMALL:String = "../maps/icons/library/basic_small.png";

        public static const MAPS_ICONS_LIBRARY_BATTLERESULTICON_1:String = "../maps/icons/library/BattleResultIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_BGBATTLERESULTICONDEFEAT_1:String = "../maps/icons/library/BgBattleResultIconDefeat-1.png";

        public static const MAPS_ICONS_LIBRARY_BGBATTLERESULTICONDRAW_1:String = "../maps/icons/library/BgBattleResultIconDraw-1.png";

        public static const MAPS_ICONS_LIBRARY_BGBATTLERESULTICONVICTORY_1:String = "../maps/icons/library/BgBattleResultIconVictory-1.png";

        public static const MAPS_ICONS_LIBRARY_BGPERSONALMISSIONFAILEDICON_1:String = "../maps/icons/library/BgPersonalMissionFailedIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_BGPOLL_1:String = "../maps/icons/library/BgPoll-1.png";

        public static const MAPS_ICONS_LIBRARY_BGRANKEDBATTLERESULT_1:String = "../maps/icons/library/BgRankedBattleResult-1.png";

        public static const MAPS_ICONS_LIBRARY_BGREFERRAL_1:String = "../maps/icons/library/BgReferral-1.png";

        public static const MAPS_ICONS_LIBRARY_BG_PROGRESSIVE_REWARD_1:String = "../maps/icons/library/bg_progressive_reward-1.png";

        public static const MAPS_ICONS_LIBRARY_BLUEACTIONBG:String = "../maps/icons/library/BlueActionBG.png";

        public static const MAPS_ICONS_LIBRARY_BONUS_X:String = "../maps/icons/library/bonus_x.png";

        public static const MAPS_ICONS_LIBRARY_BONUS_X2:String = "../maps/icons/library/bonus_x2.png";

        public static const MAPS_ICONS_LIBRARY_BONUS_X3:String = "../maps/icons/library/bonus_x3.png";

        public static const MAPS_ICONS_LIBRARY_BONUS_X4:String = "../maps/icons/library/bonus_x4.png";

        public static const MAPS_ICONS_LIBRARY_BONUS_X5:String = "../maps/icons/library/bonus_x5.png";

        public static const MAPS_ICONS_LIBRARY_BOOKMARKICON:String = "../maps/icons/library/bookmarkIcon.png";

        public static const MAPS_ICONS_LIBRARY_BUTTON_LINK:String = "../maps/icons/library/button_link.png";

        public static const MAPS_ICONS_LIBRARY_BUTTON_TRADEIN:String = "../maps/icons/library/button_tradein.png";

        public static const MAPS_ICONS_LIBRARY_BUYINWEB:String = "../maps/icons/library/buyInWeb.png";

        public static const MAPS_ICONS_LIBRARY_CANCELICON_1:String = "../maps/icons/library/CancelIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_CHANGEARROW:String = "../maps/icons/library/changeArrow.png";

        public static const MAPS_ICONS_LIBRARY_CHANGETANK:String = "../maps/icons/library/changeTank.png";

        public static const MAPS_ICONS_LIBRARY_CHANGETANK16:String = "../maps/icons/library/changeTank16.png";

        public static const MAPS_ICONS_LIBRARY_CLANBATTLERESULTICON_1:String = "../maps/icons/library/ClanBattleResultIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_CLANINVITEICON_1:String = "../maps/icons/library/clanInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_CLASS_ICON:String = "../maps/icons/library/class_icon.png";

        public static const MAPS_ICONS_LIBRARY_CLOCK:String = "../maps/icons/library/clock.png";

        public static const MAPS_ICONS_LIBRARY_CLOCKICON_1:String = "../maps/icons/library/ClockIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_COMMANDERICON:String = "../maps/icons/library/commanderIcon.png";

        public static const MAPS_ICONS_LIBRARY_COMPLETE:String = "../maps/icons/library/complete.png";

        public static const MAPS_ICONS_LIBRARY_COMPLETEDINDICATOR:String = "../maps/icons/library/completedIndicator.png";

        public static const MAPS_ICONS_LIBRARY_CONFIRMICON_1:String = "../maps/icons/library/ConfirmIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_COUNTER_1:String = "../maps/icons/library/counter_1.png";

        public static const MAPS_ICONS_LIBRARY_COUNTER_2:String = "../maps/icons/library/counter_2.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICON_1:String = "../maps/icons/library/CreditsIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICON_2:String = "../maps/icons/library/CreditsIcon-2.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICON_3:String = "../maps/icons/library/CreditsIcon-3.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICON:String = "../maps/icons/library/CreditsIcon.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICONBIG_1:String = "../maps/icons/library/CreditsIconBig-1.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICONBIG:String = "../maps/icons/library/CreditsIconBig.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICONINACTIVE_2:String = "../maps/icons/library/CreditsIconInactive-2.png";

        public static const MAPS_ICONS_LIBRARY_CREDITSICON_23X22:String = "../maps/icons/library/creditsIcon_23x22.png";

        public static const MAPS_ICONS_LIBRARY_CREW_ONLINE:String = "../maps/icons/library/crew_online.png";

        public static const MAPS_ICONS_LIBRARY_CROSS:String = "../maps/icons/library/cross.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTALICON_1:String = "../maps/icons/library/CrystalIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTALICON_2:String = "../maps/icons/library/CrystalIcon-2.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTALICONBIG_1:String = "../maps/icons/library/CrystalIconBig-1.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTALICONBIG:String = "../maps/icons/library/CrystalIconBig.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTALICONINACTIVE_2:String = "../maps/icons/library/CrystalIconInactive-2.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTAL_16X16:String = "../maps/icons/library/crystal_16x16.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTAL_23X22:String = "../maps/icons/library/crystal_23x22.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTAL_48X48:String = "../maps/icons/library/crystal_48x48.png";

        public static const MAPS_ICONS_LIBRARY_CRYSTAL_80X80:String = "../maps/icons/library/crystal_80x80.png";

        public static const MAPS_ICONS_LIBRARY_DISABLED_PATTERN:String = "../maps/icons/library/disabled_pattern.png";

        public static const MAPS_ICONS_LIBRARY_DISCOUNT_1:String = "../maps/icons/library/discount-1.png";

        public static const MAPS_ICONS_LIBRARY_DISCOUNT:String = "../maps/icons/library/discount.png";

        public static const MAPS_ICONS_LIBRARY_DIVIDER:String = "../maps/icons/library/divider.png";

        public static const MAPS_ICONS_LIBRARY_DONE:String = "../maps/icons/library/done.png";

        public static const MAPS_ICONS_LIBRARY_DOT:String = "../maps/icons/library/dot.png";

        public static const MAPS_ICONS_LIBRARY_DOUBLECHECKMARK:String = "../maps/icons/library/doubleCheckmark.png";

        public static const MAPS_ICONS_LIBRARY_ELITEXPICON_2:String = "../maps/icons/library/EliteXpIcon-2.png";

        public static const MAPS_ICONS_LIBRARY_ELITEXPICONBIG:String = "../maps/icons/library/EliteXpIconBig.png";

        public static const MAPS_ICONS_LIBRARY_ELITE_SMALL_ICON:String = "../maps/icons/library/elite_small_icon.png";

        public static const MAPS_ICONS_LIBRARY_EMPTY_SELECTION:String = "../maps/icons/library/empty_selection.png";

        public static const MAPS_ICONS_LIBRARY_EMPTY_VEH:String = "../maps/icons/library/empty_veh.png";

        public static const MAPS_ICONS_LIBRARY_EPICINVITEICON_1:String = "../maps/icons/library/epicInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_EPICVEHICLESALL:String = "../maps/icons/library/epicVehiclesAll.png";

        public static const MAPS_ICONS_LIBRARY_ERRORICON_1:String = "../maps/icons/library/ErrorIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_ERRORICON:String = "../maps/icons/library/errorIcon.png";

        public static const MAPS_ICONS_LIBRARY_EVENT_1:String = "../maps/icons/library/event-1.png";

        public static const MAPS_ICONS_LIBRARY_EVENTICON_1:String = "../maps/icons/library/EventIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_EVENTICONBG_1:String = "../maps/icons/library/EventIconBg-1.png";

        public static const MAPS_ICONS_LIBRARY_EVENTINVITEICON_1:String = "../maps/icons/library/eventInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_EXTERNALINVITEICON_1:String = "../maps/icons/library/externalInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_FALLOUTVEHICLESALL:String = "../maps/icons/library/falloutVehiclesAll.png";

        public static const MAPS_ICONS_LIBRARY_FAVORITE:String = "../maps/icons/library/favorite.png";

        public static const MAPS_ICONS_LIBRARY_FAVORITE_MEDIUM:String = "../maps/icons/library/favorite_medium.png";

        public static const MAPS_ICONS_LIBRARY_FAVORITE_SMALL:String = "../maps/icons/library/favorite_small.png";

        public static const MAPS_ICONS_LIBRARY_FLAKE:String = "../maps/icons/library/flake.png";

        public static const MAPS_ICONS_LIBRARY_FORTBATTLEDEFEATBG_1:String = "../maps/icons/library/FortBattleDefeatBg-1.png";

        public static const MAPS_ICONS_LIBRARY_FORTBATTLEDEFENCEINVITEICON_1:String = "../maps/icons/library/fortBattleDefenceInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_FORTBATTLEDRAWBG_1:String = "../maps/icons/library/FortBattleDrawBg-1.png";

        public static const MAPS_ICONS_LIBRARY_FORTBATTLEOFFENCEINVITEICON_1:String = "../maps/icons/library/fortBattleOffenceInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_FORTBATTLERESULT_1:String = "../maps/icons/library/FortBattleResult-1.png";

        public static const MAPS_ICONS_LIBRARY_FORTBATTLEVICTORYBG_1:String = "../maps/icons/library/FortBattleVictoryBg-1.png";

        public static const MAPS_ICONS_LIBRARY_FORTRESOURCE_1:String = "../maps/icons/library/FortResource-1.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICON_1:String = "../maps/icons/library/FreeXpIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICON_2:String = "../maps/icons/library/FreeXpIcon-2.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICON_3:String = "../maps/icons/library/FreeXPIcon-3.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICON:String = "../maps/icons/library/FreeXpIcon.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICONBIG:String = "../maps/icons/library/freeXPIconBig.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICONBIG24X24:String = "../maps/icons/library/freeXPIconBig24x24.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICONINACTIVE_1:String = "../maps/icons/library/FreeXpIconInactive-1.png";

        public static const MAPS_ICONS_LIBRARY_FREEXPICON_23X22:String = "../maps/icons/library/freeXpIcon_23x22.png";

        public static const MAPS_ICONS_LIBRARY_FRIENDSHIPICON_1:String = "../maps/icons/library/friendshipIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_FRTCLANBTLBGBATTLERESULT_UR_DEFEAT_1:String = "../maps/icons/library/frtClanBtlBgBattleResult_UR_Defeat-1.png";

        public static const MAPS_ICONS_LIBRARY_FRTCLANBTLBGBATTLERESULT_UR_DEFEAT:String = "../maps/icons/library/frtClanBtlBgBattleResult_UR_Defeat.png";

        public static const MAPS_ICONS_LIBRARY_FRTCLANBTLBGBATTLERESULT_UR_DRAW_1:String = "../maps/icons/library/frtClanBtlBgBattleResult_UR_Draw-1.png";

        public static const MAPS_ICONS_LIBRARY_FRTCLANBTLBGBATTLERESULT_UR_DRAW:String = "../maps/icons/library/frtClanBtlBgBattleResult_UR_Draw.png";

        public static const MAPS_ICONS_LIBRARY_FRTCLANBTLBGBATTLERESULT_UR_VICTORY_1:String = "../maps/icons/library/frtClanBtlBgBattleResult_UR_Victory-1.png";

        public static const MAPS_ICONS_LIBRARY_FRTCLANBTLBGBATTLERESULT_UR_VICTORY:String = "../maps/icons/library/frtClanBtlBgBattleResult_UR_Victory.png";

        public static const MAPS_ICONS_LIBRARY_GAMEMODE:String = "../maps/icons/library/gameMode.png";

        public static const MAPS_ICONS_LIBRARY_GEAR:String = "../maps/icons/library/gear.png";

        public static const MAPS_ICONS_LIBRARY_GERMANY_E_75:String = "../maps/icons/library/germany-E-75.png";

        public static const MAPS_ICONS_LIBRARY_GERMANY_PZVI:String = "../maps/icons/library/germany-PzVI.png";

        public static const MAPS_ICONS_LIBRARY_GIFTICON_1:String = "../maps/icons/library/GiftIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_GIFTICONDISABLED:String = "../maps/icons/library/giftIconDisabled.png";

        public static const MAPS_ICONS_LIBRARY_GIFTICONENABLED:String = "../maps/icons/library/giftIconEnabled.png";

        public static const MAPS_ICONS_LIBRARY_GOLDICON_1:String = "../maps/icons/library/GoldIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_GOLDICON_2:String = "../maps/icons/library/GoldIcon-2.png";

        public static const MAPS_ICONS_LIBRARY_GOLDICONBIG:String = "../maps/icons/library/goldIconBig.png";

        public static const MAPS_ICONS_LIBRARY_GOLDICONINACTIVE_2:String = "../maps/icons/library/GoldIconInactive-2.png";

        public static const MAPS_ICONS_LIBRARY_GOLDSTARICON_1:String = "../maps/icons/library/GoldStarIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_HANDS_1:String = "../maps/icons/library/hands-1.png";

        public static const MAPS_ICONS_LIBRARY_HANDSOFF_1:String = "../maps/icons/library/handsOff-1.png";

        public static const MAPS_ICONS_LIBRARY_HANDSPLUS_1:String = "../maps/icons/library/handsPlus-1.png";

        public static const MAPS_ICONS_LIBRARY_HINT_PLUS:String = "../maps/icons/library/hint_plus.png";

        public static const MAPS_ICONS_LIBRARY_ICON_COUPON_80:String = "../maps/icons/library/icon coupon_80.png";

        public static const MAPS_ICONS_LIBRARY_ICONVIDEOOFF:String = "../maps/icons/library/iconVideoOff.png";

        public static const MAPS_ICONS_LIBRARY_ICONVIDEOON:String = "../maps/icons/library/iconVideoOn.png";

        public static const MAPS_ICONS_LIBRARY_ICON_1:String = "../maps/icons/library/icon_1.png";

        public static const MAPS_ICONS_LIBRARY_ICON_2:String = "../maps/icons/library/icon_2.png";

        public static const MAPS_ICONS_LIBRARY_ICON_ALERT_32X32:String = "../maps/icons/library/icon_alert_32x32.png";

        public static const MAPS_ICONS_LIBRARY_ICON_ALERT_90X84:String = "../maps/icons/library/icon_alert_90x84.png";

        public static const MAPS_ICONS_LIBRARY_ICON_BUTTON_LIST:String = "../maps/icons/library/icon_button_list.png";

        public static const MAPS_ICONS_LIBRARY_ICON_CLOCK_100X100:String = "../maps/icons/library/icon_clock_100x100.png";

        public static const MAPS_ICONS_LIBRARY_ICON_EYE:String = "../maps/icons/library/icon_eye.png";

        public static const MAPS_ICONS_LIBRARY_ICON_GIFT:String = "../maps/icons/library/icon_gift.png";

        public static const MAPS_ICONS_LIBRARY_ICON_PROXY_16X16:String = "../maps/icons/library/icon_proxy_16x16.png";

        public static const MAPS_ICONS_LIBRARY_ICON_SAND_WATCH:String = "../maps/icons/library/icon_sand_watch.png";

        public static const MAPS_ICONS_LIBRARY_IGRLOGO:String = "../maps/icons/library/igrLogo.png";

        public static const MAPS_ICONS_LIBRARY_IGR_32X13:String = "../maps/icons/library/igr_32x13.png";

        public static const MAPS_ICONS_LIBRARY_INFO:String = "../maps/icons/library/info.png";

        public static const MAPS_ICONS_LIBRARY_INFORMATIONICON_1:String = "../maps/icons/library/InformationIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_INFORMATIONICON:String = "../maps/icons/library/InformationIcon.png";

        public static const MAPS_ICONS_LIBRARY_INFOTYPE_LOCKED:String = "../maps/icons/library/infotype_locked.png";

        public static const MAPS_ICONS_LIBRARY_INFO_YELLOW:String = "../maps/icons/library/info_yellow.png";

        public static const MAPS_ICONS_LIBRARY_INPROGRESSICON:String = "../maps/icons/library/inProgressIcon.png";

        public static const MAPS_ICONS_LIBRARY_INVENTORYICON:String = "../maps/icons/library/inventoryIcon.png";

        public static const MAPS_ICONS_LIBRARY_INVOICEICON_1:String = "../maps/icons/library/InvoiceIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_LOCKED:String = "../maps/icons/library/locked.png";

        public static const MAPS_ICONS_LIBRARY_LOCKINDICATOR:String = "../maps/icons/library/lockIndicator.png";

        public static const MAPS_ICONS_LIBRARY_MAPS:String = "../maps/icons/library/maps.png";

        public static const MAPS_ICONS_LIBRARY_MARKER_BLOCKED:String = "../maps/icons/library/marker_blocked.png";

        public static const MAPS_ICONS_LIBRARY_MARKER_CHECK:String = "../maps/icons/library/marker_check.png";

        public static const MAPS_ICONS_LIBRARY_MARKER_UNCHECK:String = "../maps/icons/library/marker_uncheck.png";

        public static const MAPS_ICONS_LIBRARY_MARK_ON_GUN_ICON:String = "../maps/icons/library/mark_on_gun_icon.png";

        public static const MAPS_ICONS_LIBRARY_MESSAGEICON_1:String = "../maps/icons/library/MessageIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_MISSION_ICON:String = "../maps/icons/library/mission_icon.png";

        public static const MAPS_ICONS_LIBRARY_MULTYXP:String = "../maps/icons/library/multyXp.png";

        public static const MAPS_ICONS_LIBRARY_NEW:String = "../maps/icons/library/new.png";

        public static const MAPS_ICONS_LIBRARY_NORMALXPICON:String = "../maps/icons/library/NormalXpIcon.png";

        public static const MAPS_ICONS_LIBRARY_NOTIFICATIONS_OFF:String = "../maps/icons/library/notifications_off.png";

        public static const MAPS_ICONS_LIBRARY_NOTIF_FILTERS_GIFT_16X16:String = "../maps/icons/library/notif_filters_gift_16x16.png";

        public static const MAPS_ICONS_LIBRARY_NOTIF_FILTERS_INFORMATION_16X16:String = "../maps/icons/library/notif_filters_information_16x16.png";

        public static const MAPS_ICONS_LIBRARY_NOTIF_FILTERS_INVITATIONS_24X16:String = "../maps/icons/library/notif_filters_invitations_24x16.png";

        public static const MAPS_ICONS_LIBRARY_NUT_1:String = "../maps/icons/library/nut-1.png";

        public static const MAPS_ICONS_LIBRARY_OFFERICON_1:String = "../maps/icons/library/OfferIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_OFFERICONBG_1:String = "../maps/icons/library/OfferIconBg-1.png";

        public static const MAPS_ICONS_LIBRARY_OKICON:String = "../maps/icons/library/okIcon.png";

        public static const MAPS_ICONS_LIBRARY_ONPAUSE:String = "../maps/icons/library/onPause.png";

        public static const MAPS_ICONS_LIBRARY_OWNERICON:String = "../maps/icons/library/ownerIcon.png";

        public static const MAPS_ICONS_LIBRARY_PARAMS:String = "../maps/icons/library/params.png";

        public static const MAPS_ICONS_LIBRARY_PEN:String = "../maps/icons/library/pen.png";

        public static const MAPS_ICONS_LIBRARY_PERSONALACHIEVEMENTSICON_1:String = "../maps/icons/library/PersonalAchievementsIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_PLUS:String = "../maps/icons/library/plus.png";

        public static const MAPS_ICONS_LIBRARY_POST_CLOSE:String = "../maps/icons/library/post_close.png";

        public static const MAPS_ICONS_LIBRARY_POST_OPEN:String = "../maps/icons/library/post_open.png";

        public static const MAPS_ICONS_LIBRARY_POWERLEVELICON_1:String = "../maps/icons/library/PowerlevelIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_PREBATTLEINVITEICON_1:String = "../maps/icons/library/prebattleInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_PREMACCICON_1:String = "../maps/icons/library/PremAccIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_PREMDAYICONBIG:String = "../maps/icons/library/premDayIconBig.png";

        public static const MAPS_ICONS_LIBRARY_PREMIUMACCOUNTEXPIRYICON_1:String = "../maps/icons/library/PremiumAccountExpiryIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_PREMIUM_BIG:String = "../maps/icons/library/premium_big.png";

        public static const MAPS_ICONS_LIBRARY_PREMIUM_IGR_BIG:String = "../maps/icons/library/premium_igr_big.png";

        public static const MAPS_ICONS_LIBRARY_PREMIUM_IGR_ICO:String = "../maps/icons/library/premium_igr_ico.png";

        public static const MAPS_ICONS_LIBRARY_PREMIUM_IGR_SMALL:String = "../maps/icons/library/premium_igr_small.png";

        public static const MAPS_ICONS_LIBRARY_PREMIUM_SMALL:String = "../maps/icons/library/premium_small.png";

        public static const MAPS_ICONS_LIBRARY_PREM_CHECKBOX:String = "../maps/icons/library/prem_checkbox.png";

        public static const MAPS_ICONS_LIBRARY_PREM_SMALL_ICON:String = "../maps/icons/library/prem_small_icon.png";

        public static const MAPS_ICONS_LIBRARY_PRESTIGEPOINTS_1:String = "../maps/icons/library/prestigePoints-1.png";

        public static const MAPS_ICONS_LIBRARY_PROXYCURRENCYICON_1:String = "../maps/icons/library/ProxyCurrencyIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_PROXYCURRENCYICON_2:String = "../maps/icons/library/ProxyCurrencyIcon-2.png";

        public static const MAPS_ICONS_LIBRARY_PROXYCURRENCYICON_3:String = "../maps/icons/library/ProxyCurrencyIcon-3.png";

        public static const MAPS_ICONS_LIBRARY_QUEST_ICON:String = "../maps/icons/library/quest_icon.png";

        public static const MAPS_ICONS_LIBRARY_REDACTIONBG:String = "../maps/icons/library/RedActionBG.png";

        public static const MAPS_ICONS_LIBRARY_REDCROSS:String = "../maps/icons/library/redCross.png";

        public static const MAPS_ICONS_LIBRARY_REDNOTAVAILABLE:String = "../maps/icons/library/redNotAvailable.png";

        public static const MAPS_ICONS_LIBRARY_REFERRALCOIN_1:String = "../maps/icons/library/referralCoin-1.png";

        public static const MAPS_ICONS_LIBRARY_REFERRALINVITEICON_1:String = "../maps/icons/library/ReferralInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_RENT_ICO_BIG:String = "../maps/icons/library/rent_ico_big.png";

        public static const MAPS_ICONS_LIBRARY_RESERVEACTIVATED_1:String = "../maps/icons/library/ReserveActivated-1.png";

        public static const MAPS_ICONS_LIBRARY_REWARDLIST:String = "../maps/icons/library/rewardlist.png";

        public static const MAPS_ICONS_LIBRARY_RIBBON:String = "../maps/icons/library/ribbon.png";

        public static const MAPS_ICONS_LIBRARY_SALLYRESULT_1:String = "../maps/icons/library/SallyResult-1.png";

        public static const MAPS_ICONS_LIBRARY_SALLYRESULTBG_1:String = "../maps/icons/library/SallyResultBg-1.png";

        public static const MAPS_ICONS_LIBRARY_SEND:String = "../maps/icons/library/send.png";

        public static const MAPS_ICONS_LIBRARY_SEPARATOR:String = "../maps/icons/library/separator.png";

        public static const MAPS_ICONS_LIBRARY_SERVERALERT:String = "../maps/icons/library/serverAlert.png";

        public static const MAPS_ICONS_LIBRARY_SERVERREBOOTCANCELLEDICON_1:String = "../maps/icons/library/ServerRebootCancelledIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_SERVERREBOOTICON_1:String = "../maps/icons/library/ServerRebootIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_SHEET:String = "../maps/icons/library/sheet.png";

        public static const MAPS_ICONS_LIBRARY_SLOTPLUSICON:String = "../maps/icons/library/slotPlusIcon.png";

        public static const MAPS_ICONS_LIBRARY_SORTIEINVITEICON_1:String = "../maps/icons/library/sortieInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_SQUADINVITEICON_1:String = "../maps/icons/library/squadInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_STORAGE_ICON:String = "../maps/icons/library/storage_icon.png";

        public static const MAPS_ICONS_LIBRARY_SWORDSICON:String = "../maps/icons/library/swordsIcon.png";

        public static const MAPS_ICONS_LIBRARY_TANK:String = "../maps/icons/library/tank.png";

        public static const MAPS_ICONS_LIBRARY_TANKITEM_BUY_SLOT:String = "../maps/icons/library/TankItem_buy_slot.png";

        public static const MAPS_ICONS_LIBRARY_TANKITEM_BUY_TANK:String = "../maps/icons/library/TankItem_buy_tank.png";

        public static const MAPS_ICONS_LIBRARY_TANKITEM_BUY_TANK_POPOVER_SMALL:String = "../maps/icons/library/TankItem_buy_tank_popover_small.png";

        public static const MAPS_ICONS_LIBRARY_TANKMAN:String = "../maps/icons/library/tankman.png";

        public static const MAPS_ICONS_LIBRARY_TEAMINFLUENCE:String = "../maps/icons/library/teamInfluence.png";

        public static const MAPS_ICONS_LIBRARY_TIMERICON:String = "../maps/icons/library/timerIcon.png";

        public static const MAPS_ICONS_LIBRARY_TIME_ICON:String = "../maps/icons/library/time_icon.png";

        public static const MAPS_ICONS_LIBRARY_TOURNAMENTBATTLERESULTICON_1:String = "../maps/icons/library/TournamentBattleResultIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_TRADE_IN:String = "../maps/icons/library/trade_in.png";

        public static const MAPS_ICONS_LIBRARY_TRAININGINVITEICON_1:String = "../maps/icons/library/trainingInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_TUTORIALINFOICON:String = "../maps/icons/library/tutorialInfoIcon.png";

        public static const MAPS_ICONS_LIBRARY_UNITINVITEICON_1:String = "../maps/icons/library/unitInviteIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_UNLOCKPRICE:String = "../maps/icons/library/UnlockPrice.png";

        public static const MAPS_ICONS_LIBRARY_USA_A12_T32:String = "../maps/icons/library/usa-A12_T32.png";

        public static const MAPS_ICONS_LIBRARY_USSR_OBSERVER:String = "../maps/icons/library/ussr-Observer.png";

        public static const MAPS_ICONS_LIBRARY_VEHICLEELITEICON:String = "../maps/icons/library/VehicleEliteIcon.png";

        public static const MAPS_ICONS_LIBRARY_VEHICLELOCK_1:String = "../maps/icons/library/VehicleLock-1.png";

        public static const MAPS_ICONS_LIBRARY_VEHICLE_DEFAULT:String = "../maps/icons/library/vehicle_default.png";

        public static const MAPS_ICONS_LIBRARY_VIGNETTE:String = "../maps/icons/library/vignette.png";

        public static const MAPS_ICONS_LIBRARY_WARNINGICON_1:String = "../maps/icons/library/WarningIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_WHITE_BG:String = "../maps/icons/library/white_bg.png";

        public static const MAPS_ICONS_LIBRARY_WTRICON_24:String = "../maps/icons/library/wtrIcon_24.png";

        public static const MAPS_ICONS_LIBRARY_XPCOSTICON_1:String = "../maps/icons/library/XPCostIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_XPCOSTICON_2:String = "../maps/icons/library/XpCostIcon-2.png";

        public static const MAPS_ICONS_LIBRARY_XPCOSTICON:String = "../maps/icons/library/XpCostIcon.png";

        public static const MAPS_ICONS_LIBRARY_XPCOSTICONBIG:String = "../maps/icons/library/XpCostIconBig.png";

        public static const MAPS_ICONS_LIBRARY_XPICON_1:String = "../maps/icons/library/XpIcon-1.png";

        public static const MAPS_ICONS_LIBRARY_XPICON:String = "../maps/icons/library/XpIcon.png";

        public static const MAPS_ICONS_LIBRARY_XPICONBIG_1:String = "../maps/icons/library/XpIconBig-1.png";

        public static const MAPS_ICONS_LIBRARY_XPICONBIG_2:String = "../maps/icons/library/XpIconBig-2.png";

        public static const MAPS_ICONS_LIBRARY_XPICONBIG:String = "../maps/icons/library/XpIconBig.png";

        public static const MAPS_ICONS_LIBRARY_XPICONINACTIVE_1:String = "../maps/icons/library/XpIconInactive-1.png";

        public static const MAPS_ICONS_LIBRARY_XPICON_23X22:String = "../maps/icons/library/xpIcon_23x22.png";

        public static const MAPS_ICONS_LIBRARY_YELLOWCHECK:String = "../maps/icons/library/yellowCheck.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_10:String = "../maps/icons/library/badges/110x110/badge_10.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_11:String = "../maps/icons/library/badges/110x110/badge_11.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_12:String = "../maps/icons/library/badges/110x110/badge_12.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_13:String = "../maps/icons/library/badges/110x110/badge_13.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_14:String = "../maps/icons/library/badges/110x110/badge_14.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_15:String = "../maps/icons/library/badges/110x110/badge_15.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_16:String = "../maps/icons/library/badges/110x110/badge_16.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_17:String = "../maps/icons/library/badges/110x110/badge_17.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_18:String = "../maps/icons/library/badges/110x110/badge_18.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_36:String = "../maps/icons/library/badges/110x110/badge_36.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_37:String = "../maps/icons/library/badges/110x110/badge_37.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_38:String = "../maps/icons/library/badges/110x110/badge_38.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_39:String = "../maps/icons/library/badges/110x110/badge_39.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_40:String = "../maps/icons/library/badges/110x110/badge_40.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_41:String = "../maps/icons/library/badges/110x110/badge_41.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_42:String = "../maps/icons/library/badges/110x110/badge_42.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_43:String = "../maps/icons/library/badges/110x110/badge_43.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_44:String = "../maps/icons/library/badges/110x110/badge_44.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_45:String = "../maps/icons/library/badges/110x110/badge_45.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_46:String = "../maps/icons/library/badges/110x110/badge_46.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_47:String = "../maps/icons/library/badges/110x110/badge_47.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_56:String = "../maps/icons/library/badges/110x110/badge_56.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_57:String = "../maps/icons/library/badges/110x110/badge_57.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_58:String = "../maps/icons/library/badges/110x110/badge_58.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_59:String = "../maps/icons/library/badges/110x110/badge_59.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_64:String = "../maps/icons/library/badges/110x110/badge_64.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_65:String = "../maps/icons/library/badges/110x110/badge_65.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_66:String = "../maps/icons/library/badges/110x110/badge_66.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_67:String = "../maps/icons/library/badges/110x110/badge_67.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_68:String = "../maps/icons/library/badges/110x110/badge_68.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_69:String = "../maps/icons/library/badges/110x110/badge_69.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_70:String = "../maps/icons/library/badges/110x110/badge_70.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_71:String = "../maps/icons/library/badges/110x110/badge_71.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_110X110_BADGE_76:String = "../maps/icons/library/badges/110x110/badge_76.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_10:String = "../maps/icons/library/badges/220x220/badge_10.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_11:String = "../maps/icons/library/badges/220x220/badge_11.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_12:String = "../maps/icons/library/badges/220x220/badge_12.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_13:String = "../maps/icons/library/badges/220x220/badge_13.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_14:String = "../maps/icons/library/badges/220x220/badge_14.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_15:String = "../maps/icons/library/badges/220x220/badge_15.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_16:String = "../maps/icons/library/badges/220x220/badge_16.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_17:String = "../maps/icons/library/badges/220x220/badge_17.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_18:String = "../maps/icons/library/badges/220x220/badge_18.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_36:String = "../maps/icons/library/badges/220x220/badge_36.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_37:String = "../maps/icons/library/badges/220x220/badge_37.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_38:String = "../maps/icons/library/badges/220x220/badge_38.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_39:String = "../maps/icons/library/badges/220x220/badge_39.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_40:String = "../maps/icons/library/badges/220x220/badge_40.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_41:String = "../maps/icons/library/badges/220x220/badge_41.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_42:String = "../maps/icons/library/badges/220x220/badge_42.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_43:String = "../maps/icons/library/badges/220x220/badge_43.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_44:String = "../maps/icons/library/badges/220x220/badge_44.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_45:String = "../maps/icons/library/badges/220x220/badge_45.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_46:String = "../maps/icons/library/badges/220x220/badge_46.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_47:String = "../maps/icons/library/badges/220x220/badge_47.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_52:String = "../maps/icons/library/badges/220x220/badge_52.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_56:String = "../maps/icons/library/badges/220x220/badge_56.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_57:String = "../maps/icons/library/badges/220x220/badge_57.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_58:String = "../maps/icons/library/badges/220x220/badge_58.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_59:String = "../maps/icons/library/badges/220x220/badge_59.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_64:String = "../maps/icons/library/badges/220x220/badge_64.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_65:String = "../maps/icons/library/badges/220x220/badge_65.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_66:String = "../maps/icons/library/badges/220x220/badge_66.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_67:String = "../maps/icons/library/badges/220x220/badge_67.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_68:String = "../maps/icons/library/badges/220x220/badge_68.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_69:String = "../maps/icons/library/badges/220x220/badge_69.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_70:String = "../maps/icons/library/badges/220x220/badge_70.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_71:String = "../maps/icons/library/badges/220x220/badge_71.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_72:String = "../maps/icons/library/badges/220x220/badge_72.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_73:String = "../maps/icons/library/badges/220x220/badge_73.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_74:String = "../maps/icons/library/badges/220x220/badge_74.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_76:String = "../maps/icons/library/badges/220x220/badge_76.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_79:String = "../maps/icons/library/badges/220x220/badge_79.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_80:String = "../maps/icons/library/badges/220x220/badge_80.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_81:String = "../maps/icons/library/badges/220x220/badge_81.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_82:String = "../maps/icons/library/badges/220x220/badge_82.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_83:String = "../maps/icons/library/badges/220x220/badge_83.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_84:String = "../maps/icons/library/badges/220x220/badge_84.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_85:String = "../maps/icons/library/badges/220x220/badge_85.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_220X220_BADGE_86:String = "../maps/icons/library/badges/220x220/badge_86.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_1:String = "../maps/icons/library/badges/24x24/badge_1.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_10:String = "../maps/icons/library/badges/24x24/badge_10.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_11:String = "../maps/icons/library/badges/24x24/badge_11.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_12:String = "../maps/icons/library/badges/24x24/badge_12.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_13:String = "../maps/icons/library/badges/24x24/badge_13.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_14:String = "../maps/icons/library/badges/24x24/badge_14.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_15:String = "../maps/icons/library/badges/24x24/badge_15.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_16:String = "../maps/icons/library/badges/24x24/badge_16.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_17:String = "../maps/icons/library/badges/24x24/badge_17.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_18:String = "../maps/icons/library/badges/24x24/badge_18.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_19:String = "../maps/icons/library/badges/24x24/badge_19.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_2:String = "../maps/icons/library/badges/24x24/badge_2.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_20:String = "../maps/icons/library/badges/24x24/badge_20.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_21:String = "../maps/icons/library/badges/24x24/badge_21.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_22:String = "../maps/icons/library/badges/24x24/badge_22.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_23:String = "../maps/icons/library/badges/24x24/badge_23.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_24:String = "../maps/icons/library/badges/24x24/badge_24.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_25:String = "../maps/icons/library/badges/24x24/badge_25.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_26:String = "../maps/icons/library/badges/24x24/badge_26.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_27:String = "../maps/icons/library/badges/24x24/badge_27.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_28:String = "../maps/icons/library/badges/24x24/badge_28.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_29:String = "../maps/icons/library/badges/24x24/badge_29.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_3:String = "../maps/icons/library/badges/24x24/badge_3.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_30:String = "../maps/icons/library/badges/24x24/badge_30.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_31:String = "../maps/icons/library/badges/24x24/badge_31.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_32:String = "../maps/icons/library/badges/24x24/badge_32.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_33:String = "../maps/icons/library/badges/24x24/badge_33.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_34:String = "../maps/icons/library/badges/24x24/badge_34.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_35:String = "../maps/icons/library/badges/24x24/badge_35.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_36:String = "../maps/icons/library/badges/24x24/badge_36.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_37:String = "../maps/icons/library/badges/24x24/badge_37.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_38:String = "../maps/icons/library/badges/24x24/badge_38.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_39:String = "../maps/icons/library/badges/24x24/badge_39.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_4:String = "../maps/icons/library/badges/24x24/badge_4.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_40:String = "../maps/icons/library/badges/24x24/badge_40.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_41:String = "../maps/icons/library/badges/24x24/badge_41.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_42:String = "../maps/icons/library/badges/24x24/badge_42.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_43:String = "../maps/icons/library/badges/24x24/badge_43.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_44:String = "../maps/icons/library/badges/24x24/badge_44.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_45:String = "../maps/icons/library/badges/24x24/badge_45.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_46:String = "../maps/icons/library/badges/24x24/badge_46.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_47:String = "../maps/icons/library/badges/24x24/badge_47.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_48:String = "../maps/icons/library/badges/24x24/badge_48.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_49:String = "../maps/icons/library/badges/24x24/badge_49.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_5:String = "../maps/icons/library/badges/24x24/badge_5.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_50:String = "../maps/icons/library/badges/24x24/badge_50.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_51:String = "../maps/icons/library/badges/24x24/badge_51.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_52:String = "../maps/icons/library/badges/24x24/badge_52.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_56:String = "../maps/icons/library/badges/24x24/badge_56.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_57:String = "../maps/icons/library/badges/24x24/badge_57.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_58:String = "../maps/icons/library/badges/24x24/badge_58.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_59:String = "../maps/icons/library/badges/24x24/badge_59.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_6:String = "../maps/icons/library/badges/24x24/badge_6.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_60:String = "../maps/icons/library/badges/24x24/badge_60.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_61:String = "../maps/icons/library/badges/24x24/badge_61.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_62:String = "../maps/icons/library/badges/24x24/badge_62.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_63:String = "../maps/icons/library/badges/24x24/badge_63.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_64:String = "../maps/icons/library/badges/24x24/badge_64.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_65:String = "../maps/icons/library/badges/24x24/badge_65.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_66:String = "../maps/icons/library/badges/24x24/badge_66.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_67:String = "../maps/icons/library/badges/24x24/badge_67.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_68:String = "../maps/icons/library/badges/24x24/badge_68.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_69:String = "../maps/icons/library/badges/24x24/badge_69.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_7:String = "../maps/icons/library/badges/24x24/badge_7.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_70:String = "../maps/icons/library/badges/24x24/badge_70.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_71:String = "../maps/icons/library/badges/24x24/badge_71.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_72:String = "../maps/icons/library/badges/24x24/badge_72.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_73:String = "../maps/icons/library/badges/24x24/badge_73.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_74:String = "../maps/icons/library/badges/24x24/badge_74.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_75:String = "../maps/icons/library/badges/24x24/badge_75.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_76:String = "../maps/icons/library/badges/24x24/badge_76.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_77:String = "../maps/icons/library/badges/24x24/badge_77.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_78:String = "../maps/icons/library/badges/24x24/badge_78.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_79:String = "../maps/icons/library/badges/24x24/badge_79.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_8:String = "../maps/icons/library/badges/24x24/badge_8.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_80:String = "../maps/icons/library/badges/24x24/badge_80.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_81:String = "../maps/icons/library/badges/24x24/badge_81.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_82:String = "../maps/icons/library/badges/24x24/badge_82.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_83:String = "../maps/icons/library/badges/24x24/badge_83.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_84:String = "../maps/icons/library/badges/24x24/badge_84.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_85:String = "../maps/icons/library/badges/24x24/badge_85.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_86:String = "../maps/icons/library/badges/24x24/badge_86.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_87:String = "../maps/icons/library/badges/24x24/badge_87.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_24X24_BADGE_9:String = "../maps/icons/library/badges/24x24/badge_9.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_1:String = "../maps/icons/library/badges/48x48/badge_1.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_10:String = "../maps/icons/library/badges/48x48/badge_10.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_11:String = "../maps/icons/library/badges/48x48/badge_11.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_12:String = "../maps/icons/library/badges/48x48/badge_12.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_13:String = "../maps/icons/library/badges/48x48/badge_13.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_14:String = "../maps/icons/library/badges/48x48/badge_14.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_15:String = "../maps/icons/library/badges/48x48/badge_15.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_16:String = "../maps/icons/library/badges/48x48/badge_16.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_17:String = "../maps/icons/library/badges/48x48/badge_17.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_18:String = "../maps/icons/library/badges/48x48/badge_18.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_19:String = "../maps/icons/library/badges/48x48/badge_19.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_2:String = "../maps/icons/library/badges/48x48/badge_2.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_20:String = "../maps/icons/library/badges/48x48/badge_20.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_21:String = "../maps/icons/library/badges/48x48/badge_21.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_22:String = "../maps/icons/library/badges/48x48/badge_22.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_23:String = "../maps/icons/library/badges/48x48/badge_23.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_24:String = "../maps/icons/library/badges/48x48/badge_24.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_25:String = "../maps/icons/library/badges/48x48/badge_25.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_26:String = "../maps/icons/library/badges/48x48/badge_26.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_27:String = "../maps/icons/library/badges/48x48/badge_27.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_28:String = "../maps/icons/library/badges/48x48/badge_28.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_29:String = "../maps/icons/library/badges/48x48/badge_29.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_3:String = "../maps/icons/library/badges/48x48/badge_3.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_30:String = "../maps/icons/library/badges/48x48/badge_30.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_31:String = "../maps/icons/library/badges/48x48/badge_31.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_32:String = "../maps/icons/library/badges/48x48/badge_32.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_33:String = "../maps/icons/library/badges/48x48/badge_33.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_34:String = "../maps/icons/library/badges/48x48/badge_34.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_35:String = "../maps/icons/library/badges/48x48/badge_35.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_36:String = "../maps/icons/library/badges/48x48/badge_36.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_37:String = "../maps/icons/library/badges/48x48/badge_37.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_38:String = "../maps/icons/library/badges/48x48/badge_38.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_39:String = "../maps/icons/library/badges/48x48/badge_39.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_4:String = "../maps/icons/library/badges/48x48/badge_4.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_40:String = "../maps/icons/library/badges/48x48/badge_40.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_41:String = "../maps/icons/library/badges/48x48/badge_41.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_42:String = "../maps/icons/library/badges/48x48/badge_42.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_43:String = "../maps/icons/library/badges/48x48/badge_43.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_44:String = "../maps/icons/library/badges/48x48/badge_44.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_45:String = "../maps/icons/library/badges/48x48/badge_45.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_46:String = "../maps/icons/library/badges/48x48/badge_46.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_47:String = "../maps/icons/library/badges/48x48/badge_47.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_48:String = "../maps/icons/library/badges/48x48/badge_48.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_49:String = "../maps/icons/library/badges/48x48/badge_49.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_5:String = "../maps/icons/library/badges/48x48/badge_5.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_50:String = "../maps/icons/library/badges/48x48/badge_50.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_51:String = "../maps/icons/library/badges/48x48/badge_51.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_52:String = "../maps/icons/library/badges/48x48/badge_52.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_56:String = "../maps/icons/library/badges/48x48/badge_56.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_57:String = "../maps/icons/library/badges/48x48/badge_57.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_58:String = "../maps/icons/library/badges/48x48/badge_58.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_59:String = "../maps/icons/library/badges/48x48/badge_59.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_6:String = "../maps/icons/library/badges/48x48/badge_6.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_60:String = "../maps/icons/library/badges/48x48/badge_60.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_61:String = "../maps/icons/library/badges/48x48/badge_61.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_62:String = "../maps/icons/library/badges/48x48/badge_62.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_63:String = "../maps/icons/library/badges/48x48/badge_63.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_64:String = "../maps/icons/library/badges/48x48/badge_64.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_65:String = "../maps/icons/library/badges/48x48/badge_65.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_66:String = "../maps/icons/library/badges/48x48/badge_66.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_67:String = "../maps/icons/library/badges/48x48/badge_67.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_68:String = "../maps/icons/library/badges/48x48/badge_68.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_69:String = "../maps/icons/library/badges/48x48/badge_69.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_7:String = "../maps/icons/library/badges/48x48/badge_7.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_70:String = "../maps/icons/library/badges/48x48/badge_70.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_71:String = "../maps/icons/library/badges/48x48/badge_71.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_72:String = "../maps/icons/library/badges/48x48/badge_72.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_73:String = "../maps/icons/library/badges/48x48/badge_73.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_74:String = "../maps/icons/library/badges/48x48/badge_74.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_75:String = "../maps/icons/library/badges/48x48/badge_75.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_76:String = "../maps/icons/library/badges/48x48/badge_76.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_77:String = "../maps/icons/library/badges/48x48/badge_77.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_78:String = "../maps/icons/library/badges/48x48/badge_78.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_79:String = "../maps/icons/library/badges/48x48/badge_79.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_8:String = "../maps/icons/library/badges/48x48/badge_8.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_80:String = "../maps/icons/library/badges/48x48/badge_80.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_81:String = "../maps/icons/library/badges/48x48/badge_81.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_82:String = "../maps/icons/library/badges/48x48/badge_82.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_83:String = "../maps/icons/library/badges/48x48/badge_83.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_84:String = "../maps/icons/library/badges/48x48/badge_84.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_85:String = "../maps/icons/library/badges/48x48/badge_85.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_86:String = "../maps/icons/library/badges/48x48/badge_86.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_87:String = "../maps/icons/library/badges/48x48/badge_87.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_9:String = "../maps/icons/library/badges/48x48/badge_9.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_DEFAULT:String = "../maps/icons/library/badges/48x48/badge_default.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_48X48_BADGE_EMPTY:String = "../maps/icons/library/badges/48x48/badge_empty.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_1:String = "../maps/icons/library/badges/80x80/badge_1.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_10:String = "../maps/icons/library/badges/80x80/badge_10.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_11:String = "../maps/icons/library/badges/80x80/badge_11.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_12:String = "../maps/icons/library/badges/80x80/badge_12.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_13:String = "../maps/icons/library/badges/80x80/badge_13.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_14:String = "../maps/icons/library/badges/80x80/badge_14.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_15:String = "../maps/icons/library/badges/80x80/badge_15.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_16:String = "../maps/icons/library/badges/80x80/badge_16.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_17:String = "../maps/icons/library/badges/80x80/badge_17.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_18:String = "../maps/icons/library/badges/80x80/badge_18.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_19:String = "../maps/icons/library/badges/80x80/badge_19.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_2:String = "../maps/icons/library/badges/80x80/badge_2.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_20:String = "../maps/icons/library/badges/80x80/badge_20.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_21:String = "../maps/icons/library/badges/80x80/badge_21.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_22:String = "../maps/icons/library/badges/80x80/badge_22.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_23:String = "../maps/icons/library/badges/80x80/badge_23.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_24:String = "../maps/icons/library/badges/80x80/badge_24.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_25:String = "../maps/icons/library/badges/80x80/badge_25.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_26:String = "../maps/icons/library/badges/80x80/badge_26.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_27:String = "../maps/icons/library/badges/80x80/badge_27.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_28:String = "../maps/icons/library/badges/80x80/badge_28.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_29:String = "../maps/icons/library/badges/80x80/badge_29.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_3:String = "../maps/icons/library/badges/80x80/badge_3.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_30:String = "../maps/icons/library/badges/80x80/badge_30.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_31:String = "../maps/icons/library/badges/80x80/badge_31.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_32:String = "../maps/icons/library/badges/80x80/badge_32.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_33:String = "../maps/icons/library/badges/80x80/badge_33.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_34:String = "../maps/icons/library/badges/80x80/badge_34.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_35:String = "../maps/icons/library/badges/80x80/badge_35.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_36:String = "../maps/icons/library/badges/80x80/badge_36.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_37:String = "../maps/icons/library/badges/80x80/badge_37.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_38:String = "../maps/icons/library/badges/80x80/badge_38.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_39:String = "../maps/icons/library/badges/80x80/badge_39.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_4:String = "../maps/icons/library/badges/80x80/badge_4.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_40:String = "../maps/icons/library/badges/80x80/badge_40.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_41:String = "../maps/icons/library/badges/80x80/badge_41.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_42:String = "../maps/icons/library/badges/80x80/badge_42.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_43:String = "../maps/icons/library/badges/80x80/badge_43.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_44:String = "../maps/icons/library/badges/80x80/badge_44.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_45:String = "../maps/icons/library/badges/80x80/badge_45.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_46:String = "../maps/icons/library/badges/80x80/badge_46.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_47:String = "../maps/icons/library/badges/80x80/badge_47.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_48:String = "../maps/icons/library/badges/80x80/badge_48.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_49:String = "../maps/icons/library/badges/80x80/badge_49.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_5:String = "../maps/icons/library/badges/80x80/badge_5.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_50:String = "../maps/icons/library/badges/80x80/badge_50.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_51:String = "../maps/icons/library/badges/80x80/badge_51.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_52:String = "../maps/icons/library/badges/80x80/badge_52.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_56:String = "../maps/icons/library/badges/80x80/badge_56.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_57:String = "../maps/icons/library/badges/80x80/badge_57.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_58:String = "../maps/icons/library/badges/80x80/badge_58.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_59:String = "../maps/icons/library/badges/80x80/badge_59.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_6:String = "../maps/icons/library/badges/80x80/badge_6.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_60:String = "../maps/icons/library/badges/80x80/badge_60.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_61:String = "../maps/icons/library/badges/80x80/badge_61.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_62:String = "../maps/icons/library/badges/80x80/badge_62.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_63:String = "../maps/icons/library/badges/80x80/badge_63.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_64:String = "../maps/icons/library/badges/80x80/badge_64.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_65:String = "../maps/icons/library/badges/80x80/badge_65.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_66:String = "../maps/icons/library/badges/80x80/badge_66.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_67:String = "../maps/icons/library/badges/80x80/badge_67.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_68:String = "../maps/icons/library/badges/80x80/badge_68.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_69:String = "../maps/icons/library/badges/80x80/badge_69.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_7:String = "../maps/icons/library/badges/80x80/badge_7.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_70:String = "../maps/icons/library/badges/80x80/badge_70.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_71:String = "../maps/icons/library/badges/80x80/badge_71.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_72:String = "../maps/icons/library/badges/80x80/badge_72.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_73:String = "../maps/icons/library/badges/80x80/badge_73.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_74:String = "../maps/icons/library/badges/80x80/badge_74.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_75:String = "../maps/icons/library/badges/80x80/badge_75.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_76:String = "../maps/icons/library/badges/80x80/badge_76.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_77:String = "../maps/icons/library/badges/80x80/badge_77.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_78:String = "../maps/icons/library/badges/80x80/badge_78.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_79:String = "../maps/icons/library/badges/80x80/badge_79.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_8:String = "../maps/icons/library/badges/80x80/badge_8.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_80:String = "../maps/icons/library/badges/80x80/badge_80.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_81:String = "../maps/icons/library/badges/80x80/badge_81.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_82:String = "../maps/icons/library/badges/80x80/badge_82.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_83:String = "../maps/icons/library/badges/80x80/badge_83.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_84:String = "../maps/icons/library/badges/80x80/badge_84.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_85:String = "../maps/icons/library/badges/80x80/badge_85.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_86:String = "../maps/icons/library/badges/80x80/badge_86.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_87:String = "../maps/icons/library/badges/80x80/badge_87.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_9:String = "../maps/icons/library/badges/80x80/badge_9.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_80X80_BADGE_EMPTY:String = "../maps/icons/library/badges/80x80/badge_empty.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_HIGHLIGHTS_HIGHLIGHTS_AQUAMARINE:String = "../maps/icons/library/badges/highlights/highlights_aquamarine.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_HIGHLIGHTS_HIGHLIGHTS_GOLD:String = "../maps/icons/library/badges/highlights/highlights_gold.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_HIGHLIGHTS_HIGHLIGHTS_GREEN:String = "../maps/icons/library/badges/highlights/highlights_green.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_HIGHLIGHTS_HIGHLIGHTS_RED:String = "../maps/icons/library/badges/highlights/highlights_red.png";

        public static const MAPS_ICONS_LIBRARY_BADGES_HIGHLIGHTS_HIGHLIGHTS_VIOLET:String = "../maps/icons/library/badges/highlights/highlights_violet.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_BERTHS:String = "../maps/icons/library/bonuses/berths.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_CAMOUFLAGES:String = "../maps/icons/library/bonuses/camouflages.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_DEFAULT:String = "../maps/icons/library/bonuses/default.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_EMBLEMS:String = "../maps/icons/library/bonuses/emblems.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_INSCRIPTIONS:String = "../maps/icons/library/bonuses/inscriptions.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_SLOTS:String = "../maps/icons/library/bonuses/slots.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_TANKMEN:String = "../maps/icons/library/bonuses/tankmen.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_TANKMENXP:String = "../maps/icons/library/bonuses/tankmenXP.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_TANKWOMEN:String = "../maps/icons/library/bonuses/tankwomen.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_TOKENS:String = "../maps/icons/library/bonuses/tokens.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_VEHICLEDISCOUNT:String = "../maps/icons/library/bonuses/vehicleDiscount.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_VEHICLES:String = "../maps/icons/library/bonuses/vehicles.png";

        public static const MAPS_ICONS_LIBRARY_BONUSES_VEHICLES_RENT:String = "../maps/icons/library/bonuses/vehicles_rent.png";

        public static const MAPS_ICONS_LIBRARY_CRITICAL_DAMAGE_HIT_BLOCKED:String = "../maps/icons/library/critical_damage/hit_blocked.png";

        public static const MAPS_ICONS_LIBRARY_CRITICAL_DAMAGE_HIT_CRITICAL:String = "../maps/icons/library/critical_damage/hit_critical.png";

        public static const MAPS_ICONS_LIBRARY_CRITICAL_DAMAGE_HIT_CRITICAL_TRACK:String = "../maps/icons/library/critical_damage/hit_critical_track.png";

        public static const MAPS_ICONS_LIBRARY_CRITICAL_DAMAGE_HIT_RICOCHET:String = "../maps/icons/library/critical_damage/hit_ricochet.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_AMMOBAYCRITICALSMALL:String = "../maps/icons/library/crits/ammoBayCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_AMMOBAYDESTROYEDSMALL:String = "../maps/icons/library/crits/ammoBayDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_COMMANDERDESTROYEDSMALL:String = "../maps/icons/library/crits/commanderDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_DRIVERDESTROYEDSMALL:String = "../maps/icons/library/crits/driverDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_ENGINECRITICALSMALL:String = "../maps/icons/library/crits/engineCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_ENGINEDESTROYEDSMALL:String = "../maps/icons/library/crits/engineDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_FUELTANKCRITICALSMALL:String = "../maps/icons/library/crits/fuelTankCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_FUELTANKDESTROYEDSMALL:String = "../maps/icons/library/crits/fuelTankDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_GUNCRITICALSMALL:String = "../maps/icons/library/crits/gunCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_GUNDESTROYEDSMALL:String = "../maps/icons/library/crits/gunDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_GUNNERDESTROYEDSMALL:String = "../maps/icons/library/crits/gunnerDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_LOADERDESTROYEDSMALL:String = "../maps/icons/library/crits/loaderDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_RADIOCRITICALSMALL:String = "../maps/icons/library/crits/radioCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_RADIODESTROYEDSMALL:String = "../maps/icons/library/crits/radioDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_RADIOMANDESTROYEDSMALL:String = "../maps/icons/library/crits/radiomanDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_SURVEYINGDEVICECRITICALSMALL:String = "../maps/icons/library/crits/surveyingDeviceCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_SURVEYINGDEVICEDESTROYEDSMALL:String = "../maps/icons/library/crits/surveyingDeviceDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_TRACKCRITICALSMALL:String = "../maps/icons/library/crits/trackCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_TRACKDESTROYEDSMALL:String = "../maps/icons/library/crits/trackDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_TURRETROTATORCRITICALSMALL:String = "../maps/icons/library/crits/turretRotatorCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_TURRETROTATORDESTROYEDSMALL:String = "../maps/icons/library/crits/turretRotatorDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_WHEELCRITICALSMALL:String = "../maps/icons/library/crits/wheelCriticalSmall.png";

        public static const MAPS_ICONS_LIBRARY_CRITS_WHEELDESTROYEDSMALL:String = "../maps/icons/library/crits/wheelDestroyedSmall.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ALERTICON:String = "../maps/icons/library/cybersport/alertIcon.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_CLOCKICON:String = "../maps/icons/library/cybersport/clockIcon.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ICONDOWN:String = "../maps/icons/library/cybersport/iconDown.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ICONUP:String = "../maps/icons/library/cybersport/iconUp.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_LOCKICON:String = "../maps/icons/library/cybersport/lockIcon.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_NOTAVAILABLEICON:String = "../maps/icons/library/cybersport/notAvailableIcon.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_REFRESHICON:String = "../maps/icons/library/cybersport/refreshIcon.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_RIBBON:String = "../maps/icons/library/cybersport/ribbon.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_TANKICON:String = "../maps/icons/library/cybersport/tankIcon.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_1A:String = "../maps/icons/library/cybersport/animation/bg/1a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_1B:String = "../maps/icons/library/cybersport/animation/bg/1b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_1C:String = "../maps/icons/library/cybersport/animation/bg/1c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_1D:String = "../maps/icons/library/cybersport/animation/bg/1d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_2A:String = "../maps/icons/library/cybersport/animation/bg/2a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_2B:String = "../maps/icons/library/cybersport/animation/bg/2b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_2C:String = "../maps/icons/library/cybersport/animation/bg/2c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_2D:String = "../maps/icons/library/cybersport/animation/bg/2d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_3A:String = "../maps/icons/library/cybersport/animation/bg/3a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_3B:String = "../maps/icons/library/cybersport/animation/bg/3b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_3C:String = "../maps/icons/library/cybersport/animation/bg/3c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_3D:String = "../maps/icons/library/cybersport/animation/bg/3d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_4A:String = "../maps/icons/library/cybersport/animation/bg/4a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_4B:String = "../maps/icons/library/cybersport/animation/bg/4b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_4C:String = "../maps/icons/library/cybersport/animation/bg/4c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_4D:String = "../maps/icons/library/cybersport/animation/bg/4d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_5A:String = "../maps/icons/library/cybersport/animation/bg/5a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_5B:String = "../maps/icons/library/cybersport/animation/bg/5b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_5C:String = "../maps/icons/library/cybersport/animation/bg/5c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_5D:String = "../maps/icons/library/cybersport/animation/bg/5d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_6A:String = "../maps/icons/library/cybersport/animation/bg/6a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_6B:String = "../maps/icons/library/cybersport/animation/bg/6b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_6C:String = "../maps/icons/library/cybersport/animation/bg/6c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_BG_6D:String = "../maps/icons/library/cybersport/animation/bg/6d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_1A:String = "../maps/icons/library/cybersport/animation/division/1a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_1B:String = "../maps/icons/library/cybersport/animation/division/1b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_1C:String = "../maps/icons/library/cybersport/animation/division/1c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_1D:String = "../maps/icons/library/cybersport/animation/division/1d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_2A:String = "../maps/icons/library/cybersport/animation/division/2a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_2B:String = "../maps/icons/library/cybersport/animation/division/2b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_2C:String = "../maps/icons/library/cybersport/animation/division/2c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_2D:String = "../maps/icons/library/cybersport/animation/division/2d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_3A:String = "../maps/icons/library/cybersport/animation/division/3a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_3B:String = "../maps/icons/library/cybersport/animation/division/3b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_3C:String = "../maps/icons/library/cybersport/animation/division/3c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_3D:String = "../maps/icons/library/cybersport/animation/division/3d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_4A:String = "../maps/icons/library/cybersport/animation/division/4a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_4B:String = "../maps/icons/library/cybersport/animation/division/4b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_4C:String = "../maps/icons/library/cybersport/animation/division/4c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_4D:String = "../maps/icons/library/cybersport/animation/division/4d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_5A:String = "../maps/icons/library/cybersport/animation/division/5a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_5B:String = "../maps/icons/library/cybersport/animation/division/5b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_5C:String = "../maps/icons/library/cybersport/animation/division/5c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_5D:String = "../maps/icons/library/cybersport/animation/division/5d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_6A:String = "../maps/icons/library/cybersport/animation/division/6a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_6B:String = "../maps/icons/library/cybersport/animation/division/6b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_6C:String = "../maps/icons/library/cybersport/animation/division/6c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_DIVISION_6D:String = "../maps/icons/library/cybersport/animation/division/6d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_1A:String = "../maps/icons/library/cybersport/animation/leaves/1a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_1B:String = "../maps/icons/library/cybersport/animation/leaves/1b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_1C:String = "../maps/icons/library/cybersport/animation/leaves/1c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_1D:String = "../maps/icons/library/cybersport/animation/leaves/1d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_2A:String = "../maps/icons/library/cybersport/animation/leaves/2a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_2B:String = "../maps/icons/library/cybersport/animation/leaves/2b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_2C:String = "../maps/icons/library/cybersport/animation/leaves/2c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_2D:String = "../maps/icons/library/cybersport/animation/leaves/2d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_3A:String = "../maps/icons/library/cybersport/animation/leaves/3a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_3B:String = "../maps/icons/library/cybersport/animation/leaves/3b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_3C:String = "../maps/icons/library/cybersport/animation/leaves/3c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_3D:String = "../maps/icons/library/cybersport/animation/leaves/3d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_4A:String = "../maps/icons/library/cybersport/animation/leaves/4a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_4B:String = "../maps/icons/library/cybersport/animation/leaves/4b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_4C:String = "../maps/icons/library/cybersport/animation/leaves/4c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_4D:String = "../maps/icons/library/cybersport/animation/leaves/4d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_5A:String = "../maps/icons/library/cybersport/animation/leaves/5a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_5B:String = "../maps/icons/library/cybersport/animation/leaves/5b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_5C:String = "../maps/icons/library/cybersport/animation/leaves/5c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_5D:String = "../maps/icons/library/cybersport/animation/leaves/5d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_6A:String = "../maps/icons/library/cybersport/animation/leaves/6a.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_6B:String = "../maps/icons/library/cybersport/animation/leaves/6b.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_6C:String = "../maps/icons/library/cybersport/animation/leaves/6c.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LEAVES_6D:String = "../maps/icons/library/cybersport/animation/leaves/6d.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LOGO_1:String = "../maps/icons/library/cybersport/animation/logo/1.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LOGO_2:String = "../maps/icons/library/cybersport/animation/logo/2.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LOGO_3:String = "../maps/icons/library/cybersport/animation/logo/3.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LOGO_4:String = "../maps/icons/library/cybersport/animation/logo/4.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LOGO_5:String = "../maps/icons/library/cybersport/animation/logo/5.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_ANIMATION_LOGO_6:String = "../maps/icons/library/cybersport/animation/logo/6.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_DEFAULT_16X16:String = "../maps/icons/library/cybersport/emblems/default_16x16.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_DEFAULT_24X24:String = "../maps/icons/library/cybersport/emblems/default_24x24.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_DEFAULT_256X256:String = "../maps/icons/library/cybersport/emblems/default_256x256.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_DEFAULT_32X32:String = "../maps/icons/library/cybersport/emblems/default_32x32.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_DEFAULT_64X64:String = "../maps/icons/library/cybersport/emblems/default_64x64.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_STUB_24X24:String = "../maps/icons/library/cybersport/emblems/stub_24x24.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_STUB_32X32:String = "../maps/icons/library/cybersport/emblems/stub_32x32.png";

        public static const MAPS_ICONS_LIBRARY_CYBERSPORT_EMBLEMS_STUB_64X64:String = "../maps/icons/library/cybersport/emblems/stub_64x64.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_ARMORUSING40X32:String = "../maps/icons/library/dossier/armorUsing40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_ASSIST40X32:String = "../maps/icons/library/dossier/assist40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGATTACKDMG40X32:String = "../maps/icons/library/dossier/avgAttackDmg40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGBATTLESCOUNT40X32:String = "../maps/icons/library/dossier/avgBattlesCount40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGDAMAGE40X32:String = "../maps/icons/library/dossier/avgDamage40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGDEFENCEDMG40X32:String = "../maps/icons/library/dossier/avgDefenceDmg40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGDEFES40X32:String = "../maps/icons/library/dossier/avgDefes40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGEXP40X32:String = "../maps/icons/library/dossier/avgExp40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGPERSONALRATING40X32:String = "../maps/icons/library/dossier/avgPersonalRating40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGVICTORYPOINTS40X32:String = "../maps/icons/library/dossier/avgVictoryPoints40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_AVGWINS40X32:String = "../maps/icons/library/dossier/avgWins40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_BATTLES40X32:String = "../maps/icons/library/dossier/battles40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_DEFRESRATIO40X32:String = "../maps/icons/library/dossier/defresRatio40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_DMGRATIO40X32:String = "../maps/icons/library/dossier/dmgRatio40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_HITS40X32:String = "../maps/icons/library/dossier/hits40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_HONORS40X32:String = "../maps/icons/library/dossier/honors40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_ICON_STATISTICS_AVERAGE_FRAGS_40X32:String = "../maps/icons/library/dossier/icon_statistics_average_frags_40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_KDR40X32:String = "../maps/icons/library/dossier/kdr40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_MARKOFMASTERY40X32:String = "../maps/icons/library/dossier/markOfMastery40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_MAXDESTROYED40X32:String = "../maps/icons/library/dossier/maxDestroyed40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_MAXEXP40X32:String = "../maps/icons/library/dossier/maxExp40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_MAXFRAGS40X32:String = "../maps/icons/library/dossier/maxFrags40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_MAXVICTORYPOINTS40X32:String = "../maps/icons/library/dossier/maxVictoryPoints40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_RANKPOINTS40X32:String = "../maps/icons/library/dossier/rankPoints40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_RANKSTAGEFACTOR40X32:String = "../maps/icons/library/dossier/rankStageFactor40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_RESOURCES40X32:String = "../maps/icons/library/dossier/resources40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_SEIZEDPROVINCES40X32:String = "../maps/icons/library/dossier/seizedProvinces40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_SURVIVAL40X32:String = "../maps/icons/library/dossier/survival40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_TECHDEFEAT40X32:String = "../maps/icons/library/dossier/techDefeat40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_TECHRATIO40X32:String = "../maps/icons/library/dossier/techRatio40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_WINS40X32:String = "../maps/icons/library/dossier/wins40x32.png";

        public static const MAPS_ICONS_LIBRARY_DOSSIER_WINSBYCAPTURE40X32:String = "../maps/icons/library/dossier/winsByCapture40x32.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_ARMOR:String = "../maps/icons/library/efficiency/48x48/armor.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_CAPTURE:String = "../maps/icons/library/efficiency/48x48/capture.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_DAMAGE:String = "../maps/icons/library/efficiency/48x48/damage.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_DEFENCE:String = "../maps/icons/library/efficiency/48x48/defence.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_DESTRUCTION:String = "../maps/icons/library/efficiency/48x48/destruction.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_DETECTION:String = "../maps/icons/library/efficiency/48x48/detection.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_FIRE:String = "../maps/icons/library/efficiency/48x48/fire.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_HELP:String = "../maps/icons/library/efficiency/48x48/help.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_IMMOBILIZED:String = "../maps/icons/library/efficiency/48x48/immobilized.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_MODULE:String = "../maps/icons/library/efficiency/48x48/module.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_RAM:String = "../maps/icons/library/efficiency/48x48/ram.png";

        public static const MAPS_ICONS_LIBRARY_EFFICIENCY_48X48_STUN:String = "../maps/icons/library/efficiency/48x48/stun.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSARMOUR:String = "../maps/icons/library/epicEfficiency/ribbonsArmour.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSASSISTSPOT:String = "../maps/icons/library/epicEfficiency/ribbonsAssistSpot.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSASSISTTRACK:String = "../maps/icons/library/epicEfficiency/ribbonsAssistTrack.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSBURN:String = "../maps/icons/library/epicEfficiency/ribbonsBurn.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSCAPTURE:String = "../maps/icons/library/epicEfficiency/ribbonsCapture.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSCRITS:String = "../maps/icons/library/epicEfficiency/ribbonsCrits.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSDAMAGE:String = "../maps/icons/library/epicEfficiency/ribbonsDamage.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSDEFENCE:String = "../maps/icons/library/epicEfficiency/ribbonsDefence.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSKILL:String = "../maps/icons/library/epicEfficiency/ribbonsKill.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSRAM:String = "../maps/icons/library/epicEfficiency/ribbonsRam.png";

        public static const MAPS_ICONS_LIBRARY_EPICEFFICIENCY_RIBBONSSPOTTED:String = "../maps/icons/library/epicEfficiency/ribbonsSpotted.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_BIG_RANK_CAPTAIN:String = "../maps/icons/library/epicRank/big_rank_captain.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_BIG_RANK_GENERAL:String = "../maps/icons/library/epicRank/big_rank_general.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_BIG_RANK_MAJOR:String = "../maps/icons/library/epicRank/big_rank_major.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_BIG_RANK_PRIVATE:String = "../maps/icons/library/epicRank/big_rank_private.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_BIG_RANK_RECRUIT:String = "../maps/icons/library/epicRank/big_rank_recruit.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_BIG_RANK_SERGEANT:String = "../maps/icons/library/epicRank/big_rank_sergeant.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_LIST_RANK_CAPTAIN:String = "../maps/icons/library/epicRank/list_rank_captain.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_LIST_RANK_GENERAL:String = "../maps/icons/library/epicRank/list_rank_general.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_LIST_RANK_MAJOR:String = "../maps/icons/library/epicRank/list_rank_major.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_LIST_RANK_PRIVATE:String = "../maps/icons/library/epicRank/list_rank_private.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_LIST_RANK_RECRUIT:String = "../maps/icons/library/epicRank/list_rank_recruit.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_LIST_RANK_SERGEANT:String = "../maps/icons/library/epicRank/list_rank_sergeant.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_MSG_RANK_CAPTAIN:String = "../maps/icons/library/epicRank/msg_rank_captain.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_MSG_RANK_GENERAL:String = "../maps/icons/library/epicRank/msg_rank_general.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_MSG_RANK_MAJOR:String = "../maps/icons/library/epicRank/msg_rank_major.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_MSG_RANK_PRIVATE:String = "../maps/icons/library/epicRank/msg_rank_private.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_MSG_RANK_RECRUIT:String = "../maps/icons/library/epicRank/msg_rank_recruit.png";

        public static const MAPS_ICONS_LIBRARY_EPICRANK_MSG_RANK_SERGEANT:String = "../maps/icons/library/epicRank/msg_rank_sergeant.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_AIM:String = "../maps/icons/library/fortification/aim.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_BATTLEFORTDEFEAT:String = "../maps/icons/library/fortification/battleFortDefeat.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_BATTLEFORTDRAW:String = "../maps/icons/library/fortification/battleFortDraw.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_BATTLEFORTVICTORY:String = "../maps/icons/library/fortification/battleFortVictory.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_CHECKMARK:String = "../maps/icons/library/fortification/checkmark.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_CLOCK:String = "../maps/icons/library/fortification/clock.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_CLOCK2:String = "../maps/icons/library/fortification/clock2.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_DEFENCE:String = "../maps/icons/library/fortification/defence.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_DEFENCEFUTURE:String = "../maps/icons/library/fortification/defenceFuture.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_DEFENCEFUTUREBG:String = "../maps/icons/library/fortification/defenceFutureBg.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_DEFENCEPAST:String = "../maps/icons/library/fortification/defencePast.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_DEFENCEPASTBG:String = "../maps/icons/library/fortification/defencePastBg.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_DESTRUCTION:String = "../maps/icons/library/fortification/destruction.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_HUMANS:String = "../maps/icons/library/fortification/humans.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_INPROGRESS_PAUSE:String = "../maps/icons/library/fortification/inprogress_pause.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_LEGIONNAIRE:String = "../maps/icons/library/fortification/legionnaire.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_NOTAVAILABLEBG:String = "../maps/icons/library/fortification/notAvailableBg.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_NUT:String = "../maps/icons/library/fortification/nut.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_NUTINACTIVE:String = "../maps/icons/library/fortification/nutInactive.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_NUT_STAT:String = "../maps/icons/library/fortification/nut_stat.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_OFFENCE:String = "../maps/icons/library/fortification/offence.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_OFFENCEFUTURE:String = "../maps/icons/library/fortification/offenceFuture.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_OFFENCEPAST:String = "../maps/icons/library/fortification/offencePast.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_ORDER_IN_PROGRESS:String = "../maps/icons/library/fortification/order_in_progress.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_PROMRES_ICON4POSTBATTLE:String = "../maps/icons/library/fortification/promres-icon4postbattle.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_PROMRES_ICON4POSTBATTLE_TRANSP:String = "../maps/icons/library/fortification/promres-icon4postbattle_transp.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_RESERVE_INPROGRESS_24:String = "../maps/icons/library/fortification/reserve_inprogress_24.png";

        public static const MAPS_ICONS_LIBRARY_FORTIFICATION_USSR_T62A:String = "../maps/icons/library/fortification/ussr-T62A.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_BLUE:String = "../maps/icons/library/hangarFlag/flag_blue.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_FGREEN:String = "../maps/icons/library/hangarFlag/flag_fgreen.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_FYELLOW:String = "../maps/icons/library/hangarFlag/flag_fyellow.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_GREEN:String = "../maps/icons/library/hangarFlag/flag_green.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_ITALY:String = "../maps/icons/library/hangarFlag/flag_italy.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_KHACKI:String = "../maps/icons/library/hangarFlag/flag_khacki.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_KURSK:String = "../maps/icons/library/hangarFlag/flag_kursk.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_RED:String = "../maps/icons/library/hangarFlag/flag_red.png";

        public static const MAPS_ICONS_LIBRARY_HANGARFLAG_FLAG_VINOUS:String = "../maps/icons/library/hangarFlag/flag_vinous.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_ALERT_ICON:String = "../maps/icons/library/marathon/alert_icon.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_CUP_DISABLE_ICON:String = "../maps/icons/library/marathon/cup_disable_icon.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_CUP_ICON:String = "../maps/icons/library/marathon/cup_icon.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_ICON_FLAG:String = "../maps/icons/library/marathon/icon_flag.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_OK_ICON:String = "../maps/icons/library/marathon/ok_icon.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_SALE_ICON:String = "../maps/icons/library/marathon/sale_icon.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_TIME_ICON:String = "../maps/icons/library/marathon/time_icon.png";

        public static const MAPS_ICONS_LIBRARY_MARATHON_TIME_ICON_GLOW:String = "../maps/icons/library/marathon/time_icon_glow.png";

        public static const MAPS_ICONS_LIBRARY_OUTLINE_BEGINNER:String = "../maps/icons/library/outline/beginner.png";

        public static const MAPS_ICONS_LIBRARY_OUTLINE_PLUS:String = "../maps/icons/library/outline/plus.png";

        public static const MAPS_ICONS_LIBRARY_OUTLINE_QUESTS_ALL_DONE:String = "../maps/icons/library/outline/quests_all_done.png";

        public static const MAPS_ICONS_LIBRARY_OUTLINE_QUESTS_AVAILABLE:String = "../maps/icons/library/outline/quests_available.png";

        public static const MAPS_ICONS_LIBRARY_OUTLINE_QUESTS_DISABLED:String = "../maps/icons/library/outline/quests_disabled.png";

        public static const MAPS_ICONS_LIBRARY_OUTLINE_QUESTS_ON_PAUSE:String = "../maps/icons/library/outline/quests_on_pause.png";

        public static const MAPS_ICONS_LIBRARY_OUTLINE_REWARD:String = "../maps/icons/library/outline/reward.png";

        public static const MAPS_ICONS_LIBRARY_OVERRIDE_BADGES_24X24_OVERRIDE_BADGE_1:String = "../maps/icons/library/override_badges/24x24/override_badge_1.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_1:String = "../maps/icons/library/proficiency/class_icons_1.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_1_SMALL:String = "../maps/icons/library/proficiency/class_icons_1_small.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_2:String = "../maps/icons/library/proficiency/class_icons_2.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_2_SMALL:String = "../maps/icons/library/proficiency/class_icons_2_small.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_3:String = "../maps/icons/library/proficiency/class_icons_3.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_3_SMALL:String = "../maps/icons/library/proficiency/class_icons_3_small.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_4:String = "../maps/icons/library/proficiency/class_icons_4.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_4_SMALL:String = "../maps/icons/library/proficiency/class_icons_4_small.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_16X16_ALL:String = "../maps/icons/library/qualifiers/16x16/all.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_16X16_CAMOUFLAGE:String = "../maps/icons/library/qualifiers/16x16/camouflage.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_16X16_COMMANDER:String = "../maps/icons/library/qualifiers/16x16/commander.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_16X16_DRIVER:String = "../maps/icons/library/qualifiers/16x16/driver.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_16X16_GUNNER:String = "../maps/icons/library/qualifiers/16x16/gunner.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_16X16_LOADER:String = "../maps/icons/library/qualifiers/16x16/loader.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_16X16_RADIOMAN:String = "../maps/icons/library/qualifiers/16x16/radioman.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_42X42_ALL:String = "../maps/icons/library/qualifiers/42x42/all.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_42X42_CAMOUFLAGE:String = "../maps/icons/library/qualifiers/42x42/camouflage.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_42X42_COMMANDER:String = "../maps/icons/library/qualifiers/42x42/commander.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_42X42_DRIVER:String = "../maps/icons/library/qualifiers/42x42/driver.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_42X42_GUNNER:String = "../maps/icons/library/qualifiers/42x42/gunner.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_42X42_LOADER:String = "../maps/icons/library/qualifiers/42x42/loader.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_42X42_RADIOMAN:String = "../maps/icons/library/qualifiers/42x42/radioman.png";

        public static const MAPS_ICONS_LIBRARY_QUALIFIERS_48X48_CAMOUFLAGE:String = "../maps/icons/library/qualifiers/48x48/camouflage.png";

        public static const MAPS_ICONS_LIBRARY_STORE_CONDITION_OFF:String = "../maps/icons/library/store/condition_off.png";

        public static const MAPS_ICONS_LIBRARY_STORE_CONDITION_ON:String = "../maps/icons/library/store/condition_on.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_BUSY:String = "../maps/icons/library/userStatus/busy.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_BUSYBLIND:String = "../maps/icons/library/userStatus/busyBlind.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_MOB:String = "../maps/icons/library/userStatus/mob.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_OFFLINE:String = "../maps/icons/library/userStatus/offline.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_ONLINE:String = "../maps/icons/library/userStatus/online.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_WEB:String = "../maps/icons/library/userStatus/web.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_WOTB:String = "../maps/icons/library/userStatus/wotb.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_WOTG:String = "../maps/icons/library/userStatus/wotg.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_WOWP:String = "../maps/icons/library/userStatus/wowp.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_WOWS:String = "../maps/icons/library/userStatus/wows.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_BUSY:String = "../maps/icons/library/userStatus/small/busy.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_BUSYBLIND:String = "../maps/icons/library/userStatus/small/busyBlind.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_MOB:String = "../maps/icons/library/userStatus/small/mob.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_OFFLINE:String = "../maps/icons/library/userStatus/small/offline.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_ONLINE:String = "../maps/icons/library/userStatus/small/online.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_WEB:String = "../maps/icons/library/userStatus/small/web.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_WOTB:String = "../maps/icons/library/userStatus/small/wotb.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_WOTG:String = "../maps/icons/library/userStatus/small/wotg.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_WOWP:String = "../maps/icons/library/userStatus/small/wowp.png";

        public static const MAPS_ICONS_LIBRARY_USERSTATUS_SMALL_WOWS:String = "../maps/icons/library/userStatus/small/wows.png";

        public static const MAPS_ICONS_LINKEDSET_LINKEDSET_BGR_LANDING:String = "../maps/icons/linkedSet/linkedset_bgr_landing.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONICONITEM1:String = "../maps/icons/linkedSet/missionIconItem1.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONICONITEM2:String = "../maps/icons/linkedSet/missionIconItem2.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONICONITEM3:String = "../maps/icons/linkedSet/missionIconItem3.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONITEM1:String = "../maps/icons/linkedSet/missionItem1.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONITEM2:String = "../maps/icons/linkedSet/missionItem2.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONITEM2DISABLE:String = "../maps/icons/linkedSet/missionItem2disable.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONITEM3:String = "../maps/icons/linkedSet/missionItem3.png";

        public static const MAPS_ICONS_LINKEDSET_MISSIONITEM3DISABLE:String = "../maps/icons/linkedSet/missionItem3disable.png";

        public static const MAPS_ICONS_LINKEDSET_BACKGROUNDS_MISSIONITEM1BG:String = "../maps/icons/linkedSet/backgrounds/missionItem1Bg.png";

        public static const MAPS_ICONS_LINKEDSET_BACKGROUNDS_MISSIONITEM1COMPLETEBG:String = "../maps/icons/linkedSet/backgrounds/missionItem1CompleteBg.png";

        public static const MAPS_ICONS_LINKEDSET_BACKGROUNDS_MISSIONITEM2COMPLETEBG:String = "../maps/icons/linkedSet/backgrounds/missionItem2CompleteBg.png";

        public static const MAPS_ICONS_LINKEDSET_BACKGROUNDS_MISSIONITEM3COMPLETEBG:String = "../maps/icons/linkedSet/backgrounds/missionItem3CompleteBg.png";

        public static const MAPS_ICONS_LINKEDSET_PICTURES_MISSIONSET1BG1:String = "../maps/icons/linkedSet/pictures/missionSet1Bg1.png";

        public static const MAPS_ICONS_LINKEDSET_PICTURES_MISSIONSET3BG1:String = "../maps/icons/linkedSet/pictures/missionSet3Bg1.png";

        public static const MAPS_ICONS_LINKEDSET_PICTURES_MISSIONSET3BG2:String = "../maps/icons/linkedSet/pictures/missionSet3Bg2.png";

        public static const MAPS_ICONS_LINKEDSET_PICTURES_MISSIONSET3BG3:String = "../maps/icons/linkedSet/pictures/missionSet3Bg3.png";

        public static const MAPS_ICONS_LOBBY_EVENTPOPOVERBTNBG:String = "../maps/icons/lobby/eventPopoverBtnBG.png";

        public static const MAPS_ICONS_LOBBY_FALLOUTBATTLESELECTORBG:String = "../maps/icons/lobby/falloutBattleSelectorBG.png";

        public static const MAPS_ICONS_LOBBY_ICONBTNALT:String = "../maps/icons/lobby/iconBtnAlt.png";

        public static const MAPS_ICONS_LOBBY_ICON_PREMSHOP:String = "../maps/icons/lobby/icon_premshop.png";

        public static const MAPS_ICONS_LOBBY_REPORT_BUG_BACKGROUND:String = "../maps/icons/lobby/report-bug-background.png";

        public static const MAPS_ICONS_LOBBY_SETTINGS_BLURED_BG:String = "../maps/icons/lobby/settings_blured_bg.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ACHIEVES:String = "../maps/icons/manual/backgrounds/achieves.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_AMMO:String = "../maps/icons/manual/backgrounds/ammo.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ANTICIPATION:String = "../maps/icons/manual/backgrounds/anticipation.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_APRC_SHELL:String = "../maps/icons/manual/backgrounds/aprc_shell.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_AP_SHELL:String = "../maps/icons/manual/backgrounds/ap_shell.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ARMOR:String = "../maps/icons/manual/backgrounds/armor.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ASSAULT:String = "../maps/icons/manual/backgrounds/assault.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_AUTO_AIM:String = "../maps/icons/manual/backgrounds/auto_aim.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_BATTLEFIELD:String = "../maps/icons/manual/backgrounds/battlefield.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_BONES:String = "../maps/icons/manual/backgrounds/bones.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_BOOTCAMP:String = "../maps/icons/manual/backgrounds/bootcamp.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CAMOUFLAGE:String = "../maps/icons/manual/backgrounds/camouflage.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_COMMANDER:String = "../maps/icons/manual/backgrounds/commander.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CONDITION:String = "../maps/icons/manual/backgrounds/condition.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CONSUMABLES:String = "../maps/icons/manual/backgrounds/consumables.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CONTROLS:String = "../maps/icons/manual/backgrounds/controls.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CREDITS:String = "../maps/icons/manual/backgrounds/credits.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CREW:String = "../maps/icons/manual/backgrounds/crew.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CREW_TRAINING:String = "../maps/icons/manual/backgrounds/crew_training.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_CTF_GENERAL:String = "../maps/icons/manual/backgrounds/ctf_general.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_DAMAGE:String = "../maps/icons/manual/backgrounds/damage.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_DEVELOPMENT:String = "../maps/icons/manual/backgrounds/development.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_DOMINATION:String = "../maps/icons/manual/backgrounds/domination.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_DRIVER:String = "../maps/icons/manual/backgrounds/driver.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ELITE:String = "../maps/icons/manual/backgrounds/elite.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ENGINE:String = "../maps/icons/manual/backgrounds/engine.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_EQUIPMENT:String = "../maps/icons/manual/backgrounds/Equipment.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_EXPERIENCE:String = "../maps/icons/manual/backgrounds/experience.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_FLOODING:String = "../maps/icons/manual/backgrounds/flooding.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_FREE_EXPERIENCE:String = "../maps/icons/manual/backgrounds/free_experience.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_FUEL_TANK:String = "../maps/icons/manual/backgrounds/fuel_tank.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_GOLD:String = "../maps/icons/manual/backgrounds/gold.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_GUN:String = "../maps/icons/manual/backgrounds/gun.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_GUNNER:String = "../maps/icons/manual/backgrounds/gunner.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_HEAT_SHELL:String = "../maps/icons/manual/backgrounds/heat_shell.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_HEAVY_TANK:String = "../maps/icons/manual/backgrounds/heavy_tank.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_HE_SHELL:String = "../maps/icons/manual/backgrounds/he_shell.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_IMAGE_BACK:String = "../maps/icons/manual/backgrounds/image_back.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_IMPERCEPTIBILITY:String = "../maps/icons/manual/backgrounds/imperceptibility.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_LEVELS:String = "../maps/icons/manual/backgrounds/levels.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_LIGHT_TANK:String = "../maps/icons/manual/backgrounds/light_tank.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_LOADER:String = "../maps/icons/manual/backgrounds/loader.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_MANUAL_CARD_BACK:String = "../maps/icons/manual/backgrounds/manual_card_back.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_MEDIUM_TANK:String = "../maps/icons/manual/backgrounds/medium_tank.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_MINIMAP:String = "../maps/icons/manual/backgrounds/minimap.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_MODULES:String = "../maps/icons/manual/backgrounds/modules.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_MOVEMENT_RANGE:String = "../maps/icons/manual/backgrounds/movement_range.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_OBSERVATION:String = "../maps/icons/manual/backgrounds/observation.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ORDERS:String = "../maps/icons/manual/backgrounds/orders.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_PENETRATION:String = "../maps/icons/manual/backgrounds/penetration.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_PERKS:String = "../maps/icons/manual/backgrounds/perks.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_PLATOON:String = "../maps/icons/manual/backgrounds/platoon.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_PREMIUM:String = "../maps/icons/manual/backgrounds/premium.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_PREMIUM2:String = "../maps/icons/manual/backgrounds/premium2.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_PREMIUM_PLUS_2:String = "../maps/icons/manual/backgrounds/premium_plus_2.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_PREM_TANK:String = "../maps/icons/manual/backgrounds/prem_tank.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RADIO:String = "../maps/icons/manual/backgrounds/radio.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RADIO_OPERATOR:String = "../maps/icons/manual/backgrounds/radio_operator.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RAM:String = "../maps/icons/manual/backgrounds/ram.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RANDOM:String = "../maps/icons/manual/backgrounds/random.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RANGE:String = "../maps/icons/manual/backgrounds/range.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RANKED:String = "../maps/icons/manual/backgrounds/ranked.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RELIEF:String = "../maps/icons/manual/backgrounds/relief.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RETRAIN:String = "../maps/icons/manual/backgrounds/retrain.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_RICOCHET:String = "../maps/icons/manual/backgrounds/ricochet.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_ROLES:String = "../maps/icons/manual/backgrounds/roles.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_SHELLS:String = "../maps/icons/manual/backgrounds/shells.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_SHELTER:String = "../maps/icons/manual/backgrounds/shelter.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_SHOOTING:String = "../maps/icons/manual/backgrounds/shooting.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_SOCIAL:String = "../maps/icons/manual/backgrounds/social.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_SPG:String = "../maps/icons/manual/backgrounds/spg.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_STANDART:String = "../maps/icons/manual/backgrounds/standart.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_STRONGHOLD:String = "../maps/icons/manual/backgrounds/stronghold.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_SUSPENSION:String = "../maps/icons/manual/backgrounds/suspension.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_TANT_DESTROYER:String = "../maps/icons/manual/backgrounds/tant_destroyer.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_TEAM:String = "../maps/icons/manual/backgrounds/team.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_TECH_TREE:String = "../maps/icons/manual/backgrounds/tech_tree.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_TRAINING:String = "../maps/icons/manual/backgrounds/training.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_TURRET:String = "../maps/icons/manual/backgrounds/turret.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_VISIBILITY:String = "../maps/icons/manual/backgrounds/visibility.png";

        public static const MAPS_ICONS_MANUAL_BACKGROUNDS_WELCOME:String = "../maps/icons/manual/backgrounds/welcome.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_BACKGROUND:String = "../maps/icons/manual/mainPage/background.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_MAINVIEWTILE1:String = "../maps/icons/manual/mainPage/mainViewTile1.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_MAINVIEWTILE2:String = "../maps/icons/manual/mainPage/mainViewTile2.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_MAINVIEWTILE3:String = "../maps/icons/manual/mainPage/mainViewTile3.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_MAINVIEWTILE4:String = "../maps/icons/manual/mainPage/mainViewTile4.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_MAINVIEWTILE5:String = "../maps/icons/manual/mainPage/mainViewTile5.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_MAINVIEWTILE6:String = "../maps/icons/manual/mainPage/mainViewTile6.png";

        public static const MAPS_ICONS_MANUAL_MAINPAGE_MAINVIEWTILE7:String = "../maps/icons/manual/mainPage/mainViewTile7.png";

        public static const MAPS_ICONS_MESSENGER_CONTACTCONFIRMNEEDED:String = "../maps/icons/messenger/contactConfirmNeeded.png";

        public static const MAPS_ICONS_MESSENGER_CONTACTIGNORED:String = "../maps/icons/messenger/contactIgnored.png";

        public static const MAPS_ICONS_MESSENGER_CONTACTMSGSOFF:String = "../maps/icons/messenger/contactMsgsOff.png";

        public static const MAPS_ICONS_MESSENGER_CONTACTNOTE:String = "../maps/icons/messenger/contactNote.png";

        public static const MAPS_ICONS_MESSENGER_CONTACTVOICEOFF:String = "../maps/icons/messenger/contactVoiceOff.png";

        public static const MAPS_ICONS_MESSENGER_ICONADDCONTACT:String = "../maps/icons/messenger/iconAddContact.png";

        public static const MAPS_ICONS_MESSENGER_ICONADDGROUP:String = "../maps/icons/messenger/iconAddGroup.png";

        public static const MAPS_ICONS_MESSENGER_ICONCHANNELS:String = "../maps/icons/messenger/iconChannels.png";

        public static const MAPS_ICONS_MESSENGER_ICONCOMPARISON:String = "../maps/icons/messenger/iconComparison.png";

        public static const MAPS_ICONS_MESSENGER_ICONCONTACTS:String = "../maps/icons/messenger/iconContacts.png";

        public static const MAPS_ICONS_MESSENGER_ICONREFERRAL:String = "../maps/icons/messenger/iconReferral.png";

        public static const MAPS_ICONS_MESSENGER_ICONSESSIONSTATS:String = "../maps/icons/messenger/iconSessionStats.png";

        public static const MAPS_ICONS_MESSENGER_ICONSETTINGS:String = "../maps/icons/messenger/iconSettings.png";

        public static const MAPS_ICONS_MESSENGER_SERVICE_CHANNEL_ICON:String = "../maps/icons/messenger/service_channel_icon.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_1:String = "../maps/icons/messenger/squad_gold_1.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_10:String = "../maps/icons/messenger/squad_gold_10.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_11:String = "../maps/icons/messenger/squad_gold_11.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_12:String = "../maps/icons/messenger/squad_gold_12.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_13:String = "../maps/icons/messenger/squad_gold_13.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_14:String = "../maps/icons/messenger/squad_gold_14.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_2:String = "../maps/icons/messenger/squad_gold_2.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_3:String = "../maps/icons/messenger/squad_gold_3.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_4:String = "../maps/icons/messenger/squad_gold_4.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_5:String = "../maps/icons/messenger/squad_gold_5.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_6:String = "../maps/icons/messenger/squad_gold_6.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_7:String = "../maps/icons/messenger/squad_gold_7.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_8:String = "../maps/icons/messenger/squad_gold_8.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_GOLD_9:String = "../maps/icons/messenger/squad_gold_9.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_ICON:String = "../maps/icons/messenger/squad_icon.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_1:String = "../maps/icons/messenger/squad_silver_1.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_10:String = "../maps/icons/messenger/squad_silver_10.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_11:String = "../maps/icons/messenger/squad_silver_11.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_12:String = "../maps/icons/messenger/squad_silver_12.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_13:String = "../maps/icons/messenger/squad_silver_13.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_14:String = "../maps/icons/messenger/squad_silver_14.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_2:String = "../maps/icons/messenger/squad_silver_2.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_3:String = "../maps/icons/messenger/squad_silver_3.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_4:String = "../maps/icons/messenger/squad_silver_4.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_5:String = "../maps/icons/messenger/squad_silver_5.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_6:String = "../maps/icons/messenger/squad_silver_6.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_7:String = "../maps/icons/messenger/squad_silver_7.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_8:String = "../maps/icons/messenger/squad_silver_8.png";

        public static const MAPS_ICONS_MESSENGER_SQUAD_SILVER_9:String = "../maps/icons/messenger/squad_silver_9.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_ALLDOWN:String = "../maps/icons/messenger/icons/allDown.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_ALLUP:String = "../maps/icons/messenger/icons/allUp.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_DOUBLE_LEFT_ARROW_ICON:String = "../maps/icons/messenger/icons/double-left-arrow-icon.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_DOUBLE_RIGHT_ARROW_ICON:String = "../maps/icons/messenger/icons/double-right-arrow-icon.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_DOWN:String = "../maps/icons/messenger/icons/down.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_EDIT:String = "../maps/icons/messenger/icons/edit.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_ENTER:String = "../maps/icons/messenger/icons/enter.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_FILTER:String = "../maps/icons/messenger/icons/filter.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_REFRESH:String = "../maps/icons/messenger/icons/refresh.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_SINGLE_LEFT_ARROW_ICON:String = "../maps/icons/messenger/icons/single-left-arrow-icon.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_SINGLE_RIGHT_ARROW_ICON:String = "../maps/icons/messenger/icons/single-right-arrow-icon.png";

        public static const MAPS_ICONS_MESSENGER_ICONS_UP:String = "../maps/icons/messenger/icons/up.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_MOB:String = "../maps/icons/messenger/status/24x24/chat_icon_mob.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_UNKNOWN:String = "../maps/icons/messenger/status/24x24/chat_icon_unknown.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_USER_IS_BUSY:String = "../maps/icons/messenger/status/24x24/chat_icon_user_is_busy.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_USER_IS_BUSY_VIOLET:String = "../maps/icons/messenger/status/24x24/chat_icon_user_is_busy_violet.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_USER_IS_ONLINE:String = "../maps/icons/messenger/status/24x24/chat_icon_user_is_online.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_WEB:String = "../maps/icons/messenger/status/24x24/chat_icon_web.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_WOTB:String = "../maps/icons/messenger/status/24x24/chat_icon_wotb.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_WOTG:String = "../maps/icons/messenger/status/24x24/chat_icon_wotg.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_WOWP:String = "../maps/icons/messenger/status/24x24/chat_icon_wowp.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_24X24_CHAT_ICON_WOWS:String = "../maps/icons/messenger/status/24x24/chat_icon_wows.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_MOB:String = "../maps/icons/messenger/status/48x48/chat_icon_mob.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_UNKNOWN:String = "../maps/icons/messenger/status/48x48/chat_icon_unknown.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_USER_IS_BUSY:String = "../maps/icons/messenger/status/48x48/chat_icon_user_is_busy.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_USER_IS_BUSY_VIOLET:String = "../maps/icons/messenger/status/48x48/chat_icon_user_is_busy_violet.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_USER_IS_ONLINE:String = "../maps/icons/messenger/status/48x48/chat_icon_user_is_online.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_WEB:String = "../maps/icons/messenger/status/48x48/chat_icon_web.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_WOTB:String = "../maps/icons/messenger/status/48x48/chat_icon_wotb.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_WOTG:String = "../maps/icons/messenger/status/48x48/chat_icon_wotg.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_WOWP:String = "../maps/icons/messenger/status/48x48/chat_icon_wowp.png";

        public static const MAPS_ICONS_MESSENGER_STATUS_48X48_CHAT_ICON_WOWS:String = "../maps/icons/messenger/status/48x48/chat_icon_wows.png";

        public static const MAPS_ICONS_MODULES_AUTOLOADERGUN:String = "../maps/icons/modules/autoLoaderGun.png";

        public static const MAPS_ICONS_MODULES_BATTLEABILITYLISTOVERLAY:String = "../maps/icons/modules/battleAbilitylistOverlay.png";

        public static const MAPS_ICONS_MODULES_CHASSIS:String = "../maps/icons/modules/chassis.png";

        public static const MAPS_ICONS_MODULES_DESTROYDEVICE:String = "../maps/icons/modules/destroyDevice.png";

        public static const MAPS_ICONS_MODULES_DISMANTLEDEVICE:String = "../maps/icons/modules/dismantleDevice.png";

        public static const MAPS_ICONS_MODULES_DUALGUN:String = "../maps/icons/modules/dualGun.png";

        public static const MAPS_ICONS_MODULES_ENGINE:String = "../maps/icons/modules/engine.png";

        public static const MAPS_ICONS_MODULES_GUN:String = "../maps/icons/modules/gun.png";

        public static const MAPS_ICONS_MODULES_HYDRAULICCHASSISICON:String = "../maps/icons/modules/hydraulicChassisIcon.png";

        public static const MAPS_ICONS_MODULES_HYDRAULICWHEELEDCHASSISICON:String = "../maps/icons/modules/hydraulicWheeledChassisIcon.png";

        public static const MAPS_ICONS_MODULES_LISTOVERLAY:String = "../maps/icons/modules/listOverlay.png";

        public static const MAPS_ICONS_MODULES_LISTOVERLAYSMALL:String = "../maps/icons/modules/listOverlaySmall.png";

        public static const MAPS_ICONS_MODULES_MAGAZINEGUNICON:String = "../maps/icons/modules/magazineGunIcon.png";

        public static const MAPS_ICONS_MODULES_RADIO:String = "../maps/icons/modules/radio.png";

        public static const MAPS_ICONS_MODULES_TOWER:String = "../maps/icons/modules/tower.png";

        public static const MAPS_ICONS_MODULES_WHEELEDCHASSIS:String = "../maps/icons/modules/wheeledChassis.png";

        public static const MAPS_ICONS_NATIONCHANGE_EMPTY_SLOT_ICON:String = "../maps/icons/nationChange/empty_slot_icon.png";

        public static const MAPS_ICONS_NATIONCHANGE_MAIN_ARROW:String = "../maps/icons/nationChange/main_arrow.png";

        public static const MAPS_ICONS_NATIONCHANGE_ORANGE_FLASH:String = "../maps/icons/nationChange/orange_flash.png";

        public static const MAPS_ICONS_NATIONCHANGE_R117_T34_85_RUDY:String = "../maps/icons/nationChange/R117_T34_85_Rudy.png";

        public static const MAPS_ICONS_NATIONCHANGE_R117_T34_85_RUDY_BIG:String = "../maps/icons/nationChange/R117_T34_85_Rudy_big .png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_CHINA:String = "../maps/icons/nationChange/flags/china.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_CZECH:String = "../maps/icons/nationChange/flags/czech.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_FRANCE:String = "../maps/icons/nationChange/flags/france.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_GB:String = "../maps/icons/nationChange/flags/gb.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_GERMANY:String = "../maps/icons/nationChange/flags/germany.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_ITALY:String = "../maps/icons/nationChange/flags/italy.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_JAPAN:String = "../maps/icons/nationChange/flags/japan.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_POLAND:String = "../maps/icons/nationChange/flags/poland.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_SWEDEN:String = "../maps/icons/nationChange/flags/sweden.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_USA:String = "../maps/icons/nationChange/flags/usa.png";

        public static const MAPS_ICONS_NATIONCHANGE_FLAGS_USSR:String = "../maps/icons/nationChange/flags/ussr.png";

        public static const MAPS_ICONS_NUMERICAL_STEPPER_STEPPER_LARGE_BUTTONS:String = "../maps/icons/numerical_stepper/stepper_large_buttons.png";

        public static const MAPS_ICONS_NUMERICAL_STEPPER_STEPPER_MEDIUM_BUTTONS:String = "../maps/icons/numerical_stepper/stepper_medium_buttons.png";

        public static const MAPS_ICONS_NUMERICAL_STEPPER_STEPPER_SMALL_BUTTONS:String = "../maps/icons/numerical_stepper/stepper_small_buttons.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_AWARDS_VIEW_BG:String = "../maps/icons/personalMissions/awards_view_bg.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_AWARDS_VIEW_BG_PM2:String = "../maps/icons/personalMissions/awards_view_bg_pm2.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_BALANCE:String = "../maps/icons/personalMissions/balance.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_BTN_ICON_PAUSE:String = "../maps/icons/personalMissions/btn_icon_pause.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_BTN_ICON_PLAY:String = "../maps/icons/personalMissions/btn_icon_play.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_CHECK_DOUBLE:String = "../maps/icons/personalMissions/check_double.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_GEAR_BIG:String = "../maps/icons/personalMissions/gear_big.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_INFOSCREENBG:String = "../maps/icons/personalMissions/InfoScreenBG.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_LOCK:String = "../maps/icons/personalMissions/lock.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_LOCK_PROGRESS:String = "../maps/icons/personalMissions/lock_progress.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_MAIN_100:String = "../maps/icons/personalMissions/main_100.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_MAP_1:String = "../maps/icons/personalMissions/map_1.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_MAP_2:String = "../maps/icons/personalMissions/map_2.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_PLAYICON:String = "../maps/icons/personalMissions/playIcon.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_PM2_ORDER_BLANK:String = "../maps/icons/personalMissions/pm2_order_blank.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_QUESTION_ICON:String = "../maps/icons/personalMissions/question_icon.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_REGULAR_ORDER_BLANK:String = "../maps/icons/personalMissions/regular_order_blank.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ZOOM:String = "../maps/icons/personalMissions/zoom.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_17X19_ALLIANCE_FRANCE:String = "../maps/icons/personalMissions/alliances/17x19/Alliance-France.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_17X19_ALLIANCE_GERMANY:String = "../maps/icons/personalMissions/alliances/17x19/Alliance-Germany.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_17X19_ALLIANCE_USA:String = "../maps/icons/personalMissions/alliances/17x19/Alliance-USA.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_17X19_ALLIANCE_USSR:String = "../maps/icons/personalMissions/alliances/17x19/Alliance-USSR.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_FRANCE:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-France.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_FRANCE_INACTIVE:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-France_inactive.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_GERMANY:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-Germany.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_GERMANY_INACTIVE:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-Germany_inactive.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_USA:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-USA.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_USA_INACTIVE:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-USA_inactive.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_USSR:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-USSR.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_32X32_ALLIANCE_USSR_INACTIVE:String = "../maps/icons/personalMissions/alliances/32x32/Alliance-USSR_inactive.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_54X54_ALLIANCE1:String = "../maps/icons/personalMissions/alliances/54x54/alliance1.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_54X54_ALLIANCE2:String = "../maps/icons/personalMissions/alliances/54x54/alliance2.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_54X54_ALLIANCE3:String = "../maps/icons/personalMissions/alliances/54x54/alliance3.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_54X54_ALLIANCE4:String = "../maps/icons/personalMissions/alliances/54x54/alliance4.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_CHAINBTN_ALLIANCE0:String = "../maps/icons/personalMissions/alliances/chainBtn/alliance0.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_CHAINBTN_ALLIANCE1:String = "../maps/icons/personalMissions/alliances/chainBtn/alliance1.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_CHAINBTN_ALLIANCE2:String = "../maps/icons/personalMissions/alliances/chainBtn/alliance2.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_CHAINBTN_ALLIANCE3:String = "../maps/icons/personalMissions/alliances/chainBtn/alliance3.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_ALLIANCE_FRANCE:String = "../maps/icons/personalMissions/alliances/gold_24x30/Alliance-France.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_ALLIANCE_GERMANY:String = "../maps/icons/personalMissions/alliances/gold_24x30/Alliance-Germany.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_ALLIANCE_USA:String = "../maps/icons/personalMissions/alliances/gold_24x30/Alliance-USA.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_ALLIANCE_USSR:String = "../maps/icons/personalMissions/alliances/gold_24x30/Alliance-USSR.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_AT_SPG:String = "../maps/icons/personalMissions/alliances/gold_24x30/AT-SPG.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_HEAVYTANK:String = "../maps/icons/personalMissions/alliances/gold_24x30/heavyTank.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_LIGHTTANK:String = "../maps/icons/personalMissions/alliances/gold_24x30/lightTank.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_MEDIUMTANK:String = "../maps/icons/personalMissions/alliances/gold_24x30/mediumTank.png";

        public static const MAPS_ICONS_PERSONALMISSIONS_ALLIANCES_GOLD_24X30_SPG:String = "../maps/icons/personalMissions/alliances/gold_24x30/SPG.png";

        public static const MAPS_ICONS_POPOVERS_CLOSE_BUTTON_SLIM_PRESS:String = "../maps/icons/popovers/close_button_slim_press.png";

        public static const MAPS_ICONS_POPOVERS_CLOSE_BUTTON_SLIM_UP:String = "../maps/icons/popovers/close_button_slim_up.png";

        public static const MAPS_ICONS_POPOVERS_POPOVER_ARROW_LEFT:String = "../maps/icons/popovers/popover_arrow_left.png";

        public static const MAPS_ICONS_POPOVERS_POPOVER_ARROW_TOP:String = "../maps/icons/popovers/popover_arrow_top.png";

        public static const MAPS_ICONS_POPOVERS_POPOVER_BG:String = "../maps/icons/popovers/popover_bg.png";

        public static const MAPS_ICONS_POPOVERS_POPOVER_SHADOW:String = "../maps/icons/popovers/popover_shadow.png";

        public static const MAPS_ICONS_PREMACC_BACKGROUNDS_DASHBOARD:String = "../maps/icons/premacc/backgrounds/dashboard.png";

        public static const MAPS_ICONS_PREMACC_BACKGROUNDS_PIGGYBANK:String = "../maps/icons/premacc/backgrounds/piggybank.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_BONUS_X1:String = "../maps/icons/premacc/battleResult/bonus_x1.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_BONUS_X2:String = "../maps/icons/premacc/battleResult/bonus_x2.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_BONUS_X3:String = "../maps/icons/premacc/battleResult/bonus_x3.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_BONUS_X4:String = "../maps/icons/premacc/battleResult/bonus_x4.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_BONUS_X5:String = "../maps/icons/premacc/battleResult/bonus_x5.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_CREDITS:String = "../maps/icons/premacc/battleResult/credits.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_PREMIUM:String = "../maps/icons/premacc/battleResult/premium.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_QUESTS:String = "../maps/icons/premacc/battleResult/quests.png";

        public static const MAPS_ICONS_PREMACC_BATTLERESULT_SQUAD:String = "../maps/icons/premacc/battleResult/squad.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_440_X1:String = "../maps/icons/premacc/content/bonus_440_x1.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_440_X2:String = "../maps/icons/premacc/content/bonus_440_x2.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_440_X3:String = "../maps/icons/premacc/content/bonus_440_x3.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_440_X4:String = "../maps/icons/premacc/content/bonus_440_x4.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_440_X5:String = "../maps/icons/premacc/content/bonus_440_x5.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_600_X1:String = "../maps/icons/premacc/content/bonus_600_x1.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_600_X2:String = "../maps/icons/premacc/content/bonus_600_x2.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_600_X3:String = "../maps/icons/premacc/content/bonus_600_x3.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_600_X4:String = "../maps/icons/premacc/content/bonus_600_x4.png";

        public static const MAPS_ICONS_PREMACC_CONTENT_BONUS_600_X5:String = "../maps/icons/premacc/content/bonus_600_x5.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARD_SEPARATOR:String = "../maps/icons/premacc/dashboard/card_separator.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARD_STROKE:String = "../maps/icons/premacc/dashboard/card_stroke.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_EXP:String = "../maps/icons/premacc/dashboard/exp.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HOVER:String = "../maps/icons/premacc/dashboard/hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_STROKE:String = "../maps/icons/premacc/dashboard/stroke.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_STROKE_CORNER:String = "../maps/icons/premacc/dashboard/stroke_corner.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDBLACKMAPLIST_BAD_MAP_438:String = "../maps/icons/premacc/dashboard/cardBlackMapList/Bad_map_438.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDBLACKMAPLIST_BAD_MAP_630:String = "../maps/icons/premacc/dashboard/cardBlackMapList/Bad_map_630.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDBLACKMAPLIST_LOCK:String = "../maps/icons/premacc/dashboard/cardBlackMapList/lock.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDMISSIONS_LOCK:String = "../maps/icons/premacc/dashboard/cardMissions/lock.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDMISSIONS_MISSION_COMPLETE:String = "../maps/icons/premacc/dashboard/cardMissions/mission_complete.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDMISSIONS_MISSION_DISABLED:String = "../maps/icons/premacc/dashboard/cardMissions/mission_disabled.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDMISSIONS_MISSION_INPROGRESS:String = "../maps/icons/premacc/dashboard/cardMissions/mission_inprogress.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDMISSIONS_PQ_438:String = "../maps/icons/premacc/dashboard/cardMissions/pq_438.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDMISSIONS_PQ_630:String = "../maps/icons/premacc/dashboard/cardMissions/pq_630.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPIGGY_CHECK:String = "../maps/icons/premacc/dashboard/cardPiggy/check.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPIGGY_CORNER:String = "../maps/icons/premacc/dashboard/cardPiggy/corner.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPIGGY_LOCK:String = "../maps/icons/premacc/dashboard/cardPiggy/lock.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPIGGY_PIGGYBANK_340:String = "../maps/icons/premacc/dashboard/cardPiggy/piggybank_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPIGGY_PIGGYBANK_360:String = "../maps/icons/premacc/dashboard/cardPiggy/piggybank_360.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPIGGY_PIGGYBANK_DISABLED_340:String = "../maps/icons/premacc/dashboard/cardPiggy/piggybank_disabled_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPIGGY_PIGGYBANK_DISABLED_360:String = "../maps/icons/premacc/dashboard/cardPiggy/piggybank_disabled_360.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_BONUS_CREDITS:String = "../maps/icons/premacc/dashboard/cardPrem/bonus_credits.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_BONUS_EXP:String = "../maps/icons/premacc/dashboard/cardPrem/bonus_exp.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_BONUS_MAP:String = "../maps/icons/premacc/dashboard/cardPrem/bonus_map.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_BONUS_PIGGY:String = "../maps/icons/premacc/dashboard/cardPrem/bonus_piggy.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_BONUS_TASKS:String = "../maps/icons/premacc/dashboard/cardPrem/bonus_tasks.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_ICON_CREDITS_32:String = "../maps/icons/premacc/dashboard/cardPrem/icon_credits_32.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_ICON_CREDITS_48:String = "../maps/icons/premacc/dashboard/cardPrem/icon_credits_48.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_ICON_EXP_32:String = "../maps/icons/premacc/dashboard/cardPrem/icon_exp_32.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_ICON_EXP_48:String = "../maps/icons/premacc/dashboard/cardPrem/icon_exp_48.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_ICON_SQUAD_32:String = "../maps/icons/premacc/dashboard/cardPrem/icon_squad_32.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_ICON_SQUAD_48:String = "../maps/icons/premacc/dashboard/cardPrem/icon_squad_48.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_LOGO_BACKLIGHT:String = "../maps/icons/premacc/dashboard/cardPrem/logo_backlight.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_LOGO_BACKLIGHT_SMALL:String = "../maps/icons/premacc/dashboard/cardPrem/logo_backlight_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_LOGO_PREMIUM:String = "../maps/icons/premacc/dashboard/cardPrem/logo_premium.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_LOGO_PREMIUM_SMALL:String = "../maps/icons/premacc/dashboard/cardPrem/logo_premium_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_PA_GLOW:String = "../maps/icons/premacc/dashboard/cardPrem/PA_Glow.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_PA_GLOW_366:String = "../maps/icons/premacc/dashboard/cardPrem/PA_Glow_366.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_PA_NO_GLOW:String = "../maps/icons/premacc/dashboard/cardPrem/PA_no_glow.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDPREM_PA_NO_GLOW_366:String = "../maps/icons/premacc/dashboard/cardPrem/PA_no_glow_366.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_LOCK:String = "../maps/icons/premacc/dashboard/cardXp/lock.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X1_340:String = "../maps/icons/premacc/dashboard/cardXp/x1_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X1_360:String = "../maps/icons/premacc/dashboard/cardXp/x1_360.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X1_DISABLED_340:String = "../maps/icons/premacc/dashboard/cardXp/x1_disabled_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X2_340:String = "../maps/icons/premacc/dashboard/cardXp/x2_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X2_360:String = "../maps/icons/premacc/dashboard/cardXp/x2_360.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X3_340:String = "../maps/icons/premacc/dashboard/cardXp/x3_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X3_360:String = "../maps/icons/premacc/dashboard/cardXp/x3_360.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X4_340:String = "../maps/icons/premacc/dashboard/cardXp/x4_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X4_360:String = "../maps/icons/premacc/dashboard/cardXp/x4_360.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X5_340:String = "../maps/icons/premacc/dashboard/cardXp/x5_340.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_CARDXP_X5_360:String = "../maps/icons/premacc/dashboard/cardXp/x5_360.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_BADGE_56:String = "../maps/icons/premacc/dashboard/header/badge_56.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_BADGE_57:String = "../maps/icons/premacc/dashboard/header/badge_57.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_BADGE_BACK:String = "../maps/icons/premacc/dashboard/header/badge_back.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_BADGE_BACK_SMALL:String = "../maps/icons/premacc/dashboard/header/badge_back_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_BADGESLOT_BG:String = "../maps/icons/premacc/dashboard/header/badgeSlot/bg.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_BADGESLOT_HOVER:String = "../maps/icons/premacc/dashboard/header/badgeSlot/hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_ACTIVE:String = "../maps/icons/premacc/dashboard/header/clanSlot/active.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_ACTIVE_HOVER:String = "../maps/icons/premacc/dashboard/header/clanSlot/active_hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_ACTIVE_SMALL:String = "../maps/icons/premacc/dashboard/header/clanSlot/active_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_ACTIVE_SMALL_HOVER:String = "../maps/icons/premacc/dashboard/header/clanSlot/active_small_hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_GRADIENT:String = "../maps/icons/premacc/dashboard/header/clanSlot/gradient.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_HOVER:String = "../maps/icons/premacc/dashboard/header/clanSlot/hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_HOVER_SMALL:String = "../maps/icons/premacc/dashboard/header/clanSlot/hover_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_NORMAL:String = "../maps/icons/premacc/dashboard/header/clanSlot/normal.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_CLANSLOT_NORMAL_SMALL:String = "../maps/icons/premacc/dashboard/header/clanSlot/normal_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_ACTIVE:String = "../maps/icons/premacc/dashboard/header/personalSlot/active.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_ACTIVE_HOVER:String = "../maps/icons/premacc/dashboard/header/personalSlot/active_hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_ACTIVE_SMALL:String = "../maps/icons/premacc/dashboard/header/personalSlot/active_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_ACTIVE_SMALL_HOVER:String = "../maps/icons/premacc/dashboard/header/personalSlot/active_small_hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_GRADIENT:String = "../maps/icons/premacc/dashboard/header/personalSlot/gradient.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_HOVER:String = "../maps/icons/premacc/dashboard/header/personalSlot/hover.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_HOVER_SMALL:String = "../maps/icons/premacc/dashboard/header/personalSlot/hover_small.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_NORMAL:String = "../maps/icons/premacc/dashboard/header/personalSlot/normal.png";

        public static const MAPS_ICONS_PREMACC_DASHBOARD_HEADER_PERSONALSLOT_NORMAL_SMALL:String = "../maps/icons/premacc/dashboard/header/personalSlot/normal_small.png";

        public static const MAPS_ICONS_PREMACC_ICONS_PREMIUM_218X206:String = "../maps/icons/premacc/icons/premium_218x206.png";

        public static const MAPS_ICONS_PREMACC_ICONS_PREMIUM_256X242:String = "../maps/icons/premacc/icons/premium_256x242.png";

        public static const MAPS_ICONS_PREMACC_ICONS_PREMIUM_40X40:String = "../maps/icons/premacc/icons/premium_40x40.png";

        public static const MAPS_ICONS_PREMACC_LOBBYHEADER_PREMIUM:String = "../maps/icons/premacc/lobbyHeader/premium.png";

        public static const MAPS_ICONS_PREMACC_LOBBYHEADER_PREMIUMPLUS:String = "../maps/icons/premacc/lobbyHeader/premiumPlus.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_BG_TIMER_440:String = "../maps/icons/premacc/piggybank/bg-timer-440.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_BG_TIMER_530:String = "../maps/icons/premacc/piggybank/bg-timer-530.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_ICON_INFO_HOVER:String = "../maps/icons/premacc/piggybank/icon-info-hover.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_ICON_INFO_NORMAL:String = "../maps/icons/premacc/piggybank/icon-info-normal.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_LIGHT:String = "../maps/icons/premacc/piggybank/light.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_BG_01:String = "../maps/icons/premacc/piggybank/progress-bg-01.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_BG_02:String = "../maps/icons/premacc/piggybank/progress-bg-02.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_BG_03:String = "../maps/icons/premacc/piggybank/progress-bg-03.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_FILLED_01:String = "../maps/icons/premacc/piggybank/progress-filled-01.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_FILLED_02:String = "../maps/icons/premacc/piggybank/progress-filled-02.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_FILLED_03:String = "../maps/icons/premacc/piggybank/progress-filled-03.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_FREEZED_01:String = "../maps/icons/premacc/piggybank/progress-freezed-01.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_FREEZED_02:String = "../maps/icons/premacc/piggybank/progress-freezed-02.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_FREEZED_03:String = "../maps/icons/premacc/piggybank/progress-freezed-03.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_NORMAL_01:String = "../maps/icons/premacc/piggybank/progress-normal-01.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_NORMAL_02:String = "../maps/icons/premacc/piggybank/progress-normal-02.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_PROGRESS_NORMAL_03:String = "../maps/icons/premacc/piggybank/progress-normal-03.png";

        public static const MAPS_ICONS_PREMACC_PIGGYBANK_SEPARATOR_PR_BAR:String = "../maps/icons/premacc/piggybank/separator_pr_bar.png";

        public static const MAPS_ICONS_PREMACC_QUESTS_BACKGROUND:String = "../maps/icons/premacc/quests/background.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_BIG_LOCK_PROB_MAX:String = "../maps/icons/progressiveReward/big_lock_prob_max.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_BIG_LOCK_PROB_MED:String = "../maps/icons/progressiveReward/big_lock_prob_med.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_BIG_LOCK_PROB_MIN:String = "../maps/icons/progressiveReward/big_lock_prob_min.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_DELIMITER:String = "../maps/icons/progressiveReward/delimiter.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_LIGHT:String = "../maps/icons/progressiveReward/light.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_LOCK:String = "../maps/icons/progressiveReward/lock.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_REWARD_REPEAT:String = "../maps/icons/progressiveReward/reward_repeat.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_RULE_BG:String = "../maps/icons/progressiveReward/rule_bg.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_SEPARATOR_REWARDS:String = "../maps/icons/progressiveReward/separator_rewards.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_BIG_NOT_RECEIVED:String = "../maps/icons/progressiveReward/gifts/big/not_received.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_BIG_OPENED:String = "../maps/icons/progressiveReward/gifts/big/opened.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_BIG_PROB_MAX:String = "../maps/icons/progressiveReward/gifts/big/prob_max.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_BIG_PROB_MED:String = "../maps/icons/progressiveReward/gifts/big/prob_med.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_BIG_PROB_MIN:String = "../maps/icons/progressiveReward/gifts/big/prob_min.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_BIG_RECEIVED:String = "../maps/icons/progressiveReward/gifts/big/received.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_NORMAL_NOT_RECEIVED:String = "../maps/icons/progressiveReward/gifts/normal/not_received.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_NORMAL_OPENED:String = "../maps/icons/progressiveReward/gifts/normal/opened.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_NORMAL_PROB_MAX:String = "../maps/icons/progressiveReward/gifts/normal/prob_max.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_NORMAL_PROB_MED:String = "../maps/icons/progressiveReward/gifts/normal/prob_med.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_NORMAL_PROB_MIN:String = "../maps/icons/progressiveReward/gifts/normal/prob_min.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_NORMAL_RECEIVED:String = "../maps/icons/progressiveReward/gifts/normal/received.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_SMALL_NOT_RECEIVED:String = "../maps/icons/progressiveReward/gifts/small/not_received.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_SMALL_OPENED:String = "../maps/icons/progressiveReward/gifts/small/opened.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_SMALL_PROB_MAX:String = "../maps/icons/progressiveReward/gifts/small/prob_max.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_SMALL_PROB_MED:String = "../maps/icons/progressiveReward/gifts/small/prob_med.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_SMALL_PROB_MIN:String = "../maps/icons/progressiveReward/gifts/small/prob_min.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_GIFTS_SMALL_RECEIVED:String = "../maps/icons/progressiveReward/gifts/small/received.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_REWARDS_BIG_BIG:String = "../maps/icons/progressiveReward/rewards/big/big.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_REWARDS_BIG_BIG_HIDDEN:String = "../maps/icons/progressiveReward/rewards/big/big_hidden.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_REWARDS_BIG_SMALL:String = "../maps/icons/progressiveReward/rewards/big/small.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_REWARDS_BIG_SMALL_HIDDEN:String = "../maps/icons/progressiveReward/rewards/big/small_hidden.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_SCREENBG_HIDDEN:String = "../maps/icons/progressiveReward/screenBg/hidden.png";

        public static const MAPS_ICONS_PROGRESSIVEREWARD_SCREENBG_RECEIVED:String = "../maps/icons/progressiveReward/screenBg/received.png";

        public static const MAPS_ICONS_QUESTS_AT_SPGAWARDBACK:String = "../maps/icons/quests/AT-SPGAwardBack.png";

        public static const MAPS_ICONS_QUESTS_AWARDRIBBON:String = "../maps/icons/quests/awardRibbon.png";

        public static const MAPS_ICONS_QUESTS_AWARDS_BACKGROUND:String = "../maps/icons/quests/awards_background.png";

        public static const MAPS_ICONS_QUESTS_CLASSICFALLOUTAWARDBACK:String = "../maps/icons/quests/classicFalloutAwardBack.png";

        public static const MAPS_ICONS_QUESTS_HTAWARDBACK:String = "../maps/icons/quests/HTAwardBack.png";

        public static const MAPS_ICONS_QUESTS_ICON_BATTLE_MISSIONS_PRIZE_TOKEN:String = "../maps/icons/quests/icon_battle_missions_prize_token.png";

        public static const MAPS_ICONS_QUESTS_INBATTLEHINT:String = "../maps/icons/quests/inBattleHint.png";

        public static const MAPS_ICONS_QUESTS_KURSKTOOLTIPHEADER:String = "../maps/icons/quests/kurskTooltipHeader.png";

        public static const MAPS_ICONS_QUESTS_LTAWARDBACK:String = "../maps/icons/quests/LTAwardBack.png";

        public static const MAPS_ICONS_QUESTS_MARATHONTOOLTIPHEADER:String = "../maps/icons/quests/marathonTooltipHeader.png";

        public static const MAPS_ICONS_QUESTS_MTAWARDBACK:String = "../maps/icons/quests/MTAwardBack.png";

        public static const MAPS_ICONS_QUESTS_MULTITEAMFALLOUTAWARDBACK:String = "../maps/icons/quests/multiteamFalloutAwardBack.png";

        public static const MAPS_ICONS_QUESTS_PROMOSCREEN_FALLOUT:String = "../maps/icons/quests/promoscreen_fallout.png";

        public static const MAPS_ICONS_QUESTS_QUESTTOOLTIPHEADER:String = "../maps/icons/quests/questTooltipHeader.png";

        public static const MAPS_ICONS_QUESTS_SEASONSVIEWBG:String = "../maps/icons/quests/seasonsViewBg.png";

        public static const MAPS_ICONS_QUESTS_SPGAWARDBACK:String = "../maps/icons/quests/SPGAwardBack.png";

        public static const MAPS_ICONS_QUESTS_TANKMANFEMALEGRAY:String = "../maps/icons/quests/tankmanFemaleGray.png";

        public static const MAPS_ICONS_QUESTS_TANKMANFEMALEORANGE:String = "../maps/icons/quests/tankmanFemaleOrange.png";

        public static const MAPS_ICONS_QUESTS_TOKEN128:String = "../maps/icons/quests/token128.png";

        public static const MAPS_ICONS_QUESTS_TOKEN16:String = "../maps/icons/quests/token16.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_assist_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_RADIO_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_assist_radio_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_STUN_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_assist_stun_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_STUN_MULTY_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_assist_stun_multy_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_STUN_TIME_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_assist_stun_time_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_TRACK_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_assist_track_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_AWARD_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_award_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BARREL_MARK_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_barrel_mark_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BASE_CAPTURE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_base_capture_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BASE_DEF_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_base_def_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BATTLES_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_battles_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_CREDITS_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_credits_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_DAMAGE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_damage_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_DAMAGE_BLOCK_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_damage_block_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_DISCOVER_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_discover_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_EXPERIENCE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_experience_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_FIRE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_fire_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_FOLDER_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_folder_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_GET_DAMAGE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_get_damage_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_GET_HIT_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_get_hit_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_HIT_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_hit_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_HURT_1SHOT_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_hurt_1shot_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_HURT_VEHICLES_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_hurt_vehicles_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_IMPROVE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_improve_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_KILL_1SHOT_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_kill_1shot_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_KILL_VEHICLES_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_kill_vehicles_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_MAIN_REPEAT_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_main_repeat_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_MASTER_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_master_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_METERS_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_meters_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_MODULE_CRIT_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_module_crit_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_PREPARATION_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_preparation_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_RAM_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_ram_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_SAVE_HP_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_save_hp_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_SEC_ALIVE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_sec_alive_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_SURVIVE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_survive_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_TIMES_GET_DAMAGE_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_times_get_damage_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_TOP_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_top_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_WIN_128X128:String = "../maps/icons/quests/battleCondition/128/icon_battle_condition_win_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_assist_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_RADIO_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_assist_radio_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_STUN_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_assist_stun_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_STUN_MULTY_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_assist_stun_multy_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_STUN_TIME_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_assist_stun_time_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_TRACK_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_assist_track_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_AWARD_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_award_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_BASE_CAPTURE_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_base_capture_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_BASE_DEF_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_base_def_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_DAMAGE_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_damage_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_DAMAGE_BLOCK_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_damage_block_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_DISCOVER_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_discover_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_FIRE_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_fire_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_FOLDER_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_folder_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_HIT_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_hit_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_HURT_VEHICLES_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_hurt_vehicles_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_IMPROVE_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_improve_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_KILL_VEHICLES_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_kill_vehicles_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_MAIN_REPEAT_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_main_repeat_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_MASTER_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_master_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_MODULE_CRIT_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_module_crit_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_PREPARATION_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_preparation_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_RAM_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_ram_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_SURVIVE_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_survive_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_TOP_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_top_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_WIN_128X128:String = "../maps/icons/quests/battleCondition/128_decor/icon_battle_condition_win_128x128.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_assist_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_RADIO_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_assist_radio_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_STUN_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_assist_stun_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_STUN_MULTY_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_assist_stun_multy_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_STUN_TIME_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_assist_stun_time_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_TRACK_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_assist_track_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_AWARD_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_award_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BARREL_MARK_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_barrel_mark_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BASE_CAPTURE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_base_capture_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BASE_DEF_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_base_def_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BATTLES_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_battles_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_CREDITS_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_credits_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_DAMAGE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_damage_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_DAMAGE_BLOCK_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_damage_block_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_DISCOVER_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_discover_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_EXPERIENCE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_experience_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_FIRE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_fire_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_FOLDER_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_folder_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_GET_DAMAGE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_get_damage_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_GET_HIT_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_get_hit_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_HIT_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_hit_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_HURT_1SHOT_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_hurt_1shot_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_HURT_VEHICLES_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_hurt_vehicles_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_IMPROVE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_improve_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_KILL_1SHOT_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_kill_1shot_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_KILL_VEHICLES_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_kill_vehicles_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_MAIN_REPEAT_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_main_repeat_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_MASTER_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_master_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_METERS_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_meters_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_MODULE_CRIT_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_module_crit_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_PREPARATION_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_preparation_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_RAM_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_ram_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_SAVE_HP_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_save_hp_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_SEC_ALIVE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_sec_alive_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_SURVIVE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_survive_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_TIMES_GET_DAMAGE_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_times_get_damage_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_TOP_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_top_90x90.png";

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_WIN_90X90:String = "../maps/icons/quests/battleCondition/90/icon_battle_condition_win_90x90.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_ADDITIONALBRIEFING:String = "../maps/icons/quests/bonuses/big/additionalBriefing.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_AIMINGSTABILIZER:String = "../maps/icons/quests/bonuses/big/aimingStabilizer.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_AIMINGSTABILIZERBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/aimingStabilizerBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_ANTIFRAGMENTATIONLINING:String = "../maps/icons/quests/bonuses/big/antifragmentationLining.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_AUTOEXTINGUISHERS:String = "../maps/icons/quests/bonuses/big/autoExtinguishers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BATTLEBOOSTERREPLACE_OVERLAY:String = "../maps/icons/quests/bonuses/big/battleBoosterReplace_overlay.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BATTLEBOOSTER_HIGHLIGHT:String = "../maps/icons/quests/bonuses/big/battleBooster_highlight.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BATTLEBOOSTER_OVERLAY:String = "../maps/icons/quests/bonuses/big/battleBooster_overlay.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BERTHS:String = "../maps/icons/quests/bonuses/big/berths.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BOB_POINTS:String = "../maps/icons/quests/bonuses/big/bob_points.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BOB_POINTS_NA_ASIA:String = "../maps/icons/quests/bonuses/big/bob_points_na_asia.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BONUS_BATTLE_TASK:String = "../maps/icons/quests/bonuses/big/bonus_battle_task.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BOOSTER_CREDITS:String = "../maps/icons/quests/bonuses/big/booster_credits.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BOOSTER_CREW_XP:String = "../maps/icons/quests/bonuses/big/booster_crew_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BOOSTER_FL_XP:String = "../maps/icons/quests/bonuses/big/booster_fl_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BOOSTER_FREE_XP:String = "../maps/icons/quests/bonuses/big/booster_free_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_BOOSTER_XP:String = "../maps/icons/quests/bonuses/big/booster_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CAMOUFLAGE:String = "../maps/icons/quests/bonuses/big/camouflage.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CAMOUFLAGEBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/camouflageBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CAMOUFLAGENET:String = "../maps/icons/quests/bonuses/big/camouflageNet.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CARBONDIOXIDE:String = "../maps/icons/quests/bonuses/big/carbonDioxide.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CHOCOLATE:String = "../maps/icons/quests/bonuses/big/chocolate.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COATEDOPTICS:String = "../maps/icons/quests/bonuses/big/coatedOptics.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COATEDOPTICSBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/coatedOpticsBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COCACOLA:String = "../maps/icons/quests/bonuses/big/cocacola.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMBATPAYMENTS:String = "../maps/icons/quests/bonuses/big/combatPayments.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMMANDER_SIXTHSENSE:String = "../maps/icons/quests/bonuses/big/commander_sixthSense.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS:String = "../maps/icons/quests/bonuses/big/completionTokens.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_1_1:String = "../maps/icons/quests/bonuses/big/completionTokens_1_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_1_2:String = "../maps/icons/quests/bonuses/big/completionTokens_1_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_1_3:String = "../maps/icons/quests/bonuses/big/completionTokens_1_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_1_4:String = "../maps/icons/quests/bonuses/big/completionTokens_1_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_1_5:String = "../maps/icons/quests/bonuses/big/completionTokens_1_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_2_1:String = "../maps/icons/quests/bonuses/big/completionTokens_2_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_2_2:String = "../maps/icons/quests/bonuses/big/completionTokens_2_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_2_3:String = "../maps/icons/quests/bonuses/big/completionTokens_2_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_2_4:String = "../maps/icons/quests/bonuses/big/completionTokens_2_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_2_5:String = "../maps/icons/quests/bonuses/big/completionTokens_2_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_3_1:String = "../maps/icons/quests/bonuses/big/completionTokens_3_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_3_2:String = "../maps/icons/quests/bonuses/big/completionTokens_3_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_3_3:String = "../maps/icons/quests/bonuses/big/completionTokens_3_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_3_4:String = "../maps/icons/quests/bonuses/big/completionTokens_3_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_3_5:String = "../maps/icons/quests/bonuses/big/completionTokens_3_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_4_1:String = "../maps/icons/quests/bonuses/big/completionTokens_4_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_4_2:String = "../maps/icons/quests/bonuses/big/completionTokens_4_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_4_3:String = "../maps/icons/quests/bonuses/big/completionTokens_4_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_4_4:String = "../maps/icons/quests/bonuses/big/completionTokens_4_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_4_5:String = "../maps/icons/quests/bonuses/big/completionTokens_4_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_5_1:String = "../maps/icons/quests/bonuses/big/completionTokens_5_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_5_2:String = "../maps/icons/quests/bonuses/big/completionTokens_5_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_5_3:String = "../maps/icons/quests/bonuses/big/completionTokens_5_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_5_4:String = "../maps/icons/quests/bonuses/big/completionTokens_5_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_6_1:String = "../maps/icons/quests/bonuses/big/completionTokens_6_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_6_2:String = "../maps/icons/quests/bonuses/big/completionTokens_6_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_6_3:String = "../maps/icons/quests/bonuses/big/completionTokens_6_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_6_4:String = "../maps/icons/quests/bonuses/big/completionTokens_6_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_7_1:String = "../maps/icons/quests/bonuses/big/completionTokens_7_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_7_2:String = "../maps/icons/quests/bonuses/big/completionTokens_7_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_7_3:String = "../maps/icons/quests/bonuses/big/completionTokens_7_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_COMPLETIONTOKENS_7_4:String = "../maps/icons/quests/bonuses/big/completionTokens_7_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CREDITS:String = "../maps/icons/quests/bonuses/big/credits.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CREW:String = "../maps/icons/quests/bonuses/big/crew.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CREWSKIN1:String = "../maps/icons/quests/bonuses/big/crewSkin1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CREWSKIN2:String = "../maps/icons/quests/bonuses/big/crewSkin2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CREWSKIN3:String = "../maps/icons/quests/bonuses/big/crewSkin3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_CRYSTAL:String = "../maps/icons/quests/bonuses/big/crystal.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_DECAL:String = "../maps/icons/quests/bonuses/big/decal.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_DEFAULT:String = "../maps/icons/quests/bonuses/big/default.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_DRIVER_SMOOTHDRIVING:String = "../maps/icons/quests/bonuses/big/driver_smoothDriving.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_DRIVER_VIRTUOSO:String = "../maps/icons/quests/bonuses/big/driver_virtuoso.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_EMBLEM:String = "../maps/icons/quests/bonuses/big/emblem.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_ENHANCEDAIMDRIVES:String = "../maps/icons/quests/bonuses/big/enhancedAimDrives.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_ENHANCEDAIMDRIVESBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/enhancedAimDrivesBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_EQUIPMENTPLUS_OVERLAY:String = "../maps/icons/quests/bonuses/big/equipmentPlus_overlay.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_EXP:String = "../maps/icons/quests/bonuses/big/exp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FILTERCYCLONE:String = "../maps/icons/quests/bonuses/big/filterCyclone.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FIREFIGHTING:String = "../maps/icons/quests/bonuses/big/fireFighting.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FIREFIGHTINGBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/fireFightingBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FREEEXP:String = "../maps/icons/quests/bonuses/big/freeExp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FREETOKENS:String = "../maps/icons/quests/bonuses/big/freeTokens.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FREETOKENS_0:String = "../maps/icons/quests/bonuses/big/freeTokens_0.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FREETOKENS_2:String = "../maps/icons/quests/bonuses/big/freeTokens_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_FREEXP:String = "../maps/icons/quests/bonuses/big/freeXP.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GASOLINE100:String = "../maps/icons/quests/bonuses/big/gasoline100.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GASOLINE105:String = "../maps/icons/quests/bonuses/big/gasoline105.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GERMANY_G104_STUG_IV:String = "../maps/icons/quests/bonuses/big/germany-G104_Stug_IV.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GERMANY_G105_T_55_NVA_DDR:String = "../maps/icons/quests/bonuses/big/germany-G105_T-55_NVA_DDR.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GOLD:String = "../maps/icons/quests/bonuses/big/gold.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GROUSERS:String = "../maps/icons/quests/bonuses/big/grousers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GUNNER_RANCOROUS:String = "../maps/icons/quests/bonuses/big/gunner_rancorous.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_GUNNER_SMOOTHTURRET:String = "../maps/icons/quests/bonuses/big/gunner_smoothTurret.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_HANDEXTINGUISHERS:String = "../maps/icons/quests/bonuses/big/handExtinguishers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_HOTCOFFEE:String = "../maps/icons/quests/bonuses/big/hotCoffee.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_IMPROVEDVENTILATION:String = "../maps/icons/quests/bonuses/big/improvedVentilation.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_IMPROVEDVENTILATIONBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/improvedVentilationBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_INSCRIPTION:String = "../maps/icons/quests/bonuses/big/inscription.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_LARGEMEDKIT:String = "../maps/icons/quests/bonuses/big/largeMedkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_LARGEREPAIRKIT:String = "../maps/icons/quests/bonuses/big/largeRepairkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_LASTEFFORTBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/lastEffortBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_LENDLEASEOIL:String = "../maps/icons/quests/bonuses/big/lendLeaseOil.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_LOADER_PEDANT:String = "../maps/icons/quests/bonuses/big/loader_pedant.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_LOOTBOX_NEWYEAR_PREMIUM:String = "../maps/icons/quests/bonuses/big/lootBox_newYear_premium.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_LOOTBOX_NEWYEAR_USUAL:String = "../maps/icons/quests/bonuses/big/lootBox_newYear_usual.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_MILITARYEXERCISES:String = "../maps/icons/quests/bonuses/big/militaryExercises.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_MODIFICATION:String = "../maps/icons/quests/bonuses/big/modification.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PAINT:String = "../maps/icons/quests/bonuses/big/paint.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PEDANTBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/pedantBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PERSONALNUMBER:String = "../maps/icons/quests/bonuses/big/personalNumber.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_1:String = "../maps/icons/quests/bonuses/big/premium_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_14:String = "../maps/icons/quests/bonuses/big/premium_14.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_180:String = "../maps/icons/quests/bonuses/big/premium_180.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_2:String = "../maps/icons/quests/bonuses/big/premium_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_3:String = "../maps/icons/quests/bonuses/big/premium_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_30:String = "../maps/icons/quests/bonuses/big/premium_30.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_360:String = "../maps/icons/quests/bonuses/big/premium_360.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_7:String = "../maps/icons/quests/bonuses/big/premium_7.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_90:String = "../maps/icons/quests/bonuses/big/premium_90.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_1:String = "../maps/icons/quests/bonuses/big/premium_plus_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_14:String = "../maps/icons/quests/bonuses/big/premium_plus_14.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_180:String = "../maps/icons/quests/bonuses/big/premium_plus_180.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_2:String = "../maps/icons/quests/bonuses/big/premium_plus_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_3:String = "../maps/icons/quests/bonuses/big/premium_plus_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_30:String = "../maps/icons/quests/bonuses/big/premium_plus_30.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_360:String = "../maps/icons/quests/bonuses/big/premium_plus_360.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_7:String = "../maps/icons/quests/bonuses/big/premium_plus_7.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PREMIUM_PLUS_90:String = "../maps/icons/quests/bonuses/big/premium_plus_90.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_PROJECTIONDECAL:String = "../maps/icons/quests/bonuses/big/projectionDecal.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_QUALITYOIL:String = "../maps/icons/quests/bonuses/big/qualityOil.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RADIOMAN_LASTEFFORT:String = "../maps/icons/quests/bonuses/big/radioman_lastEffort.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RAMMER:String = "../maps/icons/quests/bonuses/big/rammer.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RAMMERBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/rammerBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RANCOROUSBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/rancorousBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RANKEDPOINT:String = "../maps/icons/quests/bonuses/big/rankedPoint.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION:String = "../maps/icons/quests/bonuses/big/ration.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION_CHINA:String = "../maps/icons/quests/bonuses/big/ration_china.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION_CZECH:String = "../maps/icons/quests/bonuses/big/ration_czech.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION_ITALY:String = "../maps/icons/quests/bonuses/big/ration_italy.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION_JAPAN:String = "../maps/icons/quests/bonuses/big/ration_japan.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION_POLAND:String = "../maps/icons/quests/bonuses/big/ration_poland.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION_SWEDEN:String = "../maps/icons/quests/bonuses/big/ration_sweden.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_RATION_UK:String = "../maps/icons/quests/bonuses/big/ration_uk.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_REMOVEDRPMLIMITER:String = "../maps/icons/quests/bonuses/big/removedRpmLimiter.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_REWARD_SHEET:String = "../maps/icons/quests/bonuses/big/reward_sheet.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_SIXTHSENSEBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/sixthSenseBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_SLOTS:String = "../maps/icons/quests/bonuses/big/slots.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_SMALLMEDKIT:String = "../maps/icons/quests/bonuses/big/smallMedkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_SMALLREPAIRKIT:String = "../maps/icons/quests/bonuses/big/smallRepairkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_SMOOTHDRIVINGBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/smoothDrivingBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_SMOOTHTURRETBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/smoothTurretBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_STEELROLLERS:String = "../maps/icons/quests/bonuses/big/steelRollers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_STEREOSCOPE:String = "../maps/icons/quests/bonuses/big/stereoscope.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_STYLE:String = "../maps/icons/quests/bonuses/big/style.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_TACTICALTRAINING:String = "../maps/icons/quests/bonuses/big/tacticalTraining.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_TANKMAN:String = "../maps/icons/quests/bonuses/big/tankman.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_TANKMEN:String = "../maps/icons/quests/bonuses/big/tankmen.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_TANKMENXP:String = "../maps/icons/quests/bonuses/big/tankmenXP.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_TANKWOMAN:String = "../maps/icons/quests/bonuses/big/tankwoman.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_TOOLBOX:String = "../maps/icons/quests/bonuses/big/toolbox.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_TOOLBOXBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/toolboxBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_UK_GB96_EXCALIBUR:String = "../maps/icons/quests/bonuses/big/uk-GB96_Excalibur.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_UK_GB97_CHIMERA:String = "../maps/icons/quests/bonuses/big/uk-GB97_Chimera.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_USA_A102_T28_CONCEPT:String = "../maps/icons/quests/bonuses/big/usa-A102_T28_concept.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_USSR_R110_OBJECT_260:String = "../maps/icons/quests/bonuses/big/ussr-R110_Object_260.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_USSR_R157_OBJECT_279R:String = "../maps/icons/quests/bonuses/big/ussr-R157_Object_279R.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_VEHICLES:String = "../maps/icons/quests/bonuses/big/vehicles.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_VEHICLES_RENT:String = "../maps/icons/quests/bonuses/big/vehicles_rent.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_VIRTUOSOBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/big/virtuosoBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BIG_WETCOMBATPACK:String = "../maps/icons/quests/bonuses/big/wetCombatPack.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBAMWAY921:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobAmway921.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBAWESOMEEPICGUYS:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobAwesomeEpicGuys.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBDEZGAMEZ:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobDezgamez.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBDRAGONS:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobDragons.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBKORBENDALLAS:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobKorbenDallas.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBLEBWA:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobLebwa.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBMAILAND:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobMailand.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBSKILL4LTU:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobSkill4ltu.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBTIGERS:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobTigers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_ACHIEVEMENTS_MEDALBOBYUSHA:String = "../maps/icons/quests/bonuses/bob/achievements/medalBobYusha.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_1:String = "../maps/icons/quests/bonuses/bob/customization/emblem_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_10:String = "../maps/icons/quests/bonuses/bob/customization/emblem_10.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_11:String = "../maps/icons/quests/bonuses/bob/customization/emblem_11.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_12:String = "../maps/icons/quests/bonuses/bob/customization/emblem_12.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_2:String = "../maps/icons/quests/bonuses/bob/customization/emblem_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_3:String = "../maps/icons/quests/bonuses/bob/customization/emblem_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_4:String = "../maps/icons/quests/bonuses/bob/customization/emblem_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_5:String = "../maps/icons/quests/bonuses/bob/customization/emblem_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_6:String = "../maps/icons/quests/bonuses/bob/customization/emblem_6.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_7:String = "../maps/icons/quests/bonuses/bob/customization/emblem_7.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_8:String = "../maps/icons/quests/bonuses/bob/customization/emblem_8.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_EMBLEM_9:String = "../maps/icons/quests/bonuses/bob/customization/emblem_9.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_1:String = "../maps/icons/quests/bonuses/bob/customization/inscription_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_10:String = "../maps/icons/quests/bonuses/bob/customization/inscription_10.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_11:String = "../maps/icons/quests/bonuses/bob/customization/inscription_11.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_12:String = "../maps/icons/quests/bonuses/bob/customization/inscription_12.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_2:String = "../maps/icons/quests/bonuses/bob/customization/inscription_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_3:String = "../maps/icons/quests/bonuses/bob/customization/inscription_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_4:String = "../maps/icons/quests/bonuses/bob/customization/inscription_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_5:String = "../maps/icons/quests/bonuses/bob/customization/inscription_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_6:String = "../maps/icons/quests/bonuses/bob/customization/inscription_6.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_7:String = "../maps/icons/quests/bonuses/bob/customization/inscription_7.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_8:String = "../maps/icons/quests/bonuses/bob/customization/inscription_8.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_INSCRIPTION_9:String = "../maps/icons/quests/bonuses/bob/customization/inscription_9.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_1:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_10:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_10.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_11:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_11.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_12:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_12.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_2:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_3:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_4:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_5:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_6:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_6.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_7:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_7.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_8:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_8.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_PROJECTIONDECAL_9:String = "../maps/icons/quests/bonuses/bob/customization/projectionDecal_9.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_CUSTOMIZATION_STYLE:String = "../maps/icons/quests/bonuses/bob/customization/style.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_VEHICLES_CH41_WZ_111_5A_BOB:String = "../maps/icons/quests/bonuses/bob/vehicles/Ch41_WZ_111_5A_bob.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_VEHICLES_G134_PZKPFW_VII_BOB:String = "../maps/icons/quests/bonuses/bob/vehicles/G134_PzKpfw_VII_bob.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_VEHICLES_IT08_PROGETTO_M40_MOD_65_BOB:String = "../maps/icons/quests/bonuses/bob/vehicles/It08_Progetto_M40_mod_65_bob.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_VEHICLES_R97_OBJECT_140_BOB:String = "../maps/icons/quests/bonuses/bob/vehicles/R97_Object_140_bob.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_BOB_VEHICLES_S28_UDES_15_16_BOB:String = "../maps/icons/quests/bonuses/bob/vehicles/S28_UDES_15_16_bob.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_ADDITIONALBRIEFING:String = "../maps/icons/quests/bonuses/small/additionalBriefing.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_AIMINGSTABILIZER:String = "../maps/icons/quests/bonuses/small/aimingStabilizer.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_AIMINGSTABILIZERBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/aimingStabilizerBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_ANTIFRAGMENTATIONLINING:String = "../maps/icons/quests/bonuses/small/antifragmentationLining.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_AUTOEXTINGUISHERS:String = "../maps/icons/quests/bonuses/small/autoExtinguishers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BATTLEBOOSTERREPLACE_OVERLAY:String = "../maps/icons/quests/bonuses/small/battleBoosterReplace_overlay.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BATTLEBOOSTER_HIGHLIGHT:String = "../maps/icons/quests/bonuses/small/battleBooster_highlight.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BATTLEBOOSTER_OVERLAY:String = "../maps/icons/quests/bonuses/small/battleBooster_overlay.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BERTHS:String = "../maps/icons/quests/bonuses/small/berths.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BOB_POINTS:String = "../maps/icons/quests/bonuses/small/bob_points.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BOB_POINTS_NA_ASIA:String = "../maps/icons/quests/bonuses/small/bob_points_na_asia.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BONUS_BATTLE_TASK:String = "../maps/icons/quests/bonuses/small/bonus_battle_task.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BOOSTER_CREDITS:String = "../maps/icons/quests/bonuses/small/booster_credits.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BOOSTER_CREW_XP:String = "../maps/icons/quests/bonuses/small/booster_crew_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BOOSTER_FL_XP:String = "../maps/icons/quests/bonuses/small/booster_fl_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BOOSTER_FREE_XP:String = "../maps/icons/quests/bonuses/small/booster_free_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BOOSTER_XP:String = "../maps/icons/quests/bonuses/small/booster_xp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_BUILTINEQUIPMENT_OVERLAY:String = "../maps/icons/quests/bonuses/small/builtInEquipment_overlay.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CAMOUFLAGE:String = "../maps/icons/quests/bonuses/small/camouflage.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CAMOUFLAGEBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/camouflageBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CAMOUFLAGENET:String = "../maps/icons/quests/bonuses/small/camouflageNet.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CARBONDIOXIDE:String = "../maps/icons/quests/bonuses/small/carbonDioxide.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CHOCOLATE:String = "../maps/icons/quests/bonuses/small/chocolate.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COATEDOPTICS:String = "../maps/icons/quests/bonuses/small/coatedOptics.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COATEDOPTICSBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/coatedOpticsBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COCACOLA:String = "../maps/icons/quests/bonuses/small/cocacola.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMBATPAYMENTS:String = "../maps/icons/quests/bonuses/small/combatPayments.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMMANDER_SIXTHSENSE:String = "../maps/icons/quests/bonuses/small/commander_sixthSense.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS:String = "../maps/icons/quests/bonuses/small/completionTokens.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_1_1:String = "../maps/icons/quests/bonuses/small/completionTokens_1_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_1_2:String = "../maps/icons/quests/bonuses/small/completionTokens_1_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_1_3:String = "../maps/icons/quests/bonuses/small/completionTokens_1_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_1_4:String = "../maps/icons/quests/bonuses/small/completionTokens_1_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_1_5:String = "../maps/icons/quests/bonuses/small/completionTokens_1_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_2_1:String = "../maps/icons/quests/bonuses/small/completionTokens_2_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_2_2:String = "../maps/icons/quests/bonuses/small/completionTokens_2_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_2_3:String = "../maps/icons/quests/bonuses/small/completionTokens_2_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_2_4:String = "../maps/icons/quests/bonuses/small/completionTokens_2_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_2_5:String = "../maps/icons/quests/bonuses/small/completionTokens_2_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_3_1:String = "../maps/icons/quests/bonuses/small/completionTokens_3_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_3_2:String = "../maps/icons/quests/bonuses/small/completionTokens_3_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_3_3:String = "../maps/icons/quests/bonuses/small/completionTokens_3_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_3_4:String = "../maps/icons/quests/bonuses/small/completionTokens_3_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_3_5:String = "../maps/icons/quests/bonuses/small/completionTokens_3_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_4_1:String = "../maps/icons/quests/bonuses/small/completionTokens_4_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_4_2:String = "../maps/icons/quests/bonuses/small/completionTokens_4_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_4_3:String = "../maps/icons/quests/bonuses/small/completionTokens_4_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_4_4:String = "../maps/icons/quests/bonuses/small/completionTokens_4_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_4_5:String = "../maps/icons/quests/bonuses/small/completionTokens_4_5.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_5_1:String = "../maps/icons/quests/bonuses/small/completionTokens_5_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_5_2:String = "../maps/icons/quests/bonuses/small/completionTokens_5_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_5_3:String = "../maps/icons/quests/bonuses/small/completionTokens_5_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_5_4:String = "../maps/icons/quests/bonuses/small/completionTokens_5_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_6_1:String = "../maps/icons/quests/bonuses/small/completionTokens_6_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_6_2:String = "../maps/icons/quests/bonuses/small/completionTokens_6_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_6_3:String = "../maps/icons/quests/bonuses/small/completionTokens_6_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_6_4:String = "../maps/icons/quests/bonuses/small/completionTokens_6_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_7_1:String = "../maps/icons/quests/bonuses/small/completionTokens_7_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_7_2:String = "../maps/icons/quests/bonuses/small/completionTokens_7_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_7_3:String = "../maps/icons/quests/bonuses/small/completionTokens_7_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_COMPLETIONTOKENS_7_4:String = "../maps/icons/quests/bonuses/small/completionTokens_7_4.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CREDITS:String = "../maps/icons/quests/bonuses/small/credits.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CREW:String = "../maps/icons/quests/bonuses/small/crew.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CREWSKIN1:String = "../maps/icons/quests/bonuses/small/crewSkin1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CREWSKIN2:String = "../maps/icons/quests/bonuses/small/crewSkin2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CREWSKIN3:String = "../maps/icons/quests/bonuses/small/crewSkin3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_CRYSTAL:String = "../maps/icons/quests/bonuses/small/crystal.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_DECAL:String = "../maps/icons/quests/bonuses/small/decal.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_DEFAULT:String = "../maps/icons/quests/bonuses/small/default.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_DRIVER_SMOOTHDRIVING:String = "../maps/icons/quests/bonuses/small/driver_smoothDriving.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_DRIVER_VIRTUOSO:String = "../maps/icons/quests/bonuses/small/driver_virtuoso.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_EMBLEM:String = "../maps/icons/quests/bonuses/small/emblem.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_ENHANCEDAIMDRIVES:String = "../maps/icons/quests/bonuses/small/enhancedAimDrives.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_ENHANCEDAIMDRIVESBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/enhancedAimDrivesBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_EQUIPMENTPLUS_OVERLAY:String = "../maps/icons/quests/bonuses/small/equipmentPlus_overlay.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_EXP:String = "../maps/icons/quests/bonuses/small/exp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_FILTERCYCLONE:String = "../maps/icons/quests/bonuses/small/filterCyclone.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_FIREFIGHTING:String = "../maps/icons/quests/bonuses/small/fireFighting.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_FIREFIGHTINGBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/fireFightingBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_FREEEXP:String = "../maps/icons/quests/bonuses/small/freeExp.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_FREETOKENS:String = "../maps/icons/quests/bonuses/small/freeTokens.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_FREETOKENS_0:String = "../maps/icons/quests/bonuses/small/freeTokens_0.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_FREETOKENS_2:String = "../maps/icons/quests/bonuses/small/freeTokens_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_GASOLINE100:String = "../maps/icons/quests/bonuses/small/gasoline100.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_GASOLINE105:String = "../maps/icons/quests/bonuses/small/gasoline105.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_GOLD:String = "../maps/icons/quests/bonuses/small/gold.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_GROUSERS:String = "../maps/icons/quests/bonuses/small/grousers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_GUNNER_RANCOROUS:String = "../maps/icons/quests/bonuses/small/gunner_rancorous.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_GUNNER_SMOOTHTURRET:String = "../maps/icons/quests/bonuses/small/gunner_smoothTurret.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_HANDEXTINGUISHERS:String = "../maps/icons/quests/bonuses/small/handExtinguishers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_HOTCOFFEE:String = "../maps/icons/quests/bonuses/small/hotCoffee.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_IMPROVEDVENTILATION:String = "../maps/icons/quests/bonuses/small/improvedVentilation.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_IMPROVEDVENTILATIONBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/improvedVentilationBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_INSCRIPTION:String = "../maps/icons/quests/bonuses/small/inscription.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_LARGEMEDKIT:String = "../maps/icons/quests/bonuses/small/largeMedkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_LARGEREPAIRKIT:String = "../maps/icons/quests/bonuses/small/largeRepairkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_LASTEFFORTBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/lastEffortBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_LENDLEASEOIL:String = "../maps/icons/quests/bonuses/small/lendLeaseOil.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_LOADER_PEDANT:String = "../maps/icons/quests/bonuses/small/loader_pedant.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_LOOTBOX_NEWYEAR_PREMIUM:String = "../maps/icons/quests/bonuses/small/lootBox_newYear_premium.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_LOOTBOX_NEWYEAR_USUAL:String = "../maps/icons/quests/bonuses/small/lootBox_newYear_usual.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_MODIFICATION:String = "../maps/icons/quests/bonuses/small/modification.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PAINT:String = "../maps/icons/quests/bonuses/small/paint.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PEDANTBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/pedantBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PERSONALNUMBER:String = "../maps/icons/quests/bonuses/small/personalNumber.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_1:String = "../maps/icons/quests/bonuses/small/premium_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_14:String = "../maps/icons/quests/bonuses/small/premium_14.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_180:String = "../maps/icons/quests/bonuses/small/premium_180.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_2:String = "../maps/icons/quests/bonuses/small/premium_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_3:String = "../maps/icons/quests/bonuses/small/premium_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_30:String = "../maps/icons/quests/bonuses/small/premium_30.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_360:String = "../maps/icons/quests/bonuses/small/premium_360.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_7:String = "../maps/icons/quests/bonuses/small/premium_7.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_90:String = "../maps/icons/quests/bonuses/small/premium_90.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_1:String = "../maps/icons/quests/bonuses/small/premium_plus_1.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_14:String = "../maps/icons/quests/bonuses/small/premium_plus_14.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_180:String = "../maps/icons/quests/bonuses/small/premium_plus_180.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_2:String = "../maps/icons/quests/bonuses/small/premium_plus_2.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_3:String = "../maps/icons/quests/bonuses/small/premium_plus_3.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_30:String = "../maps/icons/quests/bonuses/small/premium_plus_30.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_360:String = "../maps/icons/quests/bonuses/small/premium_plus_360.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_7:String = "../maps/icons/quests/bonuses/small/premium_plus_7.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PREMIUM_PLUS_90:String = "../maps/icons/quests/bonuses/small/premium_plus_90.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_PROJECTIONDECAL:String = "../maps/icons/quests/bonuses/small/projectionDecal.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_QUALITYOIL:String = "../maps/icons/quests/bonuses/small/qualityOil.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RADIOMAN_LASTEFFORT:String = "../maps/icons/quests/bonuses/small/radioman_lastEffort.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RAMMER:String = "../maps/icons/quests/bonuses/small/rammer.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RAMMERBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/rammerBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RANCOROUSBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/rancorousBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RANKEDPOINT:String = "../maps/icons/quests/bonuses/small/rankedPoint.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION:String = "../maps/icons/quests/bonuses/small/ration.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION_CHINA:String = "../maps/icons/quests/bonuses/small/ration_china.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION_CZECH:String = "../maps/icons/quests/bonuses/small/ration_czech.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION_ITALY:String = "../maps/icons/quests/bonuses/small/ration_italy.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION_JAPAN:String = "../maps/icons/quests/bonuses/small/ration_japan.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION_POLAND:String = "../maps/icons/quests/bonuses/small/ration_poland.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION_SWEDEN:String = "../maps/icons/quests/bonuses/small/ration_sweden.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_RATION_UK:String = "../maps/icons/quests/bonuses/small/ration_uk.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_REMOVEDRPMLIMITER:String = "../maps/icons/quests/bonuses/small/removedRpmLimiter.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_REWARD_SHEET:String = "../maps/icons/quests/bonuses/small/reward_sheet.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_SIXTHSENSEBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/sixthSenseBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_SLOTS:String = "../maps/icons/quests/bonuses/small/slots.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_SMALLMEDKIT:String = "../maps/icons/quests/bonuses/small/smallMedkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_SMALLREPAIRKIT:String = "../maps/icons/quests/bonuses/small/smallRepairkit.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_SMOOTHDRIVINGBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/smoothDrivingBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_SMOOTHTURRETBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/smoothTurretBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_STEELROLLERS:String = "../maps/icons/quests/bonuses/small/steelRollers.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_STEREOSCOPE:String = "../maps/icons/quests/bonuses/small/stereoscope.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_STYLE:String = "../maps/icons/quests/bonuses/small/style.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_TACTICALTRAINING:String = "../maps/icons/quests/bonuses/small/tacticalTraining.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_TANKMAN:String = "../maps/icons/quests/bonuses/small/tankman.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_TANKMEN:String = "../maps/icons/quests/bonuses/small/tankmen.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_TANKMENXP:String = "../maps/icons/quests/bonuses/small/tankmenXP.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_TANKWOMAN:String = "../maps/icons/quests/bonuses/small/tankwoman.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_TOOLBOX:String = "../maps/icons/quests/bonuses/small/toolbox.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_TOOLBOXBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/toolboxBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_VEHICLES:String = "../maps/icons/quests/bonuses/small/vehicles.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_VEHICLES_RENT:String = "../maps/icons/quests/bonuses/small/vehicles_rent.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_VIRTUOSOBATTLEBOOSTER:String = "../maps/icons/quests/bonuses/small/virtuosoBattleBooster.png";

        public static const MAPS_ICONS_QUESTS_BONUSES_SMALL_WETCOMBATPACK:String = "../maps/icons/quests/bonuses/small/wetCombatPack.png";

        public static const MAPS_ICONS_RANKEDBATTLES_ICON_FINAL_CUP_100X100:String = "../maps/icons/rankedBattles/icon_final_cup_100x100.png";

        public static const MAPS_ICONS_RANKEDBATTLES_ICON_FINAL_CUP_150X100:String = "../maps/icons/rankedBattles/icon_final_cup_150x100.png";

        public static const MAPS_ICONS_RANKEDBATTLES_ICON_FINAL_CUP_80X80:String = "../maps/icons/rankedBattles/icon_final_cup_80x80.png";

        public static const MAPS_ICONS_RANKEDBATTLES_ICON_RANK_FINAL_PROXY_80X80:String = "../maps/icons/rankedBattles/icon_rank_final_proxy_80x80.png";

        public static const MAPS_ICONS_RANKEDBATTLES_ICON_VICTORY:String = "../maps/icons/rankedBattles/icon_victory.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LADDER_POINT:String = "../maps/icons/rankedBattles/ladder_point.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LADDER_POINT_HUGE:String = "../maps/icons/rankedBattles/ladder_point_huge.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LBM_ICON:String = "../maps/icons/rankedBattles/lbm_icon.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKED_POINT_28X28:String = "../maps/icons/rankedBattles/ranked_point_28x28.png";

        public static const MAPS_ICONS_RANKEDBATTLES_SPRINTER_ICON:String = "../maps/icons/rankedBattles/sprinter_icon.png";

        public static const MAPS_ICONS_RANKEDBATTLES_BG_INTRO:String = "../maps/icons/rankedBattles/bg/intro.png";

        public static const MAPS_ICONS_RANKEDBATTLES_BG_MAIN:String = "../maps/icons/rankedBattles/bg/main.png";

        public static const MAPS_ICONS_RANKEDBATTLES_BG_REWARDS:String = "../maps/icons/rankedBattles/bg/rewards.png";

        public static const MAPS_ICONS_RANKEDBATTLES_BONUSICONS_48X48:String = "../maps/icons/rankedBattles/bonusIcons/48x48.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_0:String = "../maps/icons/rankedBattles/divisions/114x160/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_1:String = "../maps/icons/rankedBattles/divisions/114x160/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_2:String = "../maps/icons/rankedBattles/divisions/114x160/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_3:String = "../maps/icons/rankedBattles/divisions/114x160/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_0:String = "../maps/icons/rankedBattles/divisions/190x260/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_1:String = "../maps/icons/rankedBattles/divisions/190x260/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_2:String = "../maps/icons/rankedBattles/divisions/190x260/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_3:String = "../maps/icons/rankedBattles/divisions/190x260/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_BRONZE:String = "../maps/icons/rankedBattles/divisions/56x56/bronze.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_CLASSIFICATION:String = "../maps/icons/rankedBattles/divisions/56x56/classification.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_GOLD:String = "../maps/icons/rankedBattles/divisions/56x56/gold.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_SILVER:String = "../maps/icons/rankedBattles/divisions/56x56/silver.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_0:String = "../maps/icons/rankedBattles/divisions/58x80/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_1:String = "../maps/icons/rankedBattles/divisions/58x80/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_2:String = "../maps/icons/rankedBattles/divisions/58x80/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_3:String = "../maps/icons/rankedBattles/divisions/58x80/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_0:String = "../maps/icons/rankedBattles/divisions/80x110/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_1:String = "../maps/icons/rankedBattles/divisions/80x110/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_2:String = "../maps/icons/rankedBattles/divisions/80x110/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_3:String = "../maps/icons/rankedBattles/divisions/80x110/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_DELTA_MINUS:String = "../maps/icons/rankedBattles/league/delta_minus.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_DELTA_PLUS:String = "../maps/icons/rankedBattles/league/delta_plus.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_LEAGUE_INDEFINITE_PROGRESS:String = "../maps/icons/rankedBattles/league/league_indefinite_progress.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_0:String = "../maps/icons/rankedBattles/league/100x100/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_1:String = "../maps/icons/rankedBattles/league/100x100/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_2:String = "../maps/icons/rankedBattles/league/100x100/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_3:String = "../maps/icons/rankedBattles/league/100x100/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_0:String = "../maps/icons/rankedBattles/league/130x130/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_1:String = "../maps/icons/rankedBattles/league/130x130/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_2:String = "../maps/icons/rankedBattles/league/130x130/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_3:String = "../maps/icons/rankedBattles/league/130x130/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_0:String = "../maps/icons/rankedBattles/league/300x300/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_1:String = "../maps/icons/rankedBattles/league/300x300/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_2:String = "../maps/icons/rankedBattles/league/300x300/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_3:String = "../maps/icons/rankedBattles/league/300x300/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_0:String = "../maps/icons/rankedBattles/league/70x70/0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_1:String = "../maps/icons/rankedBattles/league/70x70/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_2:String = "../maps/icons/rankedBattles/league/70x70/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_3:String = "../maps/icons/rankedBattles/league/70x70/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_AWARD:String = "../maps/icons/rankedBattles/rankedBattesView/award.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_AWARDBLOCKBG:String = "../maps/icons/rankedBattles/rankedBattesView/awardBlockBg.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_GIFT_BOX_208X100:String = "../maps/icons/rankedBattles/rankedBattesView/gift_box_208x100.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICN_CREW:String = "../maps/icons/rankedBattles/rankedBattesView/icn_crew.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICN_TANKS:String = "../maps/icons/rankedBattles/rankedBattesView/icn_tanks.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICON_RANKS_LADDERS_208X100:String = "../maps/icons/rankedBattles/rankedBattesView/icon_ranks_ladders_208x100.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICON_RANKS_TASK_208X100:String = "../maps/icons/rankedBattles/rankedBattesView/icon_ranks_task_208x100.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICON_TOP12_106X98:String = "../maps/icons/rankedBattles/rankedBattesView/icon_top12_106x98.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICON_TOP12_53X49:String = "../maps/icons/rankedBattles/rankedBattesView/icon_top12_53x49.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICON_TOP3_106X98:String = "../maps/icons/rankedBattles/rankedBattesView/icon_top3_106x98.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_ICON_TOP3_53X49:String = "../maps/icons/rankedBattles/rankedBattesView/icon_top3_53x49.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_PIC_ICON_RANK_SHINE_364X364:String = "../maps/icons/rankedBattles/rankedBattesView/pic_icon_rank_shine_364x364.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKEDBATTESVIEW_PIC_ICON_SHADOW_100X100:String = "../maps/icons/rankedBattles/rankedBattesView/pic_icon_shadow_100x100.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_UNKNOWN_RANK_24X24:String = "../maps/icons/rankedBattles/ranks/unknown_rank_24x24.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_1:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_10:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_11:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_12:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_13:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_14:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_15:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_2:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_3:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_4:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_5:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_6:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_7:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_8:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_9:String = "../maps/icons/rankedBattles/ranks/114x160/rank0_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_1:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_10:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_11:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_12:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_13:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_14:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_15:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_2:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_3:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_4:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_5:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_6:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_7:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_8:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_9:String = "../maps/icons/rankedBattles/ranks/114x160/rank1_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_1:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_10:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_11:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_12:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_13:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_14:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_15:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_2:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_3:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_4:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_5:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_6:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_7:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_8:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_9:String = "../maps/icons/rankedBattles/ranks/114x160/rank2_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_1:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_10:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_11:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_12:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_13:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_14:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_15:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_2:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_3:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_4:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_5:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_6:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_7:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_8:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_9:String = "../maps/icons/rankedBattles/ranks/114x160/rank3_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP0_1:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP0_2:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP0_3:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP1_1:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP1_2:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP1_3:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP2_1:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP2_2:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP2_3:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP3_1:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP3_2:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP3_3:String = "../maps/icons/rankedBattles/ranks/114x160/ranks_group3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKVEHMASTER:String = "../maps/icons/rankedBattles/ranks/114x160/rankVehMaster.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_1:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_10:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_11:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_12:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_13:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_14:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_15:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_2:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_3:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_4:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_5:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_6:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_7:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_8:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_9:String = "../maps/icons/rankedBattles/ranks/190x260/rank0_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_1:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_10:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_11:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_12:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_13:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_14:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_15:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_2:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_3:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_4:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_5:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_6:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_7:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_8:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_9:String = "../maps/icons/rankedBattles/ranks/190x260/rank1_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_1:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_10:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_11:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_12:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_13:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_14:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_15:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_2:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_3:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_4:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_5:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_6:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_7:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_8:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_9:String = "../maps/icons/rankedBattles/ranks/190x260/rank2_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_1:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_10:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_11:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_12:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_13:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_14:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_15:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_2:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_3:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_4:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_5:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_6:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_7:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_8:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_9:String = "../maps/icons/rankedBattles/ranks/190x260/rank3_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP0_1:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP0_2:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP0_3:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP1_1:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP1_2:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP1_3:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP2_1:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP2_2:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP2_3:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP3_1:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP3_2:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP3_3:String = "../maps/icons/rankedBattles/ranks/190x260/ranks_group3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKVEHMASTER:String = "../maps/icons/rankedBattles/ranks/190x260/rankVehMaster.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0:String = "../maps/icons/rankedBattles/ranks/24x24/rank0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_0:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_1:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_10:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_11:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_12:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_13:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_14:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_15:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_2:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_3:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_4:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_5:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_6:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_7:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_8:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_9:String = "../maps/icons/rankedBattles/ranks/24x24/rank0_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_0:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_1:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_10:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_11:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_12:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_13:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_14:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_15:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_2:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_3:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_4:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_5:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_6:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_7:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_8:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_9:String = "../maps/icons/rankedBattles/ranks/24x24/rank1_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_0:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_1:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_10:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_11:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_12:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_13:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_14:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_15:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_2:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_3:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_4:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_5:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_6:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_7:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_8:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_9:String = "../maps/icons/rankedBattles/ranks/24x24/rank2_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_0:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_1:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_10:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_11:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_12:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_13:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_14:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_15:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_2:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_3:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_4:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_5:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_6:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_7:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_8:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_9:String = "../maps/icons/rankedBattles/ranks/24x24/rank3_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK4_0:String = "../maps/icons/rankedBattles/ranks/24x24/rank4_0.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP0_1:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP0_2:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP0_3:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP1_1:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP1_2:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP1_3:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP2_1:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP2_2:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP2_3:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP3_1:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP3_2:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP3_3:String = "../maps/icons/rankedBattles/ranks/24x24/ranks_group3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_1:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_10:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_11:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_12:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_13:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_14:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_15:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_2:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_3:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_4:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_5:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_6:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_7:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_8:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_9:String = "../maps/icons/rankedBattles/ranks/58x80/rank0_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_1:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_10:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_11:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_12:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_13:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_14:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_15:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_2:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_3:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_4:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_5:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_6:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_7:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_8:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_9:String = "../maps/icons/rankedBattles/ranks/58x80/rank1_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_1:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_10:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_11:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_12:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_13:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_14:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_15:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_2:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_3:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_4:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_5:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_6:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_7:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_8:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_9:String = "../maps/icons/rankedBattles/ranks/58x80/rank2_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_1:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_10:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_11:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_12:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_13:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_14:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_15:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_2:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_3:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_4:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_5:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_6:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_7:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_8:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_9:String = "../maps/icons/rankedBattles/ranks/58x80/rank3_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP0_1:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP0_2:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP0_3:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP1_1:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP1_2:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP1_3:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP2_1:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP2_2:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP2_3:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP3_1:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP3_2:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP3_3:String = "../maps/icons/rankedBattles/ranks/58x80/ranks_group3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKVEHMASTER:String = "../maps/icons/rankedBattles/ranks/58x80/rankVehMaster.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKVEHMASTER_GREY:String = "../maps/icons/rankedBattles/ranks/58x80/rankVehMaster_grey.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_1:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_10:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_11:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_12:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_13:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_14:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_15:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_2:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_3:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_4:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_5:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_6:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_7:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_8:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_9:String = "../maps/icons/rankedBattles/ranks/80x110/rank0_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_1:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_10:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_11:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_12:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_13:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_14:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_15:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_2:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_3:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_4:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_5:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_6:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_7:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_8:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_9:String = "../maps/icons/rankedBattles/ranks/80x110/rank1_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_1:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_10:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_11:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_12:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_13:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_14:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_15:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_2:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_3:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_4:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_5:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_6:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_7:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_8:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_9:String = "../maps/icons/rankedBattles/ranks/80x110/rank2_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_1:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_10:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_11:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_11.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_12:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_12.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_13:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_13.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_14:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_14.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_15:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_15.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_2:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_3:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_4:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_5:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_6:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_7:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_8:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_9:String = "../maps/icons/rankedBattles/ranks/80x110/rank3_9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP0_1:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group0_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP0_2:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group0_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP0_3:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group0_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP1_1:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group1_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP1_2:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group1_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP1_3:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group1_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP2_1:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group2_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP2_2:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group2_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP2_3:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group2_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP3_1:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group3_1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP3_2:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group3_2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP3_3:String = "../maps/icons/rankedBattles/ranks/80x110/ranks_group3_3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKVEHMASTER:String = "../maps/icons/rankedBattles/ranks/80x110/rankVehMaster.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_BLINDMINUS1:String = "../maps/icons/rankedBattles/ranks/miniStage/blindMinus1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_MINUS1:String = "../maps/icons/rankedBattles/ranks/miniStage/minus1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_NOTHING:String = "../maps/icons/rankedBattles/ranks/miniStage/nothing.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_PLUS1:String = "../maps/icons/rankedBattles/ranks/miniStage/plus1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_PLUS1BONUS:String = "../maps/icons/rankedBattles/ranks/miniStage/plus1Bonus.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_PLUS2:String = "../maps/icons/rankedBattles/ranks/miniStage/plus2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_PLUS2BONUS:String = "../maps/icons/rankedBattles/ranks/miniStage/plus2Bonus.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_MINISTAGE_PROTECTED:String = "../maps/icons/rankedBattles/ranks/miniStage/protected.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_BIG:String = "../maps/icons/rankedBattles/ranks/shields/big.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_HUGE:String = "../maps/icons/rankedBattles/ranks/shields/huge.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_HUGE_BOTTOM:String = "../maps/icons/rankedBattles/ranks/shields/huge_bottom.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_HUGE_LEFT:String = "../maps/icons/rankedBattles/ranks/shields/huge_left.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_HUGE_RIGHT:String = "../maps/icons/rankedBattles/ranks/shields/huge_right.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_HUGE_TOP:String = "../maps/icons/rankedBattles/ranks/shields/huge_top.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_MEDIUM:String = "../maps/icons/rankedBattles/ranks/shields/medium.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_MEDIUM_BOTTOM:String = "../maps/icons/rankedBattles/ranks/shields/medium_bottom.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_MEDIUM_LEFT:String = "../maps/icons/rankedBattles/ranks/shields/medium_left.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_MEDIUM_RIGHT:String = "../maps/icons/rankedBattles/ranks/shields/medium_right.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_MEDIUM_TOP:String = "../maps/icons/rankedBattles/ranks/shields/medium_top.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_SMALL:String = "../maps/icons/rankedBattles/ranks/shields/small.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_1:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_10:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_2:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_3:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_4:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_5:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_6:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_7:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_8:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_BIG_9:String = "../maps/icons/rankedBattles/ranks/shields/plate/big/9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_EMPTY_BIG:String = "../maps/icons/rankedBattles/ranks/shields/plate/empty/big.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_EMPTY_HUGE:String = "../maps/icons/rankedBattles/ranks/shields/plate/empty/huge.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_EMPTY_MEDIUM:String = "../maps/icons/rankedBattles/ranks/shields/plate/empty/medium.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_EMPTY_SMALL:String = "../maps/icons/rankedBattles/ranks/shields/plate/empty/small.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_1:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_10:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_2:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_3:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_4:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_5:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_6:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_7:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_8:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_HUGE_9:String = "../maps/icons/rankedBattles/ranks/shields/plate/huge/9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_1:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_10:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_2:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_3:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_4:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_5:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_6:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_7:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_8:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_MEDIUM_9:String = "../maps/icons/rankedBattles/ranks/shields/plate/medium/9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_1:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/1.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_10:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/10.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_2:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/2.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_3:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/3.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_4:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/4.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_5:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/5.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_6:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/6.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_7:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/7.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_8:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/8.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_SHIELDS_PLATE_SMALL_9:String = "../maps/icons/rankedBattles/ranks/shields/plate/small/9.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_STAGE_140X120_STAGE2_BONUS:String = "../maps/icons/rankedBattles/ranks/stage/140x120/stage2_bonus.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_STAGE_140X120_STAGE2_GREEN:String = "../maps/icons/rankedBattles/ranks/stage/140x120/stage2_green.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_STAGE_140X120_STAGE4_BONUS:String = "../maps/icons/rankedBattles/ranks/stage/140x120/stage4_bonus.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_STAGE_140X120_STAGE_GREEN:String = "../maps/icons/rankedBattles/ranks/stage/140x120/stage_green.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_STAGE_140X120_STAGE_GREY:String = "../maps/icons/rankedBattles/ranks/stage/140x120/stage_grey.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_STAGE_140X120_STAGE_RED:String = "../maps/icons/rankedBattles/ranks/stage/140x120/stage_red.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_UNBURNABLE_BIG:String = "../maps/icons/rankedBattles/ranks/unburnable/big.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_UNBURNABLE_HUGE:String = "../maps/icons/rankedBattles/ranks/unburnable/huge.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_UNBURNABLE_MEDIUM:String = "../maps/icons/rankedBattles/ranks/unburnable/medium.png";

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_UNBURNABLE_SMALL:String = "../maps/icons/rankedBattles/ranks/unburnable/small.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_42_EFFICIENCY:String = "../maps/icons/rankedBattles/stats/42/efficiency.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_42_POSITION:String = "../maps/icons/rankedBattles/stats/42/position.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_52_BATTLES:String = "../maps/icons/rankedBattles/stats/52/battles.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_52_BATTLESTOTAL:String = "../maps/icons/rankedBattles/stats/52/battlesTotal.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_52_DIVISIONEFFICIENCY:String = "../maps/icons/rankedBattles/stats/52/divisionEfficiency.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_52_EFFICIENCY:String = "../maps/icons/rankedBattles/stats/52/efficiency.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_52_STRIPES:String = "../maps/icons/rankedBattles/stats/52/stripes.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_52_STRIPESTOTAL:String = "../maps/icons/rankedBattles/stats/52/stripesTotal.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_64_EFFICIENCY:String = "../maps/icons/rankedBattles/stats/64/efficiency.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_64_POSITION:String = "../maps/icons/rankedBattles/stats/64/position.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_64_STEPS:String = "../maps/icons/rankedBattles/stats/64/steps.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_70_BATTLES:String = "../maps/icons/rankedBattles/stats/70/battles.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_70_BATTLESTOTAL:String = "../maps/icons/rankedBattles/stats/70/battlesTotal.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_70_DIVISIONEFFICIENCY:String = "../maps/icons/rankedBattles/stats/70/divisionEfficiency.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_70_EFFICIENCY:String = "../maps/icons/rankedBattles/stats/70/efficiency.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_70_STRIPES:String = "../maps/icons/rankedBattles/stats/70/stripes.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_70_STRIPESTOTAL:String = "../maps/icons/rankedBattles/stats/70/stripesTotal.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_84_EFFICIENCY:String = "../maps/icons/rankedBattles/stats/84/efficiency.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_84_POSITION:String = "../maps/icons/rankedBattles/stats/84/position.png";

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_84_STEPS:String = "../maps/icons/rankedBattles/stats/84/steps.png";

        public static const MAPS_ICONS_REFERRAL_AWARDBACK:String = "../maps/icons/referral/awardBack.png";

        public static const MAPS_ICONS_REFERRAL_AWARD_CREDITS:String = "../maps/icons/referral/award_credits.png";

        public static const MAPS_ICONS_REFERRAL_AWARD_CREDITS_GLOW:String = "../maps/icons/referral/award_credits_glow.png";

        public static const MAPS_ICONS_REFERRAL_BG_IMAGE:String = "../maps/icons/referral/bg_image.png";

        public static const MAPS_ICONS_REFERRAL_REFERRALHAND:String = "../maps/icons/referral/referralHand.png";

        public static const MAPS_ICONS_REFERRAL_REFERRALSMALLHAND:String = "../maps/icons/referral/referralSmallHand.png";

        public static const MAPS_ICONS_REFERRAL_REFSYS_MEN_BW:String = "../maps/icons/referral/refSys_men_bw.png";

        public static const MAPS_ICONS_REFERRAL_TANKMANMALE:String = "../maps/icons/referral/tankmanMale.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_AWARD_BG:String = "../maps/icons/seniorityAwards/award_bg.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_AWARD_RIBBON:String = "../maps/icons/seniorityAwards/award_ribbon.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_AWARD_RIBBON_SMALL:String = "../maps/icons/seniorityAwards/award_ribbon_small.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_CONFETTI:String = "../maps/icons/seniorityAwards/confetti.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_CONFETTI_SMALL:String = "../maps/icons/seniorityAwards/confetti_small.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_GLOW_BUTTON:String = "../maps/icons/seniorityAwards/glow_button.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_GLOW_SMALL_BUTTON:String = "../maps/icons/seniorityAwards/glow_small_button.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_ICONVIDEOOFF:String = "../maps/icons/seniorityAwards/iconVideoOff.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_REWARD_SHINE:String = "../maps/icons/seniorityAwards/reward_shine.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_RIBBON_SHINE:String = "../maps/icons/seniorityAwards/ribbon_shine.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_RIBBON_SHINE_BG:String = "../maps/icons/seniorityAwards/ribbon_shine_bg.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_SPARKS:String = "../maps/icons/seniorityAwards/sparks.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_REWARDS_BIG:String = "../maps/icons/seniorityAwards/rewards/big.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_REWARDS_BIG_HIDDEN:String = "../maps/icons/seniorityAwards/rewards/big_hidden.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_REWARDS_BIG_WITH_SHINE:String = "../maps/icons/seniorityAwards/rewards/big_with_shine.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_REWARDS_SMALL:String = "../maps/icons/seniorityAwards/rewards/small.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_REWARDS_SMALL_HIDDEN:String = "../maps/icons/seniorityAwards/rewards/small_hidden.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_REWARDS_SMALL_WITH_SHINE:String = "../maps/icons/seniorityAwards/rewards/small_with_shine.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_VEHICLES_390X245_A130_SUPER_HELLCAT:String = "../maps/icons/seniorityAwards/vehicles/390x245/A130_Super_Hellcat.png";

        public static const MAPS_ICONS_SENIORITYAWARDS_VEHICLES_390X245_R160_T_50_2:String = "../maps/icons/seniorityAwards/vehicles/390x245/R160_T_50_2.png";

        public static const MAPS_ICONS_SETTINGS_ACOUSTICS:String = "../maps/icons/settings/acoustics.png";

        public static const MAPS_ICONS_SETTINGS_COMMANDER_SIXTHSENSE:String = "../maps/icons/settings/commander_sixthSense.png";

        public static const MAPS_ICONS_SETTINGS_CREW:String = "../maps/icons/settings/crew.png";

        public static const MAPS_ICONS_SETTINGS_DEFENDBASETOOLTIP:String = "../maps/icons/settings/defendBaseTooltip.png";

        public static const MAPS_ICONS_SETTINGS_DUAL_GUN:String = "../maps/icons/settings/dual_gun.png";

        public static const MAPS_ICONS_SETTINGS_FOLLOWMETOOLTIP:String = "../maps/icons/settings/followMeTooltip.png";

        public static const MAPS_ICONS_SETTINGS_HEADPHONES:String = "../maps/icons/settings/headphones.png";

        public static const MAPS_ICONS_SETTINGS_LAPTOP:String = "../maps/icons/settings/laptop.png";

        public static const MAPS_ICONS_SETTINGS_LMB:String = "../maps/icons/settings/lmb.png";

        public static const MAPS_ICONS_SETTINGS_MINIMAPCIRCLESTOOLTIP:String = "../maps/icons/settings/minimapCirclesTooltip.png";

        public static const MAPS_ICONS_SETTINGS_NEEDHELPTOOLTIP:String = "../maps/icons/settings/needHelpTooltip.png";

        public static const MAPS_ICONS_SETTINGS_RESETTOOLTIP:String = "../maps/icons/settings/resetTooltip.png";

        public static const MAPS_ICONS_SETTINGS_STOPICONTOOLTIP:String = "../maps/icons/settings/stopIconTooltip.png";

        public static const MAPS_ICONS_SETTINGS_SUPPORTTOOLTIP:String = "../maps/icons/settings/supportTooltip.png";

        public static const MAPS_ICONS_SETTINGS_TURNBACKTOOLTIP:String = "../maps/icons/settings/turnBackTooltip.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_DEFAULT:String = "../maps/icons/settings/colorSettings/default.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERS:String = "../maps/icons/settings/colorSettings/filters.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_MANUAL:String = "../maps/icons/settings/colorSettings/manual.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_COLORIZATION:String = "../maps/icons/settings/colorSettings/filterTypes/Colorization.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_CONTRASTY:String = "../maps/icons/settings/colorSettings/filterTypes/Contrasty.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_MUTED:String = "../maps/icons/settings/colorSettings/filterTypes/Muted.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_NONE:String = "../maps/icons/settings/colorSettings/filterTypes/NONE.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_PHOTOCHROMATIC:String = "../maps/icons/settings/colorSettings/filterTypes/Photochromatic.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_RANDOM:String = "../maps/icons/settings/colorSettings/filterTypes/RANDOM.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_SOFT:String = "../maps/icons/settings/colorSettings/filterTypes/Soft.png";

        public static const MAPS_ICONS_SETTINGS_COLORSETTINGS_FILTERTYPES_SOFT_COLORS:String = "../maps/icons/settings/colorSettings/filterTypes/Soft_Colors.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_COLORIZATION:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/Colorization.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_CONTRASTY:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/Contrasty.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_MUTED:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/Muted.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_NONE:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/NONE.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_PHOTOCHROMATIC:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/Photochromatic.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_RANDOM:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/RANDOM.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_SOFT:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/Soft.png";

        public static const MAPS_ICONS_SETTINGS_COLOR_GRADING_TECHNIQUE_SOFT_COLORS:String = "../maps/icons/settings/COLOR_GRADING_TECHNIQUE/Soft_Colors.png";

        public static const MAPS_ICONS_TANKMEN_CREW_CREWBOOKS:String = "../maps/icons/tankmen/crew/crewBooks.png";

        public static const MAPS_ICONS_TANKMEN_CREW_CREWOPERATIONS:String = "../maps/icons/tankmen/crew/crewOperations.png";

        public static const MAPS_ICONS_TANKMEN_CREW_DROPINBARRACK:String = "../maps/icons/tankmen/crew/dropInBarrack.png";

        public static const MAPS_ICONS_TANKMEN_CREW_RETRAIN:String = "../maps/icons/tankmen/crew/retrain.png";

        public static const MAPS_ICONS_TANKMEN_CREW_RETURN:String = "../maps/icons/tankmen/crew/return.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_CHINA:String = "../maps/icons/tankmen/crewSkins/nations/china.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_CZECH:String = "../maps/icons/tankmen/crewSkins/nations/czech.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_FRANCE:String = "../maps/icons/tankmen/crewSkins/nations/france.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_GERMANY:String = "../maps/icons/tankmen/crewSkins/nations/germany.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_ITALY:String = "../maps/icons/tankmen/crewSkins/nations/italy.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_JAPAN:String = "../maps/icons/tankmen/crewSkins/nations/japan.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_POLAND:String = "../maps/icons/tankmen/crewSkins/nations/poland.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_SWEDEN:String = "../maps/icons/tankmen/crewSkins/nations/sweden.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_UK:String = "../maps/icons/tankmen/crewSkins/nations/uk.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_USA:String = "../maps/icons/tankmen/crewSkins/nations/usa.png";

        public static const MAPS_ICONS_TANKMEN_CREWSKINS_NATIONS_USSR:String = "../maps/icons/tankmen/crewSkins/nations/ussr.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_ASIA:String = "../maps/icons/tankmen/icons/barracks/bob20_commander1_asia.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_EU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander1_eu.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_NA:String = "../maps/icons/tankmen/icons/barracks/bob20_commander1_na.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_RU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander1_ru.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_ASIA:String = "../maps/icons/tankmen/icons/barracks/bob20_commander2_asia.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_EU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander2_eu.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_NA:String = "../maps/icons/tankmen/icons/barracks/bob20_commander2_na.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_RU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander2_ru.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER3_EU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander3_eu.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER3_RU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander3_ru.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER4_EU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander4_eu.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER4_RU:String = "../maps/icons/tankmen/icons/barracks/bob20_commander4_ru.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_1:String = "../maps/icons/tankmen/icons/barracks/china-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_10:String = "../maps/icons/tankmen/icons/barracks/china-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_11:String = "../maps/icons/tankmen/icons/barracks/china-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_12:String = "../maps/icons/tankmen/icons/barracks/china-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_13:String = "../maps/icons/tankmen/icons/barracks/china-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_14:String = "../maps/icons/tankmen/icons/barracks/china-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_15:String = "../maps/icons/tankmen/icons/barracks/china-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_16:String = "../maps/icons/tankmen/icons/barracks/china-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_17:String = "../maps/icons/tankmen/icons/barracks/china-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_18:String = "../maps/icons/tankmen/icons/barracks/china-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_19:String = "../maps/icons/tankmen/icons/barracks/china-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_2:String = "../maps/icons/tankmen/icons/barracks/china-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_20:String = "../maps/icons/tankmen/icons/barracks/china-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_21:String = "../maps/icons/tankmen/icons/barracks/china-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_22:String = "../maps/icons/tankmen/icons/barracks/china-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_23:String = "../maps/icons/tankmen/icons/barracks/china-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_3:String = "../maps/icons/tankmen/icons/barracks/china-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_4:String = "../maps/icons/tankmen/icons/barracks/china-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_5:String = "../maps/icons/tankmen/icons/barracks/china-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_6:String = "../maps/icons/tankmen/icons/barracks/china-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_7:String = "../maps/icons/tankmen/icons/barracks/china-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_8:String = "../maps/icons/tankmen/icons/barracks/china-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_9:String = "../maps/icons/tankmen/icons/barracks/china-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/china-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/china-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/china-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/china-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/china-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/china-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/china-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/china-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/china-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/china-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/china-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/china-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/china-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_COMMANDER_MARINA:String = "../maps/icons/tankmen/icons/barracks/commander_marina.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_1:String = "../maps/icons/tankmen/icons/barracks/czech-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_10:String = "../maps/icons/tankmen/icons/barracks/czech-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_11:String = "../maps/icons/tankmen/icons/barracks/czech-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_12:String = "../maps/icons/tankmen/icons/barracks/czech-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_13:String = "../maps/icons/tankmen/icons/barracks/czech-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_14:String = "../maps/icons/tankmen/icons/barracks/czech-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_15:String = "../maps/icons/tankmen/icons/barracks/czech-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_16:String = "../maps/icons/tankmen/icons/barracks/czech-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_17:String = "../maps/icons/tankmen/icons/barracks/czech-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_18:String = "../maps/icons/tankmen/icons/barracks/czech-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_19:String = "../maps/icons/tankmen/icons/barracks/czech-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_2:String = "../maps/icons/tankmen/icons/barracks/czech-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_20:String = "../maps/icons/tankmen/icons/barracks/czech-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_21:String = "../maps/icons/tankmen/icons/barracks/czech-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_22:String = "../maps/icons/tankmen/icons/barracks/czech-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_23:String = "../maps/icons/tankmen/icons/barracks/czech-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_24:String = "../maps/icons/tankmen/icons/barracks/czech-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_25:String = "../maps/icons/tankmen/icons/barracks/czech-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_26:String = "../maps/icons/tankmen/icons/barracks/czech-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_27:String = "../maps/icons/tankmen/icons/barracks/czech-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_28:String = "../maps/icons/tankmen/icons/barracks/czech-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_29:String = "../maps/icons/tankmen/icons/barracks/czech-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_3:String = "../maps/icons/tankmen/icons/barracks/czech-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_30:String = "../maps/icons/tankmen/icons/barracks/czech-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_31:String = "../maps/icons/tankmen/icons/barracks/czech-31.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_4:String = "../maps/icons/tankmen/icons/barracks/czech-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_5:String = "../maps/icons/tankmen/icons/barracks/czech-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_6:String = "../maps/icons/tankmen/icons/barracks/czech-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_7:String = "../maps/icons/tankmen/icons/barracks/czech-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_8:String = "../maps/icons/tankmen/icons/barracks/czech-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_9:String = "../maps/icons/tankmen/icons/barracks/czech-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/czech-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/czech-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/czech-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/czech-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/czech-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/czech-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/czech-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/czech-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/czech-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/czech-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/czech-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/czech-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_1:String = "../maps/icons/tankmen/icons/barracks/france-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_10:String = "../maps/icons/tankmen/icons/barracks/france-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_11:String = "../maps/icons/tankmen/icons/barracks/france-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_12:String = "../maps/icons/tankmen/icons/barracks/france-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_13:String = "../maps/icons/tankmen/icons/barracks/france-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_14:String = "../maps/icons/tankmen/icons/barracks/france-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_15:String = "../maps/icons/tankmen/icons/barracks/france-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_16:String = "../maps/icons/tankmen/icons/barracks/france-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_17:String = "../maps/icons/tankmen/icons/barracks/france-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_18:String = "../maps/icons/tankmen/icons/barracks/france-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_19:String = "../maps/icons/tankmen/icons/barracks/france-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_2:String = "../maps/icons/tankmen/icons/barracks/france-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_20:String = "../maps/icons/tankmen/icons/barracks/france-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_21:String = "../maps/icons/tankmen/icons/barracks/france-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_22:String = "../maps/icons/tankmen/icons/barracks/france-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_23:String = "../maps/icons/tankmen/icons/barracks/france-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_24:String = "../maps/icons/tankmen/icons/barracks/france-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_25:String = "../maps/icons/tankmen/icons/barracks/france-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_26:String = "../maps/icons/tankmen/icons/barracks/france-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_27:String = "../maps/icons/tankmen/icons/barracks/france-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_28:String = "../maps/icons/tankmen/icons/barracks/france-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_29:String = "../maps/icons/tankmen/icons/barracks/france-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_3:String = "../maps/icons/tankmen/icons/barracks/france-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_30:String = "../maps/icons/tankmen/icons/barracks/france-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_31:String = "../maps/icons/tankmen/icons/barracks/france-31.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_32:String = "../maps/icons/tankmen/icons/barracks/france-32.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_33:String = "../maps/icons/tankmen/icons/barracks/france-33.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_34:String = "../maps/icons/tankmen/icons/barracks/france-34.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_4:String = "../maps/icons/tankmen/icons/barracks/france-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_5:String = "../maps/icons/tankmen/icons/barracks/france-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_6:String = "../maps/icons/tankmen/icons/barracks/france-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_7:String = "../maps/icons/tankmen/icons/barracks/france-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_8:String = "../maps/icons/tankmen/icons/barracks/france-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_9:String = "../maps/icons/tankmen/icons/barracks/france-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/france-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/france-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/france-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/france-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/france-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_14:String = "../maps/icons/tankmen/icons/barracks/france-female-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_15:String = "../maps/icons/tankmen/icons/barracks/france-female-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_16:String = "../maps/icons/tankmen/icons/barracks/france-female-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_17:String = "../maps/icons/tankmen/icons/barracks/france-female-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_18:String = "../maps/icons/tankmen/icons/barracks/france-female-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/france-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/france-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/france-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/france-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/france-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/france-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/france-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/france-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_1:String = "../maps/icons/tankmen/icons/barracks/germany-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_10:String = "../maps/icons/tankmen/icons/barracks/germany-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_11:String = "../maps/icons/tankmen/icons/barracks/germany-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_12:String = "../maps/icons/tankmen/icons/barracks/germany-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_13:String = "../maps/icons/tankmen/icons/barracks/germany-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_14:String = "../maps/icons/tankmen/icons/barracks/germany-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_15:String = "../maps/icons/tankmen/icons/barracks/germany-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_16:String = "../maps/icons/tankmen/icons/barracks/germany-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_17:String = "../maps/icons/tankmen/icons/barracks/germany-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_18:String = "../maps/icons/tankmen/icons/barracks/germany-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_19:String = "../maps/icons/tankmen/icons/barracks/germany-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_2:String = "../maps/icons/tankmen/icons/barracks/germany-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_20:String = "../maps/icons/tankmen/icons/barracks/germany-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_21:String = "../maps/icons/tankmen/icons/barracks/germany-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_22:String = "../maps/icons/tankmen/icons/barracks/germany-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_23:String = "../maps/icons/tankmen/icons/barracks/germany-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_24:String = "../maps/icons/tankmen/icons/barracks/germany-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_25:String = "../maps/icons/tankmen/icons/barracks/germany-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_26:String = "../maps/icons/tankmen/icons/barracks/germany-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_27:String = "../maps/icons/tankmen/icons/barracks/germany-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_28:String = "../maps/icons/tankmen/icons/barracks/germany-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_29:String = "../maps/icons/tankmen/icons/barracks/germany-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_3:String = "../maps/icons/tankmen/icons/barracks/germany-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_30:String = "../maps/icons/tankmen/icons/barracks/germany-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_31:String = "../maps/icons/tankmen/icons/barracks/germany-31.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_32:String = "../maps/icons/tankmen/icons/barracks/germany-32.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_4:String = "../maps/icons/tankmen/icons/barracks/germany-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_5:String = "../maps/icons/tankmen/icons/barracks/germany-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_6:String = "../maps/icons/tankmen/icons/barracks/germany-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_7:String = "../maps/icons/tankmen/icons/barracks/germany-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_8:String = "../maps/icons/tankmen/icons/barracks/germany-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_9:String = "../maps/icons/tankmen/icons/barracks/germany-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/germany-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/germany-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/germany-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/germany-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/germany-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_14:String = "../maps/icons/tankmen/icons/barracks/germany-female-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_15:String = "../maps/icons/tankmen/icons/barracks/germany-female-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/germany-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/germany-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/germany-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/germany-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/germany-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/germany-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/germany-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/germany-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GIRL_EMPTY:String = "../maps/icons/tankmen/icons/barracks/girl-empty.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_1:String = "../maps/icons/tankmen/icons/barracks/italy-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_10:String = "../maps/icons/tankmen/icons/barracks/italy-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_11:String = "../maps/icons/tankmen/icons/barracks/italy-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_12:String = "../maps/icons/tankmen/icons/barracks/italy-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_13:String = "../maps/icons/tankmen/icons/barracks/italy-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_14:String = "../maps/icons/tankmen/icons/barracks/italy-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_15:String = "../maps/icons/tankmen/icons/barracks/italy-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_16:String = "../maps/icons/tankmen/icons/barracks/italy-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_17:String = "../maps/icons/tankmen/icons/barracks/italy-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_18:String = "../maps/icons/tankmen/icons/barracks/italy-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_19:String = "../maps/icons/tankmen/icons/barracks/italy-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_2:String = "../maps/icons/tankmen/icons/barracks/italy-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_20:String = "../maps/icons/tankmen/icons/barracks/italy-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_21:String = "../maps/icons/tankmen/icons/barracks/italy-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_22:String = "../maps/icons/tankmen/icons/barracks/italy-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_3:String = "../maps/icons/tankmen/icons/barracks/italy-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_4:String = "../maps/icons/tankmen/icons/barracks/italy-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_5:String = "../maps/icons/tankmen/icons/barracks/italy-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_6:String = "../maps/icons/tankmen/icons/barracks/italy-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_7:String = "../maps/icons/tankmen/icons/barracks/italy-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_8:String = "../maps/icons/tankmen/icons/barracks/italy-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_9:String = "../maps/icons/tankmen/icons/barracks/italy-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_BUFFON:String = "../maps/icons/tankmen/icons/barracks/italy-Buffon.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/italy-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/italy-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/italy-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/italy-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/italy-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_14:String = "../maps/icons/tankmen/icons/barracks/italy-female-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_15:String = "../maps/icons/tankmen/icons/barracks/italy-female-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_16:String = "../maps/icons/tankmen/icons/barracks/italy-female-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_17:String = "../maps/icons/tankmen/icons/barracks/italy-female-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_18:String = "../maps/icons/tankmen/icons/barracks/italy-female-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_19:String = "../maps/icons/tankmen/icons/barracks/italy-female-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/italy-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_20:String = "../maps/icons/tankmen/icons/barracks/italy-female-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_21:String = "../maps/icons/tankmen/icons/barracks/italy-female-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_22:String = "../maps/icons/tankmen/icons/barracks/italy-female-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_23:String = "../maps/icons/tankmen/icons/barracks/italy-female-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/italy-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/italy-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/italy-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/italy-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/italy-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/italy-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/italy-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_1:String = "../maps/icons/tankmen/icons/barracks/japan-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_10:String = "../maps/icons/tankmen/icons/barracks/japan-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_11:String = "../maps/icons/tankmen/icons/barracks/japan-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_12:String = "../maps/icons/tankmen/icons/barracks/japan-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_13:String = "../maps/icons/tankmen/icons/barracks/japan-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_14:String = "../maps/icons/tankmen/icons/barracks/japan-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_15:String = "../maps/icons/tankmen/icons/barracks/japan-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_16:String = "../maps/icons/tankmen/icons/barracks/japan-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_17:String = "../maps/icons/tankmen/icons/barracks/japan-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_18:String = "../maps/icons/tankmen/icons/barracks/japan-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_19:String = "../maps/icons/tankmen/icons/barracks/japan-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_2:String = "../maps/icons/tankmen/icons/barracks/japan-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_20:String = "../maps/icons/tankmen/icons/barracks/japan-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_21:String = "../maps/icons/tankmen/icons/barracks/japan-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_22:String = "../maps/icons/tankmen/icons/barracks/japan-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_23:String = "../maps/icons/tankmen/icons/barracks/japan-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_24:String = "../maps/icons/tankmen/icons/barracks/japan-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_25:String = "../maps/icons/tankmen/icons/barracks/japan-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_26:String = "../maps/icons/tankmen/icons/barracks/japan-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_27:String = "../maps/icons/tankmen/icons/barracks/japan-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_28:String = "../maps/icons/tankmen/icons/barracks/japan-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_29:String = "../maps/icons/tankmen/icons/barracks/japan-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_3:String = "../maps/icons/tankmen/icons/barracks/japan-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_30:String = "../maps/icons/tankmen/icons/barracks/japan-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_4:String = "../maps/icons/tankmen/icons/barracks/japan-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_5:String = "../maps/icons/tankmen/icons/barracks/japan-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_6:String = "../maps/icons/tankmen/icons/barracks/japan-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_7:String = "../maps/icons/tankmen/icons/barracks/japan-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_8:String = "../maps/icons/tankmen/icons/barracks/japan-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_9:String = "../maps/icons/tankmen/icons/barracks/japan-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_CARISA_CONTZEN:String = "../maps/icons/tankmen/icons/barracks/japan-Carisa_Contzen.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/japan-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/japan-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/japan-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/japan-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/japan-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/japan-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/japan-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/japan-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/japan-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/japan-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/japan-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/japan-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_GUNNER:String = "../maps/icons/tankmen/icons/barracks/japan-Gunner.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_GUNNER_1:String = "../maps/icons/tankmen/icons/barracks/japan-Gunner_1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_GUSURG:String = "../maps/icons/tankmen/icons/barracks/japan-Gusurg.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_ISARA_GUNTHER:String = "../maps/icons/tankmen/icons/barracks/japan-Isara_Gunther.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_KREIS_CZHERNY:String = "../maps/icons/tankmen/icons/barracks/japan-Kreis_Czherny.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_LOADER:String = "../maps/icons/tankmen/icons/barracks/japan-Loader.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_LOADER_2:String = "../maps/icons/tankmen/icons/barracks/japan-Loader_2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_RADIO_OPERATOR:String = "../maps/icons/tankmen/icons/barracks/japan-Radio_operator.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_WELKIN_GUNTHER:String = "../maps/icons/tankmen/icons/barracks/japan-Welkin_Gunther.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_MEN:String = "../maps/icons/tankmen/icons/barracks/ny19_men.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L10:String = "../maps/icons/tankmen/icons/barracks/ny19_woman_L10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L4:String = "../maps/icons/tankmen/icons/barracks/ny19_woman_L4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L6:String = "../maps/icons/tankmen/icons/barracks/ny19_woman_L6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L8:String = "../maps/icons/tankmen/icons/barracks/ny19_woman_L8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_CHRISTMAS:String = "../maps/icons/tankmen/icons/barracks/ny20_girl_Christmas.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_FAIRYTALE:String = "../maps/icons/tankmen/icons/barracks/ny20_girl_Fairytale.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_NEWYEAR:String = "../maps/icons/tankmen/icons/barracks/ny20_girl_NewYear.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_ORIENTAL:String = "../maps/icons/tankmen/icons/barracks/ny20_girl_Oriental.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_MEN:String = "../maps/icons/tankmen/icons/barracks/ny20_men.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_SNOWMEN:String = "../maps/icons/tankmen/icons/barracks/ny20_snowmen.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_1:String = "../maps/icons/tankmen/icons/barracks/poland-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_10:String = "../maps/icons/tankmen/icons/barracks/poland-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_11:String = "../maps/icons/tankmen/icons/barracks/poland-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_12:String = "../maps/icons/tankmen/icons/barracks/poland-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_13:String = "../maps/icons/tankmen/icons/barracks/poland-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_14:String = "../maps/icons/tankmen/icons/barracks/poland-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_15:String = "../maps/icons/tankmen/icons/barracks/poland-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_16:String = "../maps/icons/tankmen/icons/barracks/poland-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_17:String = "../maps/icons/tankmen/icons/barracks/poland-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_18:String = "../maps/icons/tankmen/icons/barracks/poland-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_19:String = "../maps/icons/tankmen/icons/barracks/poland-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_2:String = "../maps/icons/tankmen/icons/barracks/poland-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_20:String = "../maps/icons/tankmen/icons/barracks/poland-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_21_KOS:String = "../maps/icons/tankmen/icons/barracks/poland-21-Kos.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_22_CZERESNIAK:String = "../maps/icons/tankmen/icons/barracks/poland-22-Czeresniak.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_23_SAAKASZWILI:String = "../maps/icons/tankmen/icons/barracks/poland-23-Saakaszwili.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_24_JELEN:String = "../maps/icons/tankmen/icons/barracks/poland-24-Jelen.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_3:String = "../maps/icons/tankmen/icons/barracks/poland-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_4:String = "../maps/icons/tankmen/icons/barracks/poland-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_5:String = "../maps/icons/tankmen/icons/barracks/poland-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_6:String = "../maps/icons/tankmen/icons/barracks/poland-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_7:String = "../maps/icons/tankmen/icons/barracks/poland-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_8:String = "../maps/icons/tankmen/icons/barracks/poland-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_9:String = "../maps/icons/tankmen/icons/barracks/poland-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/poland-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/poland-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/poland-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/poland-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/poland-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/poland-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/poland-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/poland-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/poland-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/poland-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/poland-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/poland-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_RACE19_COMMANDER_A:String = "../maps/icons/tankmen/icons/barracks/race19-commander-a.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_RACE19_COMMANDER_B:String = "../maps/icons/tankmen/icons/barracks/race19-commander-b.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_RACE19_COMMANDER_C:String = "../maps/icons/tankmen/icons/barracks/race19-commander-c.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_1:String = "../maps/icons/tankmen/icons/barracks/sweden-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_10:String = "../maps/icons/tankmen/icons/barracks/sweden-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_11:String = "../maps/icons/tankmen/icons/barracks/sweden-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_12:String = "../maps/icons/tankmen/icons/barracks/sweden-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_13:String = "../maps/icons/tankmen/icons/barracks/sweden-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_14:String = "../maps/icons/tankmen/icons/barracks/sweden-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_15:String = "../maps/icons/tankmen/icons/barracks/sweden-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_16:String = "../maps/icons/tankmen/icons/barracks/sweden-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_17:String = "../maps/icons/tankmen/icons/barracks/sweden-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_18:String = "../maps/icons/tankmen/icons/barracks/sweden-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_19:String = "../maps/icons/tankmen/icons/barracks/sweden-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_2:String = "../maps/icons/tankmen/icons/barracks/sweden-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_20:String = "../maps/icons/tankmen/icons/barracks/sweden-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_21:String = "../maps/icons/tankmen/icons/barracks/sweden-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_22:String = "../maps/icons/tankmen/icons/barracks/sweden-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_23:String = "../maps/icons/tankmen/icons/barracks/sweden-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_24:String = "../maps/icons/tankmen/icons/barracks/sweden-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_25:String = "../maps/icons/tankmen/icons/barracks/sweden-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_26:String = "../maps/icons/tankmen/icons/barracks/sweden-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_27:String = "../maps/icons/tankmen/icons/barracks/sweden-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_28:String = "../maps/icons/tankmen/icons/barracks/sweden-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_29:String = "../maps/icons/tankmen/icons/barracks/sweden-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_3:String = "../maps/icons/tankmen/icons/barracks/sweden-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_30:String = "../maps/icons/tankmen/icons/barracks/sweden-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_31:String = "../maps/icons/tankmen/icons/barracks/sweden-31.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_32:String = "../maps/icons/tankmen/icons/barracks/sweden-32.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_33:String = "../maps/icons/tankmen/icons/barracks/sweden-33.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_34:String = "../maps/icons/tankmen/icons/barracks/sweden-34.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_35:String = "../maps/icons/tankmen/icons/barracks/sweden-35.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_36:String = "../maps/icons/tankmen/icons/barracks/sweden-36.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_37:String = "../maps/icons/tankmen/icons/barracks/sweden-37.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_4:String = "../maps/icons/tankmen/icons/barracks/sweden-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_5:String = "../maps/icons/tankmen/icons/barracks/sweden-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_6:String = "../maps/icons/tankmen/icons/barracks/sweden-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_7:String = "../maps/icons/tankmen/icons/barracks/sweden-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_8:String = "../maps/icons/tankmen/icons/barracks/sweden-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_9:String = "../maps/icons/tankmen/icons/barracks/sweden-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/sweden-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/sweden-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/sweden-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/sweden-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/sweden-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_14:String = "../maps/icons/tankmen/icons/barracks/sweden-female-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_15:String = "../maps/icons/tankmen/icons/barracks/sweden-female-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_16:String = "../maps/icons/tankmen/icons/barracks/sweden-female-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/sweden-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/sweden-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/sweden-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/sweden-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/sweden-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/sweden-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/sweden-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/sweden-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TANKMAN:String = "../maps/icons/tankmen/icons/barracks/tankman.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH6_PUMPKIN_TWITCH:String = "../maps/icons/tankmen/icons/barracks/twitch6_pumpkin_twitch.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH7:String = "../maps/icons/tankmen/icons/barracks/twitch7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH8:String = "../maps/icons/tankmen/icons/barracks/twitch8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH9:String = "../maps/icons/tankmen/icons/barracks/twitch9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH_GIRL:String = "../maps/icons/tankmen/icons/barracks/twitch_girl.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH_GUY:String = "../maps/icons/tankmen/icons/barracks/twitch_guy.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_1:String = "../maps/icons/tankmen/icons/barracks/uk-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_10:String = "../maps/icons/tankmen/icons/barracks/uk-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_11:String = "../maps/icons/tankmen/icons/barracks/uk-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_12:String = "../maps/icons/tankmen/icons/barracks/uk-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_13:String = "../maps/icons/tankmen/icons/barracks/uk-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_14:String = "../maps/icons/tankmen/icons/barracks/uk-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_15:String = "../maps/icons/tankmen/icons/barracks/uk-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_16:String = "../maps/icons/tankmen/icons/barracks/uk-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_17:String = "../maps/icons/tankmen/icons/barracks/uk-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_18:String = "../maps/icons/tankmen/icons/barracks/uk-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_19:String = "../maps/icons/tankmen/icons/barracks/uk-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_2:String = "../maps/icons/tankmen/icons/barracks/uk-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_20:String = "../maps/icons/tankmen/icons/barracks/uk-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_21:String = "../maps/icons/tankmen/icons/barracks/uk-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_22:String = "../maps/icons/tankmen/icons/barracks/uk-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_23:String = "../maps/icons/tankmen/icons/barracks/uk-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_24:String = "../maps/icons/tankmen/icons/barracks/uk-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_25:String = "../maps/icons/tankmen/icons/barracks/uk-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_26:String = "../maps/icons/tankmen/icons/barracks/uk-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_27:String = "../maps/icons/tankmen/icons/barracks/uk-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_28:String = "../maps/icons/tankmen/icons/barracks/uk-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_29:String = "../maps/icons/tankmen/icons/barracks/uk-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_3:String = "../maps/icons/tankmen/icons/barracks/uk-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_30:String = "../maps/icons/tankmen/icons/barracks/uk-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_31:String = "../maps/icons/tankmen/icons/barracks/uk-31.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_32:String = "../maps/icons/tankmen/icons/barracks/uk-32.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_33:String = "../maps/icons/tankmen/icons/barracks/uk-33.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_34:String = "../maps/icons/tankmen/icons/barracks/uk-34.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_35:String = "../maps/icons/tankmen/icons/barracks/uk-35.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_4:String = "../maps/icons/tankmen/icons/barracks/uk-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_5:String = "../maps/icons/tankmen/icons/barracks/uk-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_6:String = "../maps/icons/tankmen/icons/barracks/uk-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_7:String = "../maps/icons/tankmen/icons/barracks/uk-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_8:String = "../maps/icons/tankmen/icons/barracks/uk-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_9:String = "../maps/icons/tankmen/icons/barracks/uk-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/uk-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/uk-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/uk-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/uk-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/uk-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_14:String = "../maps/icons/tankmen/icons/barracks/uk-female-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_15:String = "../maps/icons/tankmen/icons/barracks/uk-female-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_16:String = "../maps/icons/tankmen/icons/barracks/uk-female-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_17:String = "../maps/icons/tankmen/icons/barracks/uk-female-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_18:String = "../maps/icons/tankmen/icons/barracks/uk-female-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_19:String = "../maps/icons/tankmen/icons/barracks/uk-female-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/uk-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/uk-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/uk-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/uk-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/uk-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/uk-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/uk-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/uk-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_1:String = "../maps/icons/tankmen/icons/barracks/usa-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_10:String = "../maps/icons/tankmen/icons/barracks/usa-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_11:String = "../maps/icons/tankmen/icons/barracks/usa-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_12:String = "../maps/icons/tankmen/icons/barracks/usa-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_13:String = "../maps/icons/tankmen/icons/barracks/usa-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_14:String = "../maps/icons/tankmen/icons/barracks/usa-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_15:String = "../maps/icons/tankmen/icons/barracks/usa-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_16:String = "../maps/icons/tankmen/icons/barracks/usa-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_17:String = "../maps/icons/tankmen/icons/barracks/usa-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_18:String = "../maps/icons/tankmen/icons/barracks/usa-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_19:String = "../maps/icons/tankmen/icons/barracks/usa-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_2:String = "../maps/icons/tankmen/icons/barracks/usa-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_20:String = "../maps/icons/tankmen/icons/barracks/usa-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_21:String = "../maps/icons/tankmen/icons/barracks/usa-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_22:String = "../maps/icons/tankmen/icons/barracks/usa-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_23:String = "../maps/icons/tankmen/icons/barracks/usa-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_24:String = "../maps/icons/tankmen/icons/barracks/usa-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_25:String = "../maps/icons/tankmen/icons/barracks/usa-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_26:String = "../maps/icons/tankmen/icons/barracks/usa-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_27:String = "../maps/icons/tankmen/icons/barracks/usa-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_28:String = "../maps/icons/tankmen/icons/barracks/usa-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_29:String = "../maps/icons/tankmen/icons/barracks/usa-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_3:String = "../maps/icons/tankmen/icons/barracks/usa-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_30:String = "../maps/icons/tankmen/icons/barracks/usa-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_31:String = "../maps/icons/tankmen/icons/barracks/usa-31.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_32:String = "../maps/icons/tankmen/icons/barracks/usa-32.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_33:String = "../maps/icons/tankmen/icons/barracks/usa-33.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_34:String = "../maps/icons/tankmen/icons/barracks/usa-34.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_35:String = "../maps/icons/tankmen/icons/barracks/usa-35.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_36:String = "../maps/icons/tankmen/icons/barracks/usa-36.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_37:String = "../maps/icons/tankmen/icons/barracks/usa-37.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_38:String = "../maps/icons/tankmen/icons/barracks/usa-38.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_39:String = "../maps/icons/tankmen/icons/barracks/usa-39.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_4:String = "../maps/icons/tankmen/icons/barracks/usa-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_40:String = "../maps/icons/tankmen/icons/barracks/usa-40.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_41:String = "../maps/icons/tankmen/icons/barracks/usa-41.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_42:String = "../maps/icons/tankmen/icons/barracks/usa-42.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_43:String = "../maps/icons/tankmen/icons/barracks/usa-43.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_44:String = "../maps/icons/tankmen/icons/barracks/usa-44.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_45:String = "../maps/icons/tankmen/icons/barracks/usa-45.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_46:String = "../maps/icons/tankmen/icons/barracks/usa-46.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_47:String = "../maps/icons/tankmen/icons/barracks/usa-47.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_5:String = "../maps/icons/tankmen/icons/barracks/usa-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_6:String = "../maps/icons/tankmen/icons/barracks/usa-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_7:String = "../maps/icons/tankmen/icons/barracks/usa-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_8:String = "../maps/icons/tankmen/icons/barracks/usa-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_9:String = "../maps/icons/tankmen/icons/barracks/usa-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/usa-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/usa-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/usa-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/usa-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/usa-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_14:String = "../maps/icons/tankmen/icons/barracks/usa-female-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_15:String = "../maps/icons/tankmen/icons/barracks/usa-female-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_16:String = "../maps/icons/tankmen/icons/barracks/usa-female-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_17:String = "../maps/icons/tankmen/icons/barracks/usa-female-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_18:String = "../maps/icons/tankmen/icons/barracks/usa-female-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_19:String = "../maps/icons/tankmen/icons/barracks/usa-female-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/usa-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_20:String = "../maps/icons/tankmen/icons/barracks/usa-female-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_21:String = "../maps/icons/tankmen/icons/barracks/usa-female-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_22:String = "../maps/icons/tankmen/icons/barracks/usa-female-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_23:String = "../maps/icons/tankmen/icons/barracks/usa-female-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_24:String = "../maps/icons/tankmen/icons/barracks/usa-female-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_25:String = "../maps/icons/tankmen/icons/barracks/usa-female-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_26:String = "../maps/icons/tankmen/icons/barracks/usa-female-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_27:String = "../maps/icons/tankmen/icons/barracks/usa-female-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_28:String = "../maps/icons/tankmen/icons/barracks/usa-female-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/usa-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/usa-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/usa-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/usa-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/usa-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/usa-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/usa-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_1:String = "../maps/icons/tankmen/icons/barracks/ussr-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_10:String = "../maps/icons/tankmen/icons/barracks/ussr-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_11:String = "../maps/icons/tankmen/icons/barracks/ussr-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_12:String = "../maps/icons/tankmen/icons/barracks/ussr-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_13:String = "../maps/icons/tankmen/icons/barracks/ussr-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_14:String = "../maps/icons/tankmen/icons/barracks/ussr-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_15:String = "../maps/icons/tankmen/icons/barracks/ussr-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_16:String = "../maps/icons/tankmen/icons/barracks/ussr-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_17:String = "../maps/icons/tankmen/icons/barracks/ussr-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_18:String = "../maps/icons/tankmen/icons/barracks/ussr-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_19:String = "../maps/icons/tankmen/icons/barracks/ussr-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_2:String = "../maps/icons/tankmen/icons/barracks/ussr-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_20:String = "../maps/icons/tankmen/icons/barracks/ussr-20.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_21:String = "../maps/icons/tankmen/icons/barracks/ussr-21.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_22:String = "../maps/icons/tankmen/icons/barracks/ussr-22.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_23:String = "../maps/icons/tankmen/icons/barracks/ussr-23.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_24:String = "../maps/icons/tankmen/icons/barracks/ussr-24.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_25:String = "../maps/icons/tankmen/icons/barracks/ussr-25.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_26:String = "../maps/icons/tankmen/icons/barracks/ussr-26.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_27:String = "../maps/icons/tankmen/icons/barracks/ussr-27.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_28:String = "../maps/icons/tankmen/icons/barracks/ussr-28.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_29:String = "../maps/icons/tankmen/icons/barracks/ussr-29.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_3:String = "../maps/icons/tankmen/icons/barracks/ussr-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_30:String = "../maps/icons/tankmen/icons/barracks/ussr-30.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_31:String = "../maps/icons/tankmen/icons/barracks/ussr-31.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_32:String = "../maps/icons/tankmen/icons/barracks/ussr-32.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_33:String = "../maps/icons/tankmen/icons/barracks/ussr-33.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_34:String = "../maps/icons/tankmen/icons/barracks/ussr-34.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_4:String = "../maps/icons/tankmen/icons/barracks/ussr-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_5:String = "../maps/icons/tankmen/icons/barracks/ussr-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_6:String = "../maps/icons/tankmen/icons/barracks/ussr-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_7:String = "../maps/icons/tankmen/icons/barracks/ussr-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_8:String = "../maps/icons/tankmen/icons/barracks/ussr-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_9:String = "../maps/icons/tankmen/icons/barracks/ussr-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_1:String = "../maps/icons/tankmen/icons/barracks/ussr-female-1.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_10:String = "../maps/icons/tankmen/icons/barracks/ussr-female-10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_11:String = "../maps/icons/tankmen/icons/barracks/ussr-female-11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_12:String = "../maps/icons/tankmen/icons/barracks/ussr-female-12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_13:String = "../maps/icons/tankmen/icons/barracks/ussr-female-13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_14:String = "../maps/icons/tankmen/icons/barracks/ussr-female-14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_15:String = "../maps/icons/tankmen/icons/barracks/ussr-female-15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_16:String = "../maps/icons/tankmen/icons/barracks/ussr-female-16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_17:String = "../maps/icons/tankmen/icons/barracks/ussr-female-17.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_18:String = "../maps/icons/tankmen/icons/barracks/ussr-female-18.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_19:String = "../maps/icons/tankmen/icons/barracks/ussr-female-19.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_2:String = "../maps/icons/tankmen/icons/barracks/ussr-female-2.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_3:String = "../maps/icons/tankmen/icons/barracks/ussr-female-3.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_4:String = "../maps/icons/tankmen/icons/barracks/ussr-female-4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_5:String = "../maps/icons/tankmen/icons/barracks/ussr-female-5.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_6:String = "../maps/icons/tankmen/icons/barracks/ussr-female-6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_7:String = "../maps/icons/tankmen/icons/barracks/ussr-female-7.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_8:String = "../maps/icons/tankmen/icons/barracks/ussr-female-8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_9:String = "../maps/icons/tankmen/icons/barracks/ussr-female-9.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE01:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave01.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE02:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave02.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE03:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave03.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE04:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave04.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE05:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave05.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE06:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave06.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE07:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave07.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE08:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave08.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE09:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave09.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE10:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE11:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE12:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE13:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE14:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE15:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE16:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Brave16.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_DEGAULLE:String = "../maps/icons/tankmen/icons/barracks/crewSkins/DeGaulle.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_CRAYFISH:String = "../maps/icons/tankmen/icons/barracks/crewSkins/FoolsDay_Crayfish.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_DEER:String = "../maps/icons/tankmen/icons/barracks/crewSkins/FoolsDay_Deer.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_RNGESUS:String = "../maps/icons/tankmen/icons/barracks/crewSkins/FoolsDay_Rngesus.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_TOMATO:String = "../maps/icons/tankmen/icons/barracks/crewSkins/FoolsDay_Tomato.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_UNICORN:String = "../maps/icons/tankmen/icons/barracks/crewSkins/FoolsDay_Unicorn.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_KATUKOV:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Katukov.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_PATTON:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Patton.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE01:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race01.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE02:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race02.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE03:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race03.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE04:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race04.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE05:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race05.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE06:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race06.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE07:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race07.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE08:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race08.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE09:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race09.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE10:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE11:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race11.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE12:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race12.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE13:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race13.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE14:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race14.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE15:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Race15.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RYBALKO:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Rybalko.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE01:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Space01.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE02:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Space02.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE03:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Space03.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE04:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Space04.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE05:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Space05.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE06:String = "../maps/icons/tankmen/icons/barracks/crewSkins/Space06.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY19_MEN:String = "../maps/icons/tankmen/icons/special/ny19_men.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY19_WOMAN_L10:String = "../maps/icons/tankmen/icons/special/ny19_woman_L10.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY19_WOMAN_L4:String = "../maps/icons/tankmen/icons/special/ny19_woman_L4.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY19_WOMAN_L6:String = "../maps/icons/tankmen/icons/special/ny19_woman_L6.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY19_WOMAN_L8:String = "../maps/icons/tankmen/icons/special/ny19_woman_L8.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY20_DEFAULT_GIRL:String = "../maps/icons/tankmen/icons/special/ny20_default_girl.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY20_GIRL_CHRISTMAS:String = "../maps/icons/tankmen/icons/special/ny20_girl_Christmas.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY20_GIRL_FAIRYTALE:String = "../maps/icons/tankmen/icons/special/ny20_girl_Fairytale.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY20_GIRL_NEWYEAR:String = "../maps/icons/tankmen/icons/special/ny20_girl_NewYear.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY20_GIRL_ORIENTAL:String = "../maps/icons/tankmen/icons/special/ny20_girl_Oriental.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY20_MEN:String = "../maps/icons/tankmen/icons/special/ny20_men.png";

        public static const MAPS_ICONS_TANKMEN_ICONS_SPECIAL_NY20_SNOWMEN:String = "../maps/icons/tankmen/icons/special/ny20_snowmen.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_BROTHERHOOD:String = "../maps/icons/tankmen/skills/small/brotherhood.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_CAMOUFLAGE:String = "../maps/icons/tankmen/skills/small/camouflage.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_EAGLEEYE:String = "../maps/icons/tankmen/skills/small/commander_eagleEye.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_EXPERT:String = "../maps/icons/tankmen/skills/small/commander_expert.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_SIXTHSENSE:String = "../maps/icons/tankmen/skills/small/commander_sixthSense.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_TUTOR:String = "../maps/icons/tankmen/skills/small/commander_tutor.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_UNIVERSALIST:String = "../maps/icons/tankmen/skills/small/commander_universalist.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_BADROADSKING:String = "../maps/icons/tankmen/skills/small/driver_badRoadsKing.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_RAMMINGMASTER:String = "../maps/icons/tankmen/skills/small/driver_rammingMaster.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_SMOOTHDRIVING:String = "../maps/icons/tankmen/skills/small/driver_smoothDriving.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_TIDYPERSON:String = "../maps/icons/tankmen/skills/small/driver_tidyPerson.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_VIRTUOSO:String = "../maps/icons/tankmen/skills/small/driver_virtuoso.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_FIREFIGHTING:String = "../maps/icons/tankmen/skills/small/fireFighting.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_GUNSMITH:String = "../maps/icons/tankmen/skills/small/gunner_gunsmith.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_RANCOROUS:String = "../maps/icons/tankmen/skills/small/gunner_rancorous.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_SMOOTHTURRET:String = "../maps/icons/tankmen/skills/small/gunner_smoothTurret.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_SNIPER:String = "../maps/icons/tankmen/skills/small/gunner_sniper.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_LOADER_DESPERADO:String = "../maps/icons/tankmen/skills/small/loader_desperado.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_LOADER_INTUITION:String = "../maps/icons/tankmen/skills/small/loader_intuition.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_LOADER_PEDANT:String = "../maps/icons/tankmen/skills/small/loader_pedant.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_NEW_CURRENT_SKILL:String = "../maps/icons/tankmen/skills/small/new_current_skill.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_NEW_SKILL:String = "../maps/icons/tankmen/skills/small/new_skill.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_OFFSPRING_BROTHERHOOD:String = "../maps/icons/tankmen/skills/small/offspring_brotherhood.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_FINDER:String = "../maps/icons/tankmen/skills/small/radioman_finder.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_INVENTOR:String = "../maps/icons/tankmen/skills/small/radioman_inventor.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_LASTEFFORT:String = "../maps/icons/tankmen/skills/small/radioman_lastEffort.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_RETRANSMITTER:String = "../maps/icons/tankmen/skills/small/radioman_retransmitter.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_REPAIR:String = "../maps/icons/tankmen/skills/small/repair.png";

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_SABATON_BROTHERHOOD:String = "../maps/icons/tankmen/skills/small/sabaton_brotherhood.png";

        public static const MAPS_ICONS_TOOLTIP_ACTIVEICON:String = "../maps/icons/tooltip/activeIcon.png";

        public static const MAPS_ICONS_TOOLTIP_ALERTICON:String = "../maps/icons/tooltip/alertIcon.png";

        public static const MAPS_ICONS_TOOLTIP_ASSIST:String = "../maps/icons/tooltip/assist.png";

        public static const MAPS_ICONS_TOOLTIP_ASTERISK_OPTIONAL:String = "../maps/icons/tooltip/asterisk_optional.png";

        public static const MAPS_ICONS_TOOLTIP_ASTERISK_RED:String = "../maps/icons/tooltip/asterisk_red.png";

        public static const MAPS_ICONS_TOOLTIP_COMPLEX_EQUIPMENT:String = "../maps/icons/tooltip/complex_equipment.png";

        public static const MAPS_ICONS_TOOLTIP_CRITS:String = "../maps/icons/tooltip/crits.png";

        public static const MAPS_ICONS_TOOLTIP_DAMAGE:String = "../maps/icons/tooltip/damage.png";

        public static const MAPS_ICONS_TOOLTIP_DONEICON:String = "../maps/icons/tooltip/doneIcon.png";

        public static const MAPS_ICONS_TOOLTIP_DUPLICATED_OPTIONAL:String = "../maps/icons/tooltip/duplicated_optional.png";

        public static const MAPS_ICONS_TOOLTIP_EVIL:String = "../maps/icons/tooltip/evil.png";

        public static const MAPS_ICONS_TOOLTIP_FLAGTOOLTIP:String = "../maps/icons/tooltip/flagTooltip.png";

        public static const MAPS_ICONS_TOOLTIP_IGR_64X26:String = "../maps/icons/tooltip/igr_64x26.png";

        public static const MAPS_ICONS_TOOLTIP_KILL:String = "../maps/icons/tooltip/kill.png";

        public static const MAPS_ICONS_TOOLTIP_LINE:String = "../maps/icons/tooltip/line.png";

        public static const MAPS_ICONS_TOOLTIP_MAIN_TYPE:String = "../maps/icons/tooltip/main_type.png";

        public static const MAPS_ICONS_TOOLTIP_RESOURCETOOLTIP:String = "../maps/icons/tooltip/resourceTooltip.png";

        public static const MAPS_ICONS_TOOLTIP_SPOTTED:String = "../maps/icons/tooltip/spotted.png";

        public static const MAPS_ICONS_TOOLTIP_TEAMKILL:String = "../maps/icons/tooltip/teamKill.png";

        public static const MAPS_ICONS_TOOLTIP_TOOLTIP_TANKMAN_BACK:String = "../maps/icons/tooltip/tooltip_tankman_back.png";

        public static const MAPS_ICONS_TOOLTIP_TOOL_TIP_SEPARATOR:String = "../maps/icons/tooltip/tool_tip_separator.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_CHINA:String = "../maps/icons/tooltip/flag_160x100/china.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_CZECH:String = "../maps/icons/tooltip/flag_160x100/czech.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_FRANCE:String = "../maps/icons/tooltip/flag_160x100/france.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_GERMANY:String = "../maps/icons/tooltip/flag_160x100/germany.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_ITALY:String = "../maps/icons/tooltip/flag_160x100/italy.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_JAPAN:String = "../maps/icons/tooltip/flag_160x100/japan.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_POLAND:String = "../maps/icons/tooltip/flag_160x100/poland.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_SWEDEN:String = "../maps/icons/tooltip/flag_160x100/sweden.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_UK:String = "../maps/icons/tooltip/flag_160x100/uk.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_USA:String = "../maps/icons/tooltip/flag_160x100/usa.png";

        public static const MAPS_ICONS_TOOLTIP_FLAG_160X100_USSR:String = "../maps/icons/tooltip/flag_160x100/ussr.png";

        public static const MAPS_ICONS_TRADEIN_TRADE_IN_ICON:String = "../maps/icons/tradeIn/trade_in_icon.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_CHINA:String = "../maps/icons/tradeIn/nations/china.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_CZECH:String = "../maps/icons/tradeIn/nations/czech.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_FRANCE:String = "../maps/icons/tradeIn/nations/france.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_GERMANY:String = "../maps/icons/tradeIn/nations/germany.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_ITALY:String = "../maps/icons/tradeIn/nations/italy.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_JAPAN:String = "../maps/icons/tradeIn/nations/japan.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_POLAND:String = "../maps/icons/tradeIn/nations/poland.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_SWEDEN:String = "../maps/icons/tradeIn/nations/sweden.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_UK:String = "../maps/icons/tradeIn/nations/uk.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_USA:String = "../maps/icons/tradeIn/nations/usa.png";

        public static const MAPS_ICONS_TRADEIN_NATIONS_USSR:String = "../maps/icons/tradeIn/nations/ussr.png";

        public static const MAPS_ICONS_VEHICLESTATES_BATTLE:String = "../maps/icons/vehicleStates/battle.png";

        public static const MAPS_ICONS_VEHICLESTATES_CREWNOTFULL:String = "../maps/icons/vehicleStates/crewNotFull.png";

        public static const MAPS_ICONS_VEHICLESTATES_DAMAGED:String = "../maps/icons/vehicleStates/damaged.png";

        public static const MAPS_ICONS_VEHICLESTATES_GROUP_IS_NOT_READY:String = "../maps/icons/vehicleStates/group_is_not_ready.png";

        public static const MAPS_ICONS_VEHICLESTATES_INPREBATTLE:String = "../maps/icons/vehicleStates/inPrebattle.png";

        public static const MAPS_ICONS_VEHICLESTATES_RENTAGAIN_ICO_BIG:String = "../maps/icons/vehicleStates/rentAgain_ico_big.png";

        public static const MAPS_ICONS_VEHICLESTATES_RENTALISOVER:String = "../maps/icons/vehicleStates/rentalIsOver.png";

        public static const MAPS_ICONS_VEHICLESTATES_RENT_ICO_BIG:String = "../maps/icons/vehicleStates/rent_ico_big.png";

        public static const MAPS_ICONS_VEHICLESTATES_UNSUITABLETOUNIT:String = "../maps/icons/vehicleStates/unsuitableToUnit.png";

        public static const MAPS_ICONS_VEHICLETYPES_AT_SPG:String = "../maps/icons/vehicleTypes/AT-SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_HEAVYTANK:String = "../maps/icons/vehicleTypes/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_LIGHTTANK:String = "../maps/icons/vehicleTypes/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_MEDIUMTANK:String = "../maps/icons/vehicleTypes/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_SPG:String = "../maps/icons/vehicleTypes/SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_AT_SPG:String = "../maps/icons/vehicleTypes/big/AT-SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_AT_SPG_ELITE:String = "../maps/icons/vehicleTypes/big/AT-SPG_elite.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_HEAVYTANK:String = "../maps/icons/vehicleTypes/big/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_HEAVYTANK_ELITE:String = "../maps/icons/vehicleTypes/big/heavyTank_elite.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_LIGHTTANK:String = "../maps/icons/vehicleTypes/big/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_LIGHTTANK_ELITE:String = "../maps/icons/vehicleTypes/big/lightTank_elite.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_MEDIUMTANK:String = "../maps/icons/vehicleTypes/big/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_MEDIUMTANK_ELITE:String = "../maps/icons/vehicleTypes/big/mediumTank_elite.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_SPG:String = "../maps/icons/vehicleTypes/big/SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_BIG_SPG_ELITE:String = "../maps/icons/vehicleTypes/big/SPG_elite.png";

        public static const MAPS_ICONS_VEHICLETYPES_ELITE_AT_SPG:String = "../maps/icons/vehicleTypes/elite/AT-SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_ELITE_HEAVYTANK:String = "../maps/icons/vehicleTypes/elite/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_ELITE_LIGHTTANK:String = "../maps/icons/vehicleTypes/elite/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_ELITE_MEDIUMTANK:String = "../maps/icons/vehicleTypes/elite/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_ELITE_SPG:String = "../maps/icons/vehicleTypes/elite/SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_GOLD_AT_SPG:String = "../maps/icons/vehicleTypes/gold/AT-SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_GOLD_HEAVYTANK:String = "../maps/icons/vehicleTypes/gold/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_GOLD_LIGHTTANK:String = "../maps/icons/vehicleTypes/gold/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_GOLD_MEDIUMTANK:String = "../maps/icons/vehicleTypes/gold/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_GOLD_SPG:String = "../maps/icons/vehicleTypes/gold/SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_GREEN_AT_SPG:String = "../maps/icons/vehicleTypes/green/at-spg.png";

        public static const MAPS_ICONS_VEHICLETYPES_GREEN_HEAVYTANK:String = "../maps/icons/vehicleTypes/green/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_GREEN_LIGHTTANK:String = "../maps/icons/vehicleTypes/green/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_GREEN_MEDIUMTANK:String = "../maps/icons/vehicleTypes/green/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_GREEN_SPG:String = "../maps/icons/vehicleTypes/green/spg.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_AT_SPG:String = "../maps/icons/vehicleTypes/outline/AT-SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_HEAVYTANK:String = "../maps/icons/vehicleTypes/outline/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_ALLIANCE_FRANCE:String = "../maps/icons/vehicleTypes/outline/inactive_Alliance-France.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_ALLIANCE_GERMANY:String = "../maps/icons/vehicleTypes/outline/inactive_Alliance-Germany.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_ALLIANCE_USA:String = "../maps/icons/vehicleTypes/outline/inactive_Alliance-USA.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_ALLIANCE_USSR:String = "../maps/icons/vehicleTypes/outline/inactive_Alliance-USSR.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_AT_SPG:String = "../maps/icons/vehicleTypes/outline/inactive_AT-SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_HEAVYTANK:String = "../maps/icons/vehicleTypes/outline/inactive_heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_LIGHTTANK:String = "../maps/icons/vehicleTypes/outline/inactive_lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_MEDIUMTANK:String = "../maps/icons/vehicleTypes/outline/inactive_mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_INACTIVE_SPG:String = "../maps/icons/vehicleTypes/outline/inactive_SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_LIGHTTANK:String = "../maps/icons/vehicleTypes/outline/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_MEDIUMTANK:String = "../maps/icons/vehicleTypes/outline/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_OUTLINE_SPG:String = "../maps/icons/vehicleTypes/outline/SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_RED_AT_SPG:String = "../maps/icons/vehicleTypes/red/at-spg.png";

        public static const MAPS_ICONS_VEHICLETYPES_RED_HEAVYTANK:String = "../maps/icons/vehicleTypes/red/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_RED_LIGHTTANK:String = "../maps/icons/vehicleTypes/red/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_RED_MEDIUMTANK:String = "../maps/icons/vehicleTypes/red/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_RED_SPG:String = "../maps/icons/vehicleTypes/red/spg.png";

        public static const MAPS_ICONS_VEHICLETYPES_SILVER_24X30_AT_SPG:String = "../maps/icons/vehicleTypes/silver_24x30/AT-SPG.png";

        public static const MAPS_ICONS_VEHICLETYPES_SILVER_24X30_HEAVYTANK:String = "../maps/icons/vehicleTypes/silver_24x30/heavyTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_SILVER_24X30_LIGHTTANK:String = "../maps/icons/vehicleTypes/silver_24x30/lightTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_SILVER_24X30_MEDIUMTANK:String = "../maps/icons/vehicleTypes/silver_24x30/mediumTank.png";

        public static const MAPS_ICONS_VEHICLETYPES_SILVER_24X30_SPG:String = "../maps/icons/vehicleTypes/silver_24x30/SPG.png";

        public static const MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_ENUM:Array = [MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_1,MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_1_SMALL,MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_2,MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_2_SMALL,MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_3,MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_3_SMALL,MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_4,MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_4_SMALL];

        public static const MAPS_ICONS_BUTTONS_ENUM:Array = [MAPS_ICONS_BUTTONS_BOOKMARK,MAPS_ICONS_BUTTONS_BUTTON_BACK_ARROW,MAPS_ICONS_BUTTONS_BUTTON_NEXT_ARROW,MAPS_ICONS_BUTTONS_BUTTON_PLAY2,MAPS_ICONS_BUTTONS_CALENDAR,MAPS_ICONS_BUTTONS_CHECKMARK,MAPS_ICONS_BUTTONS_CLAN_LIST,MAPS_ICONS_BUTTONS_CLAN_STATS,MAPS_ICONS_BUTTONS_DESTROY,MAPS_ICONS_BUTTONS_ENTERWHITE,MAPS_ICONS_BUTTONS_ENVELOPE,MAPS_ICONS_BUTTONS_ENVELOPEOPENED,MAPS_ICONS_BUTTONS_EQUIPPED_ICON,MAPS_ICONS_BUTTONS_EXPLORE,MAPS_ICONS_BUTTONS_EXP_GATHERING,MAPS_ICONS_BUTTONS_FILTER,MAPS_ICONS_BUTTONS_FOOTHOLD,MAPS_ICONS_BUTTONS_GOLD_EXCHANGING,MAPS_ICONS_BUTTONS_HORN,MAPS_ICONS_BUTTONS_ICONDESTROY,MAPS_ICONS_BUTTONS_ICONDOWN,MAPS_ICONS_BUTTONS_ICONUP,MAPS_ICONS_BUTTONS_ICONUPGRADE,MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_LEFT,MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_LEFT_DOUBLE,MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_RIGHT,MAPS_ICONS_BUTTONS_ICON_ARROW_FORMATION_RIGHT_DOUBLE,MAPS_ICONS_BUTTONS_ICON_BUTTON_ARROW,MAPS_ICONS_BUTTONS_ICON_CHANNELS_HIDE_SYMBOLS,MAPS_ICONS_BUTTONS_ICON_CHANNELS_SHOW_SYMBOLS_097,MAPS_ICONS_BUTTONS_ICON_TABLE_COMPARISON_CHECKMARK,MAPS_ICONS_BUTTONS_ICON_TABLE_COMPARISON_INHANGAR,MAPS_ICONS_BUTTONS_LEVEL_UP,MAPS_ICONS_BUTTONS_MAINMENU,MAPS_ICONS_BUTTONS_MAITENANCE,MAPS_ICONS_BUTTONS_NATION_CHANGE,MAPS_ICONS_BUTTONS_NC_ICON_MEDIUM,MAPS_ICONS_BUTTONS_NEXTPAGE,MAPS_ICONS_BUTTONS_NON_HISTORICAL,MAPS_ICONS_BUTTONS_PAUSE,MAPS_ICONS_BUTTONS_PLAY,MAPS_ICONS_BUTTONS_PLAYERS,MAPS_ICONS_BUTTONS_PREMIUM,MAPS_ICONS_BUTTONS_PREVIOUSPAGE,MAPS_ICONS_BUTTONS_REFILL_ACCOUNT,MAPS_ICONS_BUTTONS_REFRESH,MAPS_ICONS_BUTTONS_REMOVE,MAPS_ICONS_BUTTONS_REMOVEGOLD,MAPS_ICONS_BUTTONS_REVERT_24X24,MAPS_ICONS_BUTTONS_SEARCH,MAPS_ICONS_BUTTONS_SELECTORRENDERERBGEVENT,MAPS_ICONS_BUTTONS_SETTINGS,MAPS_ICONS_BUTTONS_SMALLATTACKICON,MAPS_ICONS_BUTTONS_SOUND,MAPS_ICONS_BUTTONS_SPEAKER_SMALL,MAPS_ICONS_BUTTONS_SPEAKER_SUB,MAPS_ICONS_BUTTONS_SWAP,MAPS_ICONS_BUTTONS_SWAP2,MAPS_ICONS_BUTTONS_SWAP3,MAPS_ICONS_BUTTONS_SWAP3_ROTATED,MAPS_ICONS_BUTTONS_TANK_ICO,MAPS_ICONS_BUTTONS_TANKMAN_SKILLS_DROP,MAPS_ICONS_BUTTONS_TIMER,MAPS_ICONS_BUTTONS_TRANSPORTING,MAPS_ICONS_BUTTONS_TRANSPORTINGARROW,MAPS_ICONS_BUTTONS_TUNING,MAPS_ICONS_BUTTONS_VEHICLE_FILTER,MAPS_ICONS_BUTTONS_VEHICLECOMPAREBTN,MAPS_ICONS_BUTTONS_SOCIAL_FACEBOOK,MAPS_ICONS_BUTTONS_SOCIAL_GOOGLE,MAPS_ICONS_BUTTONS_SOCIAL_NAVER,MAPS_ICONS_BUTTONS_SOCIAL_VKONTAKTE,MAPS_ICONS_BUTTONS_SOCIAL_WGNI,MAPS_ICONS_BUTTONS_SOCIAL_YAHOO,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_FACEBOOK,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_GOOGLE,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_NAVER,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_VKONTAKTE,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_WGNI,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_YAHOO,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ASCENDINGSORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ASCPROFILESORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DAMAGE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DEATHS,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DESCENDINGSORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DESCPROFILESORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FALLOUTRESOURCEPOINTS,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FLAG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FRAG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_HEALTH,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_LEVEL,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_LEVEL_6_8,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_MEDAL,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_NOTAVAILABLE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_OK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYER,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYERNUMBER,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYERRANK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RANK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RESOURCECOUNT,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RESPAWN,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_SQUAD,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_STAR_RED,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TABLEHEADERBG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TABLEHEADERSORTEDBG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TANK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TEAMSCORE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_VICTORYSCORE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_XP];

        public static const MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ENUM:Array = [MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_ASIA,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_EU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_NA,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER1_RU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_ASIA,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_EU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_NA,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER2_RU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER3_EU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER3_RU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER4_EU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_BOB20_COMMANDER4_RU,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CHINA_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_COMMANDER_MARINA,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_31,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CZECH_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_31,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_32,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_33,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_34,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_FRANCE_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_31,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_32,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GERMANY_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_GIRL_EMPTY,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_BUFFON,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ITALY_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_CARISA_CONTZEN,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_GUNNER,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_GUNNER_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_GUSURG,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_ISARA_GUNTHER,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_KREIS_CZHERNY,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_LOADER,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_LOADER_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_RADIO_OPERATOR,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_JAPAN_WELKIN_GUNTHER,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_MEN,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY19_WOMAN_L8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_CHRISTMAS,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_FAIRYTALE,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_NEWYEAR,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_GIRL_ORIENTAL,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_MEN,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_NY20_SNOWMEN,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_21_KOS,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_22_CZERESNIAK,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_23_SAAKASZWILI,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_24_JELEN,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_POLAND_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_RACE19_COMMANDER_A,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_RACE19_COMMANDER_B,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_RACE19_COMMANDER_C,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_31,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_32,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_33,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_34,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_35,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_36,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_37,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_SWEDEN_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TANKMAN,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH6_PUMPKIN_TWITCH,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH_GIRL,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_TWITCH_GUY,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_31,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_32,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_33,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_34,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_35,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_UK_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_31,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_32,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_33,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_34,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_35,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_36,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_37,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_38,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_39,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_40,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_41,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_42,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_43,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_44,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_45,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_46,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_47,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USA_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_20,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_21,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_22,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_23,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_24,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_25,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_26,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_27,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_28,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_29,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_30,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_31,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_32,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_33,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_34,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_1,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_17,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_18,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_19,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_2,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_3,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_4,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_5,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_6,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_7,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_8,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_USSR_FEMALE_9,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE01,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE02,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE03,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE04,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE05,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE06,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE07,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE08,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE09,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_BRAVE16,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_DEGAULLE,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_CRAYFISH,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_DEER,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_RNGESUS,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_TOMATO,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_FOOLSDAY_UNICORN,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_KATUKOV,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_PATTON,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE01,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE02,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE03,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE04,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE05,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE06,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE07,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE08,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE09,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE10,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE11,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE12,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE13,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE14,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RACE15,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_RYBALKO,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE01,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE02,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE03,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE04,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE05,MAPS_ICONS_TANKMEN_ICONS_BARRACKS_CREWSKINS_SPACE06];

        public static const MAPS_ICONS_TANKMEN_SKILLS_SMALL_ENUM:Array = [MAPS_ICONS_TANKMEN_SKILLS_SMALL_BROTHERHOOD,MAPS_ICONS_TANKMEN_SKILLS_SMALL_CAMOUFLAGE,MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_EAGLEEYE,MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_EXPERT,MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_SIXTHSENSE,MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_TUTOR,MAPS_ICONS_TANKMEN_SKILLS_SMALL_COMMANDER_UNIVERSALIST,MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_BADROADSKING,MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_RAMMINGMASTER,MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_SMOOTHDRIVING,MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_TIDYPERSON,MAPS_ICONS_TANKMEN_SKILLS_SMALL_DRIVER_VIRTUOSO,MAPS_ICONS_TANKMEN_SKILLS_SMALL_FIREFIGHTING,MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_GUNSMITH,MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_RANCOROUS,MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_SMOOTHTURRET,MAPS_ICONS_TANKMEN_SKILLS_SMALL_GUNNER_SNIPER,MAPS_ICONS_TANKMEN_SKILLS_SMALL_LOADER_DESPERADO,MAPS_ICONS_TANKMEN_SKILLS_SMALL_LOADER_INTUITION,MAPS_ICONS_TANKMEN_SKILLS_SMALL_LOADER_PEDANT,MAPS_ICONS_TANKMEN_SKILLS_SMALL_NEW_CURRENT_SKILL,MAPS_ICONS_TANKMEN_SKILLS_SMALL_NEW_SKILL,MAPS_ICONS_TANKMEN_SKILLS_SMALL_OFFSPRING_BROTHERHOOD,MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_FINDER,MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_INVENTOR,MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_LASTEFFORT,MAPS_ICONS_TANKMEN_SKILLS_SMALL_RADIOMAN_RETRANSMITTER,MAPS_ICONS_TANKMEN_SKILLS_SMALL_REPAIR,MAPS_ICONS_TANKMEN_SKILLS_SMALL_SABATON_BROTHERHOOD];

        public static const MAPS_ICONS_BUTTONS_SOCIAL_COLOR_ENUM:Array = [MAPS_ICONS_BUTTONS_SOCIAL_COLOR_FACEBOOK,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_GOOGLE,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_NAVER,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_VKONTAKTE,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_WGNI,MAPS_ICONS_BUTTONS_SOCIAL_COLOR_YAHOO];

        public static const MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ENUM:Array = [MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ASCENDINGSORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ASCPROFILESORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DAMAGE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DEATHS,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DESCENDINGSORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_DESCPROFILESORTARROW,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FALLOUTRESOURCEPOINTS,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FLAG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_FRAG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_HEALTH,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_LEVEL,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_LEVEL_6_8,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_MEDAL,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_NOTAVAILABLE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_OK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYER,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYERNUMBER,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_PLAYERRANK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RANK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RESOURCECOUNT,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_RESPAWN,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_SQUAD,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_STAR_RED,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TABLEHEADERBG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TABLEHEADERSORTEDBG,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TANK,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_TEAMSCORE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_VICTORYSCORE,MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_XP];

        public static const MAPS_ICONS_FILTERS_NATIONS_ENUM:Array = [MAPS_ICONS_FILTERS_NATIONS_ALL,MAPS_ICONS_FILTERS_NATIONS_CHINA,MAPS_ICONS_FILTERS_NATIONS_CZECH,MAPS_ICONS_FILTERS_NATIONS_FRANCE,MAPS_ICONS_FILTERS_NATIONS_GERMANY,MAPS_ICONS_FILTERS_NATIONS_IGR,MAPS_ICONS_FILTERS_NATIONS_ITALY,MAPS_ICONS_FILTERS_NATIONS_JAPAN,MAPS_ICONS_FILTERS_NATIONS_POLAND,MAPS_ICONS_FILTERS_NATIONS_SWEDEN,MAPS_ICONS_FILTERS_NATIONS_UK,MAPS_ICONS_FILTERS_NATIONS_USA,MAPS_ICONS_FILTERS_NATIONS_USSR];

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ALL_90X90_ENUM:Array = [MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_RADIO_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_STUN_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_STUN_MULTY_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_STUN_TIME_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ASSIST_TRACK_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_AWARD_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BARREL_MARK_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BASE_CAPTURE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BASE_DEF_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_BATTLES_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_CREDITS_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_DAMAGE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_DAMAGE_BLOCK_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_DISCOVER_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_EXPERIENCE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_FIRE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_FOLDER_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_GET_DAMAGE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_GET_HIT_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_HIT_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_HURT_1SHOT_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_HURT_VEHICLES_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_IMPROVE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_KILL_1SHOT_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_KILL_VEHICLES_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_MAIN_REPEAT_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_MASTER_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_METERS_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_MODULE_CRIT_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_PREPARATION_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_RAM_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_SAVE_HP_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_SEC_ALIVE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_SURVIVE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_TIMES_GET_DAMAGE_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_TOP_90X90,MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_WIN_90X90];

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ALL_128X128_ENUM:Array = [MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_RADIO_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_STUN_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_STUN_MULTY_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_STUN_TIME_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ASSIST_TRACK_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_AWARD_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BARREL_MARK_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BASE_CAPTURE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BASE_DEF_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_BATTLES_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_CREDITS_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_DAMAGE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_DAMAGE_BLOCK_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_DISCOVER_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_EXPERIENCE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_FIRE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_FOLDER_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_GET_DAMAGE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_GET_HIT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_HIT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_HURT_1SHOT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_HURT_VEHICLES_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_IMPROVE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_KILL_1SHOT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_KILL_VEHICLES_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_MAIN_REPEAT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_MASTER_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_METERS_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_MODULE_CRIT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_PREPARATION_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_RAM_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_SAVE_HP_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_SEC_ALIVE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_SURVIVE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_TIMES_GET_DAMAGE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_TOP_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_WIN_128X128];

        public static const MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ALL_128X128_ENUM:Array = [MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_RADIO_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_STUN_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_STUN_MULTY_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_STUN_TIME_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ASSIST_TRACK_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_AWARD_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_BASE_CAPTURE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_BASE_DEF_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_DAMAGE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_DAMAGE_BLOCK_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_DISCOVER_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_FIRE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_FOLDER_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_HIT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_HURT_VEHICLES_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_IMPROVE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_KILL_VEHICLES_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_MAIN_REPEAT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_MASTER_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_MODULE_CRIT_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_PREPARATION_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_RAM_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_SURVIVE_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_TOP_128X128,MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_WIN_128X128];

        public static const MAPS_ICONS_BATTLETYPES_136X136_ENUM:Array = [MAPS_ICONS_BATTLETYPES_136X136_ASSAULT,MAPS_ICONS_BATTLETYPES_136X136_ASSAULT1,MAPS_ICONS_BATTLETYPES_136X136_ASSAULT2,MAPS_ICONS_BATTLETYPES_136X136_BOB,MAPS_ICONS_BATTLETYPES_136X136_BOOTCAMP,MAPS_ICONS_BATTLETYPES_136X136_CLAN,MAPS_ICONS_BATTLETYPES_136X136_CTF,MAPS_ICONS_BATTLETYPES_136X136_CTF30X30,MAPS_ICONS_BATTLETYPES_136X136_DOMINATION,MAPS_ICONS_BATTLETYPES_136X136_DOMINATION30X30,MAPS_ICONS_BATTLETYPES_136X136_EPICBATTLE,MAPS_ICONS_BATTLETYPES_136X136_ESCORT,MAPS_ICONS_BATTLETYPES_136X136_FALLOUT,MAPS_ICONS_BATTLETYPES_136X136_FALLOUT_CLASSIC,MAPS_ICONS_BATTLETYPES_136X136_FALLOUT_MULTITEAM,MAPS_ICONS_BATTLETYPES_136X136_FORTIFICATIONS,MAPS_ICONS_BATTLETYPES_136X136_HISTORICAL,MAPS_ICONS_BATTLETYPES_136X136_NATIONS,MAPS_ICONS_BATTLETYPES_136X136_NEUTRAL,MAPS_ICONS_BATTLETYPES_136X136_PLATOON,MAPS_ICONS_BATTLETYPES_136X136_RANDOM,MAPS_ICONS_BATTLETYPES_136X136_RANKED,MAPS_ICONS_BATTLETYPES_136X136_RATEDSANDBOX,MAPS_ICONS_BATTLETYPES_136X136_RESOURCEPOINTS,MAPS_ICONS_BATTLETYPES_136X136_SANDBOX,MAPS_ICONS_BATTLETYPES_136X136_SQUAD,MAPS_ICONS_BATTLETYPES_136X136_TEAM,MAPS_ICONS_BATTLETYPES_136X136_TEAM7X7,MAPS_ICONS_BATTLETYPES_136X136_TRAINING,MAPS_ICONS_BATTLETYPES_136X136_TUTORIAL];

        public static const MAPS_ICONS_BATTLETYPES_64X64_ENUM:Array = [MAPS_ICONS_BATTLETYPES_64X64_ASSAULT,MAPS_ICONS_BATTLETYPES_64X64_ASSAULT1,MAPS_ICONS_BATTLETYPES_64X64_ASSAULT2,MAPS_ICONS_BATTLETYPES_64X64_BATTLETEACHING,MAPS_ICONS_BATTLETYPES_64X64_BOB,MAPS_ICONS_BATTLETYPES_64X64_BOBSQUAD,MAPS_ICONS_BATTLETYPES_64X64_CTF,MAPS_ICONS_BATTLETYPES_64X64_CTF30X30,MAPS_ICONS_BATTLETYPES_64X64_DOMINATION,MAPS_ICONS_BATTLETYPES_64X64_EPICBATTLE,MAPS_ICONS_BATTLETYPES_64X64_EPICQUEUE,MAPS_ICONS_BATTLETYPES_64X64_EPICTRAININGSLIST,MAPS_ICONS_BATTLETYPES_64X64_ESCORT,MAPS_ICONS_BATTLETYPES_64X64_ESPORT,MAPS_ICONS_BATTLETYPES_64X64_EVENT,MAPS_ICONS_BATTLETYPES_64X64_EVENTSQUAD,MAPS_ICONS_BATTLETYPES_64X64_FALLOUT,MAPS_ICONS_BATTLETYPES_64X64_FALLOUT_CLASSIC,MAPS_ICONS_BATTLETYPES_64X64_FALLOUT_MULTITEAM,MAPS_ICONS_BATTLETYPES_64X64_FORT,MAPS_ICONS_BATTLETYPES_64X64_FORTIFICATIONS,MAPS_ICONS_BATTLETYPES_64X64_HISTORICAL,MAPS_ICONS_BATTLETYPES_64X64_NATIONS,MAPS_ICONS_BATTLETYPES_64X64_NEUTRAL,MAPS_ICONS_BATTLETYPES_64X64_RANDOM,MAPS_ICONS_BATTLETYPES_64X64_RANKED,MAPS_ICONS_BATTLETYPES_64X64_RANKED_EPICRANDOM,MAPS_ICONS_BATTLETYPES_64X64_RESOURCEPOINTS,MAPS_ICONS_BATTLETYPES_64X64_SPECBATTLESLIST,MAPS_ICONS_BATTLETYPES_64X64_SQUAD,MAPS_ICONS_BATTLETYPES_64X64_STRONGHOLDSBATTLESLIST,MAPS_ICONS_BATTLETYPES_64X64_TEAM7X7,MAPS_ICONS_BATTLETYPES_64X64_TRAININGSLIST,MAPS_ICONS_BATTLETYPES_64X64_TUTORIAL];

        public static const MAPS_ICONS_RANKEDBATTLES_STATS_ALL_ENUM:Array = [MAPS_ICONS_RANKEDBATTLES_STATS_42_EFFICIENCY,MAPS_ICONS_RANKEDBATTLES_STATS_42_POSITION,MAPS_ICONS_RANKEDBATTLES_STATS_52_BATTLES,MAPS_ICONS_RANKEDBATTLES_STATS_52_BATTLESTOTAL,MAPS_ICONS_RANKEDBATTLES_STATS_52_DIVISIONEFFICIENCY,MAPS_ICONS_RANKEDBATTLES_STATS_52_EFFICIENCY,MAPS_ICONS_RANKEDBATTLES_STATS_52_STRIPES,MAPS_ICONS_RANKEDBATTLES_STATS_52_STRIPESTOTAL,MAPS_ICONS_RANKEDBATTLES_STATS_64_EFFICIENCY,MAPS_ICONS_RANKEDBATTLES_STATS_64_POSITION,MAPS_ICONS_RANKEDBATTLES_STATS_64_STEPS,MAPS_ICONS_RANKEDBATTLES_STATS_70_BATTLES,MAPS_ICONS_RANKEDBATTLES_STATS_70_BATTLESTOTAL,MAPS_ICONS_RANKEDBATTLES_STATS_70_DIVISIONEFFICIENCY,MAPS_ICONS_RANKEDBATTLES_STATS_70_EFFICIENCY,MAPS_ICONS_RANKEDBATTLES_STATS_70_STRIPES,MAPS_ICONS_RANKEDBATTLES_STATS_70_STRIPESTOTAL,MAPS_ICONS_RANKEDBATTLES_STATS_84_EFFICIENCY,MAPS_ICONS_RANKEDBATTLES_STATS_84_POSITION,MAPS_ICONS_RANKEDBATTLES_STATS_84_STEPS];

        public static const MAPS_ICONS_RANKEDBATTLES_DIVISIONS_ALL_ENUM:Array = [MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_0,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_1,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_2,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_114X160_3,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_0,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_1,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_2,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_190X260_3,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_BRONZE,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_CLASSIFICATION,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_GOLD,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_56X56_SILVER,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_0,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_1,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_2,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_58X80_3,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_0,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_1,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_2,MAPS_ICONS_RANKEDBATTLES_DIVISIONS_80X110_3];

        public static const MAPS_ICONS_RANKEDBATTLES_LEAGUE_ALL_ENUM:Array = [MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_0,MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_1,MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_2,MAPS_ICONS_RANKEDBATTLES_LEAGUE_100X100_3,MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_0,MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_1,MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_2,MAPS_ICONS_RANKEDBATTLES_LEAGUE_130X130_3,MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_0,MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_1,MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_2,MAPS_ICONS_RANKEDBATTLES_LEAGUE_300X300_3,MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_0,MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_1,MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_2,MAPS_ICONS_RANKEDBATTLES_LEAGUE_70X70_3];

        public static const MAPS_ICONS_RANKEDBATTLES_RANKS_ALL_RANKALL_ENUM:Array = [MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_10,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_11,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_12,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_13,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_14,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_15,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_4,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_5,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_6,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_7,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_8,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK0_9,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_10,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_11,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_12,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_13,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_14,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_15,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_4,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_5,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_6,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_7,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_8,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK1_9,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_10,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_11,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_12,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_13,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_14,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_15,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_4,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_5,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_6,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_7,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_8,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK2_9,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_10,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_11,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_12,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_13,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_14,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_15,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_4,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_5,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_6,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_7,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_8,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANK3_9,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_114X160_RANKS_GROUP3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_10,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_11,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_12,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_13,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_14,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_15,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_4,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_5,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_6,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_7,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_8,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK0_9,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_10,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_11,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_12,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_13,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_14,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_15,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_4,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_5,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_6,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_7,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_8,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK1_9,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_10,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_11,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_12,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_13,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_14,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_15,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_4,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_5,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_6,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_7,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_8,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK2_9,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_10,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_11,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_12,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_13,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_14,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_15,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_4,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_5,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_6,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_7,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_8,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANK3_9,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_190X260_RANKS_GROUP3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_0,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_10,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_11,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_12,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_13,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_14,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_15,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_4,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_5,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_6,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_7,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_8,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK0_9,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_0,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_10,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_11,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_12,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_13,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_14,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_15,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_4,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_5,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_6,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_7,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_8,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK1_9,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_0,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_10,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_11,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_12,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_13,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_14,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_15,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_4,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_5,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_6,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_7,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_8,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK2_9,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_0,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_10,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_11,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_12,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_13,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_14,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_15,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_4,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_5,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_6,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_7,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_8,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK3_9,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANK4_0,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_24X24_RANKS_GROUP3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_10,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_11,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_12,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_13,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_14,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_15,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_4,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_5,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_6,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_7,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_8,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK0_9,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_10,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_11,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_12,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_13,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_14,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_15,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_4,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_5,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_6,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_7,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_8,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK1_9,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_10,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_11,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_12,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_13,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_14,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_15,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_4,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_5,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_6,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_7,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_8,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK2_9,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_10,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_11,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_12,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_13,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_14,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_15,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_4,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_5,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_6,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_7,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_8,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANK3_9,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKS_GROUP3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_58X80_RANKVEHMASTER_GREY,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_10,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_11,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_12,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_13,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_14,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_15,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_4,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_5,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_6,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_7,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_8,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK0_9,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_10,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_11,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_12,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_13,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_14,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_15,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_4,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_5,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_6,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_7,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_8,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK1_9,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_10,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_11,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_12,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_13,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_14,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_15,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_4,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_5,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_6,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_7,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_8,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK2_9,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_10,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_11,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_12,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_13,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_14,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_15,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_3,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_4,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_5,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_6,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_7,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_8,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANK3_9,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP0_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP0_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP0_3,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP1_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP1_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP1_3,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP2_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP2_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP2_3,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP3_1,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP3_2,MAPS_ICONS_RANKEDBATTLES_RANKS_80X110_RANKS_GROUP3_3];

        public static const MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_ENUM:Array = [MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_1,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_10,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_11,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_12,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_2,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_3,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_4,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_5,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_6,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_7,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_8,MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_9];

        public function RES_ICONS()
        {
            super();
        }

        public static function maps_icons_library_proficiency_class_icons(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/library/proficiency/class_icons_" + param1;
            if(MAPS_ICONS_LIBRARY_PROFICIENCY_CLASS_ICONS_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_library_proficiency_class_icons]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_buttons(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/buttons/" + param1;
            if(MAPS_ICONS_BUTTONS_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_buttons]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_tankmen_icons_barracks(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/tankmen/icons/barracks/" + param1;
            if(MAPS_ICONS_TANKMEN_ICONS_BARRACKS_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_tankmen_icons_barracks]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_tankmen_skills_small(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/tankmen/skills/small/" + param1;
            if(MAPS_ICONS_TANKMEN_SKILLS_SMALL_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_tankmen_skills_small]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_buttons_social_color(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/buttons/social/color/" + param1;
            if(MAPS_ICONS_BUTTONS_SOCIAL_COLOR_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_buttons_social_color]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_buttons_tab_sort_button(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/buttons/tab_sort_button/" + param1;
            if(MAPS_ICONS_BUTTONS_TAB_SORT_BUTTON_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_buttons_tab_sort_button]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function getFilterNation(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/filters/nations/" + param1 + ".png";
            if(MAPS_ICONS_FILTERS_NATIONS_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[getFilterNation]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_quests_battlecondition_90_icon_battle_condition_all_90x90_png(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/quests/battleCondition/90/icon_battle_condition_" + param1 + "_90x90.png";
            if(MAPS_ICONS_QUESTS_BATTLECONDITION_90_ICON_BATTLE_CONDITION_ALL_90X90_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_quests_battlecondition_90_icon_battle_condition_all_90x90_png]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_quests_battlecondition_128_icon_battle_condition_all_128x128_png(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/quests/battleCondition/128/icon_battle_condition_" + param1 + "_128x128.png";
            if(MAPS_ICONS_QUESTS_BATTLECONDITION_128_ICON_BATTLE_CONDITION_ALL_128X128_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_quests_battlecondition_128_icon_battle_condition_all_128x128_png]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_quests_battlecondition_128_decor_icon_battle_condition_all_128x128_png(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/quests/battleCondition/128_decor/icon_battle_condition_" + param1 + "_128x128.png";
            if(MAPS_ICONS_QUESTS_BATTLECONDITION_128_DECOR_ICON_BATTLE_CONDITION_ALL_128X128_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_quests_battlecondition_128_decor_icon_battle_condition_all_128x128_png]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_battletypes_136x136_all_png(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/battleTypes/136x136/" + param1 + ".png";
            if(MAPS_ICONS_BATTLETYPES_136X136_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_battletypes_136x136_all_png]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_battletypes_64x64_all_png(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/battleTypes/64x64/" + param1 + ".png";
            if(MAPS_ICONS_BATTLETYPES_64X64_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_battletypes_64x64_all_png]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function maps_icons_rankedbattles_stats_all_all_png(param1:String, param2:String) : String
        {
            var _loc3_:String = null;
            _loc3_ = "../" + "maps/icons/rankedBattles/stats/" + param1 + "/" + param2 + ".png";
            if(MAPS_ICONS_RANKEDBATTLES_STATS_ALL_ENUM.indexOf(_loc3_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_rankedbattles_stats_all_all_png]:locale key \"" + _loc3_ + "\" was not found");
                return null;
            }
            return _loc3_;
        }

        public static function maps_icons_rankedbattles_divisions_all_all_png(param1:String, param2:String) : String
        {
            var _loc3_:String = null;
            _loc3_ = "../" + "maps/icons/rankedBattles/divisions/" + param1 + "/" + param2 + ".png";
            if(MAPS_ICONS_RANKEDBATTLES_DIVISIONS_ALL_ENUM.indexOf(_loc3_) == -1)
            {
                DebugUtils.LOG_WARNING("[maps_icons_rankedbattles_divisions_all_all_png]:locale key \"" + _loc3_ + "\" was not found");
                return null;
            }
            return _loc3_;
        }

        public static function getLeagueIcon(param1:String, param2:String) : String
        {
            var _loc3_:String = null;
            _loc3_ = "../" + "maps/icons/rankedBattles/league/" + param1 + "/" + param2 + ".png";
            if(MAPS_ICONS_RANKEDBATTLES_LEAGUE_ALL_ENUM.indexOf(_loc3_) == -1)
            {
                DebugUtils.LOG_WARNING("[getLeagueIcon]:locale key \"" + _loc3_ + "\" was not found");
                return null;
            }
            return _loc3_;
        }

        public static function getRankIcon(param1:String, param2:String, param3:String) : String
        {
            var _loc4_:String = null;
            _loc4_ = "../" + "maps/icons/rankedBattles/ranks/" + param1 + "/rank" + param2 + "_" + param3 + ".png";
            if(MAPS_ICONS_RANKEDBATTLES_RANKS_ALL_RANKALL_ENUM.indexOf(_loc4_) == -1)
            {
                DebugUtils.LOG_WARNING("[getRankIcon]:locale key \"" + _loc4_ + "\" was not found");
                return null;
            }
            return _loc4_;
        }

        public static function getBlogger(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "../" + "maps/icons/bob/bloggers/fullPortrait/blogger_" + param1 + ".png";
            if(MAPS_ICONS_BOB_BLOGGERS_FULLPORTRAIT_BLOGGER_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[getBlogger]:locale key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }
    }
}
